package tests

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/stretchr/testify/require"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/constants"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/database/repositories"
	infraHttp "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/settings"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/tests/testsupport"
	testdata "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/tests/testsupport/data"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/response"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/validators"
)

type StrategiesStage struct {
	t       *testing.T
	require *require.Assertions

	dbPool *pgxpool.Pool

	payload []byte
	req     *http.Request
	resp    *http.Response
	body    []byte

	strategyID  uuid.UUID
	userID      uuid.UUID
	reqUser     *entities.User
	reqStrat    *entities.Strategy
	expStrat    *entities.Strategy
	expProgress *entities.RaceProgress
	reqProgress *entities.RaceProgress
}

func NewStrategiesStage(t *testing.T) (*StrategiesStage, *StrategiesStage, *StrategiesStage) {
	s := &StrategiesStage{
		t: t,
	}

	s.require = require.New(s.t)

	var err error
	s.dbPool, err = pgxpool.New(context.Background(), testsupport.DatabaseDSN)
	s.require.NoError(err, "create new postgres db pool")

	return s, s, s
}

func (s *StrategiesStage) and() *StrategiesStage {
	return s
}

func (s *StrategiesStage) a_valid_strategy() *StrategiesStage {
	reqStrat := *testdata.TestStrategy
	s.reqStrat = &reqStrat
	s.userID = uuid.New()
	s.reqStrat.UserID = s.userID

	expStrat := *s.reqStrat
	s.expStrat = &expStrat

	return s
}

func (s *StrategiesStage) a_valid_race_progress() *StrategiesStage {
	reqProgress := *testdata.TestRaceProgress
	s.reqProgress = &reqProgress

	expProgress := *s.reqProgress
	s.expProgress = &expProgress

	s.expStrat.Progress = &expProgress
	s.expStrat.Data.PitStrategy.Stints = testdata.TestProgressAdjustedStints
	return s
}

func (s *StrategiesStage) a_strategy_record_in_the_database() *StrategiesStage {
	s.a_valid_strategy()

	err := repositories.NewStrategy(s.dbPool).Create(context.Background(), s.reqStrat)
	s.require.NoError(err, "create strategy record in the database")

	s.strategyID = s.reqStrat.ID
	s.expStrat.ID = s.reqStrat.ID

	return s
}

func (s *StrategiesStage) strategy_adjusted_for_progress_stored_in_the_database() *StrategiesStage {
	s.a_valid_race_progress()

	err := repositories.NewStrategy(s.dbPool).StoreRecalculatedStrategy(context.Background(), s.expStrat)
	s.require.NoError(err, "create strategy record in the database")

	return s
}

func (s *StrategiesStage) a_request_for_retrieving_strategy_details() *StrategiesStage {
	var err error
	s.req, err = http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1/strategies",
		testsupport.BaseURL), nil)
	s.require.NoError(err, "build get /strategies request")
	return s
}

func (s *StrategiesStage) a_request_for_retrieving_user_saved_strategies() *StrategiesStage {
	// base url is the same as the url to retrieve details
	return s.a_request_for_retrieving_strategy_details()
}

func (s *StrategiesStage) a_request_for_saving_a_strategy() *StrategiesStage {
	var err error
	s.req, err = http.NewRequest(http.MethodPost, fmt.Sprintf("%s/api/v1/strategies/save",
		testsupport.BaseURL), nil)
	s.require.NoError(err, "build post /strategies/save request")
	return s
}

func (s *StrategiesStage) a_request_to_adjust_the_saved_strategy_with_progress() *StrategiesStage {
	var err error
	s.req, err = http.NewRequest(http.MethodPost, fmt.Sprintf("%s/api/v1/strategies/%s/progress",
		testsupport.BaseURL, s.strategyID), nil)
	s.require.NoErrorf(err, "build post /strategies/%s/progress request", s.strategyID)
	return s
}

func (s *StrategiesStage) a_valid_strategy_in_the_request() *StrategiesStage {
	s.a_valid_strategy()
	payload, err := json.Marshal(s.reqStrat)
	s.require.NoError(err, "marshal strategy request")

	s.req.Body = io.NopCloser(bytes.NewBuffer(payload))
	return s
}

func (s *StrategiesStage) an_invalid_strategy_in_the_request() *StrategiesStage {
	s.a_valid_strategy()

	s.reqStrat.Name = ""
	s.expStrat.Name = ""
	s.reqStrat.Data.RaceInfo.Laptime = 100
	s.expStrat.Data.RaceInfo.Laptime = 100

	payload, err := json.Marshal(s.reqStrat)
	s.require.NoError(err, "marshal strategy request")

	s.req.Body = io.NopCloser(bytes.NewBuffer(payload))
	return s

}

func (s *StrategiesStage) valid_race_progress_data_in_the_request() *StrategiesStage {
	s.a_valid_race_progress()

	payload, err := json.Marshal(s.reqProgress)
	s.require.NoError(err, "marshal race progress request")

	s.req.Body = io.NopCloser(bytes.NewBuffer(payload))

	return s
}

func (s *StrategiesStage) with_a_strategy_id(id string) *StrategiesStage {
	var err error
	s.req.URL.Path, err = url.JoinPath(s.req.URL.Path, fmt.Sprintf("/%s", id))
	s.require.NoError(err, "attach strategy id to get strategy detail url")
	return s
}

func (s *StrategiesStage) with_a_valid_and_existing_strategy_id() *StrategiesStage {
	if s.reqStrat != nil {
		return s.with_a_strategy_id(s.reqStrat.ID.String())
	}

	row := s.dbPool.QueryRow(context.Background(), "select id from strategies limit 1")
	err := row.Scan(&s.strategyID)
	s.require.NoError(err, "retrieve a strategies id from the database")

	return s.with_a_strategy_id(s.strategyID.String())
}

func (s *StrategiesStage) with_an_invalid_strategy_id() *StrategiesStage {
	return s.with_a_strategy_id("invalid id")
}

func (s *StrategiesStage) with_a_valid_but_nonexisting_strategy_id() *StrategiesStage {
	return s.with_a_strategy_id(uuid.New().String())
}

func (s *StrategiesStage) a_valid_user_token() *StrategiesStage {
	userID := uuid.New()
	if s.userID != uuid.Nil {
		userID = s.userID
	}
	s.reqUser = &entities.User{
		ID:          userID,
		Username:    "test_user",
		DisplayName: "new_setup_user",
	}

	token, err := testsupport.GenerateUserToken(s.reqUser, time.Now().Add(10*time.Minute))
	s.require.NoError(err, "signing of user data token failed")

	s.req.Header.Add(settings.UserTokenHeader, token)
	return s
}

func (s *StrategiesStage) a_valid_user_token_with_empty_data() *StrategiesStage {
	s.req.Header.Add(settings.UserTokenHeader, "")
	return s
}

func (s *StrategiesStage) the_correct_passphrase() *StrategiesStage {
	query := url.Values{}
	query.Add("passphrase", "random passphrase")
	s.req.URL.RawQuery = query.Encode()
	return s
}

func (s *StrategiesStage) the_request_is_executed() *StrategiesStage {
	var err error
	s.resp, err = http.DefaultClient.Do(s.req)
	s.require.NoError(err, "execute http request")
	defer s.resp.Body.Close()

	s.body, err = ioutil.ReadAll(s.resp.Body)
	s.require.NoError(err, "read response body")
	return s
}

func (s *StrategiesStage) the_response_status_is(status int) *StrategiesStage {
	s.require.Equal(status, s.resp.StatusCode, "response status unexpected")
	return s
}

func (s *StrategiesStage) the_response_status_is_ok() *StrategiesStage {
	return s.the_response_status_is(http.StatusOK)
}

func (s *StrategiesStage) the_strategy_details_are_in_expected_format() *StrategiesStage {
	strategy := entities.Strategy{}
	err := json.Unmarshal(s.body, &strategy)
	s.require.NoError(err, "unmarshal strategy detail response")
	s.require.Equal(s.strategyID.String(), strategy.ID.String())
	return s
}

func (s *StrategiesStage) error_response_details_indicate_invalid_id() *StrategiesStage {
	respErr := response.Error{}
	err := json.Unmarshal(s.body, &respErr)
	s.require.NoError(err, "unmarshal response error")
	s.require.Equal(constants.ErrorDetails_MissingOrMalformedResourceID, respErr.Details)
	return s
}

func (s *StrategiesStage) the_passphrase_is_empty() *StrategiesStage {
	strategy := entities.Strategy{}
	err := json.Unmarshal(s.body, &strategy)
	s.require.NoError(err, "unmarshal strategy detail response")
	s.require.Equal("", strategy.Passphrase, "unexpected passphrase in response")
	return s
}

func (s *StrategiesStage) the_passphrase_is_received_in_response() *StrategiesStage {
	strategy := entities.Strategy{}
	err := json.Unmarshal(s.body, &strategy)
	s.require.NoError(err, "unmarshal strategy detail response")
	s.require.Equal(s.reqStrat.Passphrase, strategy.Passphrase,
		"passphrase does not match on the retrieved strategy")
	return s
}

func (s *StrategiesStage) the_created_strategy_id_is_in_the_response_body() *StrategiesStage {
	strategy := entities.Strategy{}
	err := json.Unmarshal(s.body, &strategy)
	s.require.NoError(err, "unmarshal strategy detail response")

	_, err = repositories.NewStrategy(s.dbPool).GetByID(context.Background(), strategy.ID)
	s.require.NoError(err, "find strategy in the database")

	return s
}

func (s *StrategiesStage) the_response_contains_validation_errors() *StrategiesStage {
	respErr := response.ErrorResponse{}
	err := json.Unmarshal(s.body, &respErr)
	s.require.NoError(err, "unmarshal validation errors response")

	vErrs := validators.ValidationErrors{}
	err = json.Unmarshal(respErr.Details, &vErrs)
	s.require.NoErrorf(err, "error response does not contain validation error details, response:\n%s\n%#v",
		string(s.body), respErr.Details)
	s.require.Len(vErrs, 2, "unexpected number of validation errors")

	expValidationErrors := validators.ValidationErrors{
		{
			Field: "name",
			Err:   validators.ErrorMessage_Required,
			Value: s.expStrat.Name,
		},
		{
			Field: "Laptime",
			Param: "9999",
			Err:   fmt.Sprintf(validators.ErrorMessage_GreaterThan, "9999"),
			Value: float64(s.expStrat.Data.RaceInfo.Laptime),
		},
	}
	s.require.EqualValues(expValidationErrors, vErrs, "unexpected validation errors")

	return s
}

func (s *StrategiesStage) the_response_contains_the_adjusted_strategy() *StrategiesStage {
	strategy := entities.Strategy{}
	err := json.Unmarshal(s.body, &strategy)
	s.require.NoError(err, "unmarshal strategy detail response")

	s.require.EqualValues(s.expProgress, strategy.Progress)
	s.require.EqualValues(s.expStrat.Data.PitStrategy.Stints, strategy.Data.PitStrategy.Stints)

	return s
}

func (s *StrategiesStage) the_strategy_remains_unaltered_in_main_table() *StrategiesStage {
	row := s.dbPool.QueryRow(context.Background(), `SELECT s.data FROM strategies s WHERE s.id = $1`, s.strategyID)

	var data json.RawMessage
	var stratData entities.StrategyData

	err := row.Scan(&data)
	s.require.NoError(err, "scan strategy data")

	err = json.Unmarshal(data, &stratData)
	s.require.NoError(err, "unmarshal strategy data")

	s.require.EqualValues(testdata.TestStrategy.Data.PitStrategy.Stints,
		stratData.PitStrategy.Stints, "base strategy stints have been tampered")

	return s
}

func (s *StrategiesStage) the_response_contains_the_saved_strategies() *StrategiesStage {
	response := infraHttp.PaginatedItemsResponse{}
	err := json.Unmarshal(s.body, &response)
	s.require.NoError(err, "unmarshal retrieved paginated response")

	strats := []entities.Strategy{}
	err = json.Unmarshal(response.Items, &strats)
	s.require.NoError(err, "unmarshal saved strategies response")

	s.require.Equal(1, len(strats), "unexpected number of strategies in response")

	return s
}
