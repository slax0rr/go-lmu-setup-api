package tests

import (
	"net/http"
	"testing"
)

func TestStrategy_GetDetails_HappyPath1(t *testing.T) {
	given, when, then := NewStrategiesStage(t)

	given.a_strategy_record_in_the_database().and().
		a_request_for_retrieving_strategy_details().and().
		with_a_valid_and_existing_strategy_id()

	when.the_request_is_executed()

	then.the_response_status_is_ok().and().
		the_strategy_details_are_in_expected_format().and().
		the_passphrase_is_empty()
}

func TestStrategy_GetDetails_WithAdjustedStrategy_HappyPath2(t *testing.T) {
	given, when, then := NewStrategiesStage(t)

	given.a_strategy_record_in_the_database().and().
		strategy_adjusted_for_progress_stored_in_the_database().and().
		a_request_for_retrieving_strategy_details().and().
		with_a_valid_and_existing_strategy_id()

	when.the_request_is_executed()

	then.the_response_status_is_ok().and().
		the_strategy_details_are_in_expected_format().and().
		the_response_contains_the_adjusted_strategy()
}

func TestStrategy_GetPassphrase_HappyPath(t *testing.T) {
	given, when, then := NewStrategiesStage(t)

	given.a_strategy_record_in_the_database().and().
		a_request_for_retrieving_strategy_details().and().
		with_a_valid_and_existing_strategy_id().and().
		a_valid_user_token()

	when.the_request_is_executed()

	then.the_response_status_is_ok().and().
		the_passphrase_is_received_in_response()
}

func TestStrategy_GetDetailsWithInvalidID_UnhappyPath1(t *testing.T) {
	given, when, then := NewStrategiesStage(t)

	given.a_request_for_retrieving_strategy_details().
		with_an_invalid_strategy_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusBadRequest).and().
		error_response_details_indicate_invalid_id()
}

func TestStrategy_GetDetailsOfNonExistingStrategy_UnhappyPath2(t *testing.T) {
	given, when, then := NewStrategiesStage(t)

	given.a_request_for_retrieving_strategy_details().
		with_a_valid_but_nonexisting_strategy_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusNotFound)
}

func TestStrategy_Save_HappyPath(t *testing.T) {
	given, when, then := NewStrategiesStage(t)

	given.a_request_for_saving_a_strategy().and().
		a_valid_strategy_in_the_request().and().
		a_valid_user_token()

	when.the_request_is_executed()

	then.the_response_status_is_ok().and().
		the_created_strategy_id_is_in_the_response_body()
}

func TestStrategy_Save_Unauthorized_UnhappyPath1(t *testing.T) {
	given, when, then := NewStrategiesStage(t)

	given.a_request_for_saving_a_strategy().and().
		a_valid_strategy_in_the_request().and().
		a_valid_user_token_with_empty_data()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusUnauthorized)
}

func TestStrategy_Save_WithInvalidPayload_UnhappyPath2(t *testing.T) {
	given, when, then := NewStrategiesStage(t)

	given.a_request_for_saving_a_strategy().and().
		an_invalid_strategy_in_the_request().and().
		a_valid_user_token()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusBadRequest).and().
		the_response_contains_validation_errors()
}

func TestStrategy_GetUserStrategy_HappyPath(t *testing.T) {
	given, when, then := NewStrategiesStage(t)

	given.a_strategy_record_in_the_database().and().
		a_request_for_retrieving_user_saved_strategies().and().
		a_valid_user_token()

	when.the_request_is_executed()

	then.the_response_status_is_ok().and().
		the_response_contains_the_saved_strategies()
}

func TestStrategy_AdjustWithProgress_Owner_HappyPath1(t *testing.T) {
	given, when, then := NewStrategiesStage(t)

	given.a_strategy_record_in_the_database().and().
		a_request_to_adjust_the_saved_strategy_with_progress().and().
		valid_race_progress_data_in_the_request().and().
		a_valid_user_token()

	when.the_request_is_executed()

	then.the_response_status_is_ok().and().
		the_response_contains_the_adjusted_strategy().and().
		the_strategy_remains_unaltered_in_main_table()
}

func TestStrategy_AdjustWithProgress_Passphrase_HappyPath2(t *testing.T) {
	given, when, then := NewStrategiesStage(t)

	given.a_strategy_record_in_the_database().and().
		a_request_to_adjust_the_saved_strategy_with_progress().and().
		valid_race_progress_data_in_the_request().and().
		the_correct_passphrase()

	when.the_request_is_executed()

	then.the_response_status_is_ok().and().
		the_response_contains_the_adjusted_strategy().and().
		the_strategy_remains_unaltered_in_main_table()
}
