package tests

import (
	"net/http"
	"testing"
)

func TestCars_List_HappyPath(t *testing.T) {
	given, when, then := NewCarsStage(t)

	given.a_request_for_listing_cars()

	when.the_request_is_executed()

	then.the_response_status_is_ok().and().
		all_cars_are_listed()
}

func TestCars_GetDetails_HappyPath(t *testing.T) {
	given, when, then := NewCarsStage(t)

	given.a_request_for_retrieving_car_details().
		with_a_valid_and_existing_car_id()

	when.the_request_is_executed()

	then.the_response_status_is_ok().and().
		the_car_details_are_in_expected_format()
}

func TestCars_GetDetailsWithInvalidID_UnhappyPath1(t *testing.T) {
	given, when, then := NewCarsStage(t)

	given.a_request_for_retrieving_car_details().
		with_an_invalid_car_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusBadRequest).and().
		error_response_details_indicate_invalid_id()
}

func TestCars_GetDetailsOfNonExistingCar_UnhappyPath2(t *testing.T) {
	given, when, then := NewCarsStage(t)

	given.a_request_for_retrieving_car_details().
		with_a_valid_but_nonexisting_car_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusNotFound)
}
