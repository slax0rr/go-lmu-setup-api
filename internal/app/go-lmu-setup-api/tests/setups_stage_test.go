package tests

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand/v2"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/stretchr/testify/require"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/constants"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	infraHttp "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/settings"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/tests/testsupport"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/response"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/validators"
)

type SetupsStage struct {
	t       *testing.T
	require *require.Assertions

	dbPool *pgxpool.Pool

	payload []byte
	req     *http.Request
	resp    *http.Response
	body    []byte

	setup              *entities.Setup
	upvotes, downvotes float64

	reqSetup    *entities.Setup
	reqUser     *entities.User
	userID      uuid.UUID
	filecontent []byte

	expSetupFileCount int
	expSetup          *entities.Setup
	expSetupCount     int
	expVldErrors      []testsupport.ExpectedValidationError
}

func NewSetupsStage(t *testing.T) (*SetupsStage, *SetupsStage, *SetupsStage) {
	s := &SetupsStage{
		t: t,
	}

	s.require = require.New(s.t)

	var err error
	s.dbPool, err = pgxpool.New(context.Background(), testsupport.DatabaseDSN)
	s.require.NoError(err, "create new postgres db pool")

	s._populateSetups(context.Background())

	return s, s, s
}

func (s *SetupsStage) _populateSetups(ctx context.Context) {
	row := s.dbPool.QueryRow(ctx, fmt.Sprintf("select id from cars limit 1 offset %d", rand.IntN(20)))
	var carID uuid.UUID
	err := row.Scan(&carID)
	s.require.NoError(err, "retrieve car id")

	row = s.dbPool.QueryRow(ctx, fmt.Sprintf("select id from tracks limit 1 offset %d", rand.IntN(9)))
	var trackID uuid.UUID
	err = row.Scan(&trackID)
	s.require.NoError(err, "retrieve track id")

	userID := uuid.New()
	if s.userID != uuid.Nil {
		userID = s.userID
	}

	desc := "setup description"
	s.setup = &entities.Setup{
		CarID:       carID,
		TrackID:     trackID,
		FileName:    "file_name.svm",
		Laptime:     "1:11:000",
		Type:        entities.SetupTypeEndurance,
		Condition:   entities.SetupConditionDamp,
		Rating:      0,
		Description: &desc,
		Downloads:   10,
		VideoLink:   "https://youtube.com/",
		Version:     "1.10",
		UserID:      userID,
		User:        "user",
	}

	row = s.dbPool.QueryRow(ctx, `
insert into setups
	(car_id, track_id, track_layout_id, file_name, laptime, type, condition, description, downloads, video_link, version, user_id, "user")
values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) returning id, created_at`,
		s.setup.CarID,
		s.setup.TrackID,
		s.setup.TrackLayoutID,
		s.setup.FileName,
		s.setup.Laptime,
		s.setup.Type,
		s.setup.Condition,
		s.setup.Description,
		s.setup.Downloads,
		s.setup.VideoLink,
		s.setup.Version,
		s.setup.UserID,
		s.setup.User,
	)
	err = row.Scan(&s.setup.ID, &s.setup.CreatedAt)
	s.require.NoError(err, "insert test setup")

	s._addInitialRatings(ctx)
}

func (s *SetupsStage) _addInitialRatings(ctx context.Context) {
	votes := []bool{true, true, false, false, true, true, false}

	for _, vote := range votes {
		_, err := s.dbPool.Exec(ctx, `
insert into "setup_rating" (setup_id, user_id, upvote) values ($1, $2, $3)
`, s.setup.ID, uuid.New(), vote)
		s.require.NoError(err, "create initial setup rating")
		if vote {
			s.upvotes += 1
		} else {
			s.downvotes += 1
		}
	}

	s.setup.Rating = int64(s.upvotes / (s.upvotes + s.downvotes) * 1000)
}

func (s *SetupsStage) and() *SetupsStage {
	return s
}

func (s *SetupsStage) a_request_for_listing_setups() *SetupsStage {
	var err error
	s.req, err = http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1/setups",
		testsupport.BaseURL), nil)
	s.require.NoError(err, "build get /setups request")
	return s
}

func (s *SetupsStage) a_request_for_listing_user_saved_setups() *SetupsStage {
	var err error
	s.req, err = http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1/setups/me",
		testsupport.BaseURL), nil)
	s.require.NoError(err, "build get /setups/me request")
	return s
}

func (s *SetupsStage) a_request_for_retrieving_setup_details() *SetupsStage {
	// same URL is used for both, param is to be added a separate function
	return s.a_request_for_listing_setups()
}

func (s *SetupsStage) a_request_for_retrieving_the_setup_file() *SetupsStage {
	var err error
	s.req, err = http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1/setups/download",
		testsupport.BaseURL), nil)
	s.require.NoError(err, "build get /setups/download request")
	return s
}

func (s *SetupsStage) a_request_for_adding_a_setup() *SetupsStage {
	var err error
	s.req, err = http.NewRequest(http.MethodPost, fmt.Sprintf("%s/api/v1/setups",
		testsupport.BaseURL), nil)
	s.require.NoError(err, "build post /setups request")
	return s
}

func (s *SetupsStage) a_request_for_updating_a_setup() *SetupsStage {
	var err error
	s.req, err = http.NewRequest(http.MethodPut, fmt.Sprintf("%s/api/v1/setups",
		testsupport.BaseURL), nil)
	s.require.NoError(err, "build put /setups request")
	return s
}

func (s *SetupsStage) a_request_for_upvoting_a_setup() *SetupsStage {
	var err error
	s.req, err = http.NewRequest(http.MethodPost, fmt.Sprintf("%s/api/v1/setups/rate/upvote",
		testsupport.BaseURL), nil)
	s.require.NoError(err, "build put /setups/rate/upvote request")
	s.upvotes += 1
	return s
}

func (s *SetupsStage) with_a_setup_id(id string) *SetupsStage {
	var err error
	s.req.URL.Path, err = url.JoinPath(s.req.URL.Path, fmt.Sprintf("/%s", id))
	s.require.NoError(err, "attach setup id to get setup detail url")
	return s
}

func (s *SetupsStage) with_a_valid_and_existing_setup_id() *SetupsStage {
	row := s.dbPool.QueryRow(context.Background(), "select user_id from setups where id = $1 limit 1", s.setup.ID)
	err := row.Scan(&s.userID)
	s.require.NoError(err, "retrieve a setups id and user_id from the database")

	row = s.dbPool.QueryRow(context.Background(), "select count(id) from setup_files where setup_id = $1", s.setup.ID)
	err = row.Scan(&s.expSetupFileCount)
	s.require.NoError(err, "get setup file count from the database")

	return s.with_a_setup_id(s.setup.ID.String())
}

func (s *SetupsStage) a_user_created_setup() *SetupsStage {
	return s.n_created_user_setups(1)
}

func (s *SetupsStage) n_created_user_setups(n int) *SetupsStage {
	s.expSetupCount = n

	if s.userID == uuid.Nil {
		s.userID = uuid.New()
	}

	for range n {
		s._populateSetups(context.Background())
	}
	return s
}

func (s *SetupsStage) with_an_invalid_setup_id() *SetupsStage {
	return s.with_a_setup_id("invalid id")
}

func (s *SetupsStage) with_a_valid_but_nonexisting_setup_id() *SetupsStage {
	return s.with_a_setup_id(uuid.New().String())
}

func (s *SetupsStage) the_setup_file_will_exist() *SetupsStage {
	s.filecontent = []byte("test setup file")
	_, err := s.dbPool.Exec(context.Background(),
		`insert into setup_files (setup_id, file_name, content, updated_at) values ($1,$2,$3,$4)`,
		s.setup.ID, s.setup.FileName, s.filecontent, time.Now().UTC())
	s.require.NoError(err, "store setup file")
	s.expSetupFileCount += 1
	return s
}

func (s *SetupsStage) with_a_valid_setup() *SetupsStage {
	desc := "setup description"
	s.reqSetup = &entities.Setup{
		CarID:       s.setup.CarID,
		TrackID:     s.setup.TrackID,
		FileName:    "new_file_name.svm",
		FileContent: "new setup file content",
		Laptime:     "1:11:000",
		Type:        entities.SetupTypeEndurance,
		Condition:   entities.SetupConditionDamp,
		Rating:      0,
		Description: &desc,
		Downloads:   0,
		VideoLink:   "https://youtube.com/",
		Version:     "1.10",
	}
	s.filecontent = []byte(s.reqSetup.FileContent)
	s.expSetupFileCount += 1

	return s
}

func (s *SetupsStage) without_laptime() *SetupsStage {
	s.reqSetup.Laptime = ""
	return s
}

func (s *SetupsStage) with_a_valid_setup_for_update() *SetupsStage {
	desc := "updated setup description"
	s.reqSetup = &entities.Setup{
		CarID:       s.setup.CarID,
		TrackID:     s.setup.TrackID,
		FileName:    "new_file_name_updated.svm",
		FileContent: "new setup file updated content",
		Laptime:     "2:22:000",
		Type:        entities.SetupTypeEndurance,
		Condition:   entities.SetupConditionDamp,
		Description: &desc,
		VideoLink:   "https://youtube.com/updated",
		Version:     "1.10",
	}
	s.filecontent = []byte(s.reqSetup.FileContent)
	s.expSetupFileCount += 1

	return s
}

func (s *SetupsStage) with_an_invalid_setup_payload() *SetupsStage {
	s.with_a_valid_setup()
	s.reqSetup.Laptime = "invalid laptime"
	s.expVldErrors = []testsupport.ExpectedValidationError{
		{
			Field: "laptime",
			Msg:   validators.ErrorMessage_Laptime,
			Value: s.reqSetup.Laptime,
		},
	}
	return s
}

func (s *SetupsStage) with_empty_file_contents_in_setup_payload() *SetupsStage {
	s.with_a_valid_setup()
	s.reqSetup.FileContent = ""
	s.expVldErrors = []testsupport.ExpectedValidationError{
		{
			Field: "file_content",
			Msg:   fmt.Sprintf(validators.ErrorMessage_RequiredWithout, "ID"),
			Value: "",
		},
	}
	return s
}

func (s *SetupsStage) file_content_is_empty_in_the_request() *SetupsStage {
	s.reqSetup.FileContent = ""
	s.filecontent = []byte{}
	s.expSetupFileCount -= 1
	return s
}

func (s *SetupsStage) a_valid_user_token() *SetupsStage {
	userID := uuid.New()
	if s.userID != uuid.Nil {
		userID = s.userID
	}
	s.reqUser = &entities.User{
		ID:          userID,
		Username:    "test_user",
		DisplayName: "new_setup_user",
	}

	token, err := testsupport.GenerateUserToken(s.reqUser, time.Now().Add(10*time.Minute))
	s.require.NoError(err, "signing of user data token failed")

	s.req.Header.Add(settings.UserTokenHeader, token)
	return s
}

func (s *SetupsStage) an_expired_user_token() *SetupsStage {
	userID := uuid.New()
	if s.userID != uuid.Nil {
		userID = s.userID
	}
	s.reqUser = &entities.User{
		ID:          userID,
		Username:    "test_user",
		DisplayName: "new_setup_user",
	}

	token, err := testsupport.GenerateUserToken(s.reqUser, time.Now().Add(-10*time.Minute))
	s.require.NoError(err, "signing of user data token failed")

	s.req.Header.Add(settings.UserTokenHeader, token)
	return s
}

func (s *SetupsStage) a_valid_user_token_with_empty_data() *SetupsStage {
	s.req.Header.Add(settings.UserTokenHeader, "")
	return s
}

func (s *SetupsStage) a_setup_with_the_same_filename_exists() *SetupsStage {
	row := s.dbPool.QueryRow(context.Background(), `
insert into setups
    (car_id, track_id, track_layout_id, file_name, laptime, type, condition, description, downloads, video_link, version, user_id, "user")
values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) returning id`,
		s.expSetup.CarID,
		s.expSetup.TrackID,
		s.expSetup.TrackLayoutID,
		s.expSetup.FileName,
		s.expSetup.Laptime,
		s.expSetup.Type,
		s.expSetup.Condition,
		s.expSetup.Description,
		s.expSetup.Downloads,
		s.expSetup.VideoLink,
		s.expSetup.Version,
		s.reqUser.ID,
		s.reqUser.DisplayName,
	)
	id := ""
	err := row.Scan(&id)
	s.require.NoError(err, "insert new setup for duplicate check")

	_, err = s.dbPool.Exec(context.Background(), `
insert into setup_files
    (setup_id, file_name, content)
values ($1, $2, $3)`,
		id,
		s.expSetup.FileName,
		s.expSetup.FileContent,
	)
	s.require.NoError(err, "storing new setup for duplicate check")

	return s
}

func (s *SetupsStage) the_setup_data_is_added_to_the_payload() *SetupsStage {
	if s.reqSetup != nil {
		s.expSetup = new(entities.Setup)
		(*s.expSetup) = *s.reqSetup
	}

	payload, err := json.Marshal(s.expSetup)
	s.require.NoError(err, "marshal new setup")

	s.req.Body = io.NopCloser(bytes.NewBuffer(payload))

	return s
}

func (s *SetupsStage) the_user_has_recorded_an_upvote() *SetupsStage {
	_, err := s.dbPool.Exec(context.Background(), `
insert into "setup_rating" (setup_id, user_id, upvote) values ($1, $2, $3)
`, s.setup.ID, s.reqUser.ID, true)
	s.require.NoError(err, "create initial setup rating")
	s.upvotes += 1
	return s
}

func (s *SetupsStage) the_request_is_executed() *SetupsStage {
	var err error
	s.resp, err = http.DefaultClient.Do(s.req)
	s.require.NoError(err, "execute http request")
	defer s.resp.Body.Close()

	s.body, err = ioutil.ReadAll(s.resp.Body)
	s.require.NoError(err, "read response body")
	return s
}

func (s *SetupsStage) the_response_status_is(status int) *SetupsStage {
	s.require.Equal(status, s.resp.StatusCode, "unexpected response status code")
	return s
}

func (s *SetupsStage) all_setups_are_listed() *SetupsStage {
	userID := uuid.Nil
	if s.reqUser != nil {
		userID = s.reqUser.ID
	}

	rows, err := s.dbPool.Query(context.Background(), `
select
  s.id, s.car_id, s.track_id, s.track_layout_id, s.file_name, s.laptime, s.type, s.condition,
  (select count(setup_id) from setup_rating sr where sr.setup_id = s.id and upvote = true) as upvotes,
  (select count(setup_id) from setup_rating sr where sr.setup_id = s.id and upvote = false) as downvotes,
  s.description, s.downloads, s.video_link, s.version, s.user_id, s."user", s.created_at, sr.upvote as user_vote
from setups s
left outer join setup_rating sr on s.id = sr.setup_id and sr.user_id = $1
`, userID)
	s.require.NoError(err, "fetch all setups from the database")

	var upvotes, downvotes float64
	dbSetups := entities.SetupList{}
	for rows.Next() {
		setup := entities.Setup{}
		rows.Scan(
			&setup.ID,
			&setup.CarID,
			&setup.TrackID,
			&setup.TrackLayoutID,
			&setup.FileName,
			&setup.Laptime,
			&setup.Type,
			&setup.Condition,
			&upvotes,
			&downvotes,
			&setup.Description,
			&setup.Downloads,
			&setup.VideoLink,
			&setup.Version,
			&setup.UserID,
			&setup.User,
			&setup.CreatedAt,
			&setup.UserVote,
		)
		if upvotes+downvotes > 0 {
			setup.Rating = int64(upvotes / (upvotes + downvotes) * 1000)
		}
		dbSetups = append(dbSetups, setup)
	}

	response := infraHttp.PaginatedItemsResponse{}
	err = json.Unmarshal(s.body, &response)
	s.require.NoError(err, "unmarshal retrieved paginated response")

	apiSetups := entities.SetupList{}
	err = json.Unmarshal(response.Items, &apiSetups)
	s.require.NoError(err, "unmarshal retrieved setups list")

	s.require.EqualValues(dbSetups, apiSetups)

	return s
}

func (s *SetupsStage) all_user_created_setups_are_listed() *SetupsStage {
	rows, err := s.dbPool.Query(context.Background(), `
select
  s.id, s.car_id, s.track_id, s.track_layout_id, s.file_name, s.laptime, s.type, s.condition,
  (select count(setup_id) from setup_rating sr where sr.setup_id = s.id and upvote = true) as upvotes,
  (select count(setup_id) from setup_rating sr where sr.setup_id = s.id and upvote = false) as downvotes,
  s.description, s.downloads, s.video_link, s.version, s.user_id, s."user", s.created_at, sr.upvote as user_vote
from setups s
left outer join setup_rating sr on s.id = sr.setup_id and sr.user_id = $1
where s.user_id = $1`, s.userID)
	s.require.NoError(err, "fetch all setups from the database")

	var upvotes, downvotes float64
	dbSetups := entities.SetupList{}
	for rows.Next() {
		setup := entities.Setup{}
		rows.Scan(
			&setup.ID,
			&setup.CarID,
			&setup.TrackID,
			&setup.TrackLayoutID,
			&setup.FileName,
			&setup.Laptime,
			&setup.Type,
			&setup.Condition,
			&upvotes,
			&downvotes,
			&setup.Description,
			&setup.Downloads,
			&setup.VideoLink,
			&setup.Version,
			&setup.UserID,
			&setup.User,
			&setup.CreatedAt,
			&setup.UserVote,
		)
		if upvotes+downvotes > 0 {
			setup.Rating = int64(upvotes / (upvotes + downvotes) * 1000)
		}
		dbSetups = append(dbSetups, setup)
	}

	response := infraHttp.PaginatedItemsResponse{}
	err = json.Unmarshal(s.body, &response)
	s.require.NoError(err, "unmarshal retrieved paginated response")

	apiSetups := entities.SetupList{}
	err = json.Unmarshal(response.Items, &apiSetups)
	s.require.NoError(err, "unmarshal retrieved setups list")

	s.require.EqualValues(dbSetups, apiSetups)

	return s
}

func (s *SetupsStage) results_are_paginated() *SetupsStage {
	response := infraHttp.PaginatedItemsResponse{}
	err := json.Unmarshal(s.body, &response)
	s.require.NoError(err, "unmarshal retrieved paginated response")

	s.require.Greater(response.Pagination.Limit, uint64(0), "pagination limit is supposed to be greater than 0")

	return s
}

func (s *SetupsStage) the_setup_details_are_in_expected_format() *SetupsStage {
	setup := &entities.Setup{}
	err := json.Unmarshal(s.body, &setup)
	s.require.NoError(err, "unmarshal setup detail response")
	s.require.EqualValues(s.setup, setup)
	return s
}

func (s *SetupsStage) the_setup_file_is_downloaded() *SetupsStage {
	s.require.Equal(fmt.Sprintf(`attachment; filename="%s"`, s.setup.FileName),
		s.resp.Header.Get("Content-Disposition"))

	s.require.Equal(s.filecontent, s.body)
	return s
}

func (s *SetupsStage) the_updated_setup_is_stored_in_the_database() *SetupsStage {
	s.expSetup.Rating = s.setup.Rating
	s.expSetup.Downloads = s.setup.Downloads
	return s.the_setup_is_stored_in_the_database()
}

func (s *SetupsStage) the_setup_is_stored_in_the_database() *SetupsStage {
	var addedSetupResponse map[string]interface{}
	err := json.Unmarshal(s.body, &addedSetupResponse)
	s.require.NoError(err, "unmarshal add setup response")

	id, ok := addedSetupResponse["id"].(string)
	s.require.True(ok, "response did not provide a new setup id")

	s.expSetup.ID = uuid.MustParse(id)
	s.expSetup.UserID = s.reqUser.ID
	s.expSetup.User = s.reqUser.DisplayName
	s.expSetup.FileContent = ""

	row := s.dbPool.QueryRow(context.Background(), `
select
  id, car_id, track_id, file_name, laptime, type, condition,
  (select count(setup_id) from setup_rating sr where sr.setup_id = s.id and upvote = true) as upvotes,
  (select count(setup_id) from setup_rating sr where sr.setup_id = s.id and upvote = false) as downvotes,
  description, downloads, video_link, version, user_id, "user"
from setups s
where id = $1`, id)

	var upvotes, downvotes float64
	setup := entities.Setup{}
	err = row.Scan(
		&setup.ID,
		&setup.CarID,
		&setup.TrackID,
		&setup.FileName,
		&setup.Laptime,
		&setup.Type,
		&setup.Condition,
		&upvotes,
		&downvotes,
		&setup.Description,
		&setup.Downloads,
		&setup.VideoLink,
		&setup.Version,
		&setup.UserID,
		&setup.User,
	)
	s.require.NoError(err, "get newly added setup from database")
	if upvotes+downvotes > 0 {
		setup.Rating = int64(upvotes / (upvotes + downvotes) * 1000)
	}

	s.require.EqualValues(s.expSetup, &setup)
	return s
}

func (s *SetupsStage) setup_file_is_added_to_the_database() *SetupsStage {
	files := []entities.SetupFile{}

	rows, err := s.dbPool.Query(context.Background(), `
select
  id, setup_id, file_name, content, created_at, updated_at
from setup_files
where setup_id = $1
order by updated_at desc
`, s.expSetup.ID)
	s.require.NoError(err, "query setup files")

	for rows.Next() {
		file := entities.SetupFile{}
		err := rows.Scan(&file.ID, &file.SetupID, &file.Name, &file.Content, &file.CreatedAt, &file.UpdatedAt)
		s.require.NoError(err, "scan setup file")

		files = append(files, file)
	}

	if err := rows.Err(); err != nil {
		s.require.NoError(err, "setup file scanner error")
	}

	s.require.Equal(s.expSetupFileCount, len(files), "unexpected amount of setup files")

	if len(files) > 0 && len(s.filecontent) > 0 {
		s.require.Equal(s.filecontent, files[0].Content, "unexpected setup file content")
	}

	return s
}

func (s *SetupsStage) error_response_details_indicate_invalid_id() *SetupsStage {
	respErr := response.Error{}
	err := json.Unmarshal(s.body, &respErr)
	s.require.NoError(err, "unmarshal response error")
	s.require.Equal(constants.ErrorDetails_MissingOrMalformedResourceID, respErr.Details)
	return s
}

func (s *SetupsStage) error_response_details_indicate_missing_file() *SetupsStage {
	respErr := response.Error{}
	err := json.Unmarshal(s.body, &respErr)
	s.require.NoError(err, "unmarshal response error")
	s.require.Equal(constants.ErrorDetails_SetupFileMissing, respErr.Details)
	return s
}

func (s *SetupsStage) the_response_contains_validation_errors() *SetupsStage {
	respErr := response.ErrorResponse{}
	err := json.Unmarshal(s.body, &respErr)
	s.require.NoError(err, "unmarshal validation errors response")

	vErrs := validators.ValidationErrors{}
	err = json.Unmarshal(respErr.Details, &vErrs)
	s.require.NoErrorf(err, "error response does not contain validation error details, response:\n%s\n%#v",
		string(s.body), respErr.Details)
	s.require.Len(vErrs, 1, "unexpected number of validation errors")

	s.require.Equal(len(s.expVldErrors), len(vErrs), "unexpected number of validation errors")

	for i, vErr := range vErrs {
		expErr := s.expVldErrors[i]

		s.require.Equal(expErr.Field, vErr.Field, "unexpected validation error field")
		s.require.Equal(expErr.Msg, vErr.Err,
			"unexpected validation error message")
		s.require.Equal(expErr.Value, vErr.Value, "unexpected validation value")
	}

	return s
}

func (s *SetupsStage) the_new_rating_is_in_the_response() *SetupsStage {
	rating := int64(s.upvotes / (s.upvotes + s.downvotes) * 1000)
	resp := map[string]int64{}
	err := json.Unmarshal(s.body, &resp)
	s.require.NoError(err, "unmarshal rating response")
	s.require.Equal(rating, resp["rating"])
	return s
}

func (s *SetupsStage) the_rating_is_updated() *SetupsStage {
	rating := int64(s.upvotes / (s.upvotes + s.downvotes) * 1000)
	row := s.dbPool.QueryRow(context.Background(), `
select
  (select count(setup_id) from setup_rating sr where sr.setup_id = s.id and upvote = true) as upvotes,
  (select count(setup_id) from setup_rating sr where sr.setup_id = s.id and upvote = false) as downvotes
from setups s
where id = $1`, s.setup.ID)

	var upvotes, downvotes float64
	setup := entities.Setup{}
	err := row.Scan(
		&upvotes,
		&downvotes,
	)
	s.require.NoError(err, "get updated ratings for the setup")
	if upvotes+downvotes > 0 {
		setup.Rating = int64(upvotes / (upvotes + downvotes) * 1000)
	}

	s.require.Equal(rating, setup.Rating, "rating does not match after the update")
	return s
}
