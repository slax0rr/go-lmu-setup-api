package tests

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/stretchr/testify/require"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/constants"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/tests/testsupport"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/response"
)

type TrackLayoutsStage struct {
	t       *testing.T
	require *require.Assertions

	dbPool *pgxpool.Pool

	payload []byte
	req     *http.Request
	resp    *http.Response
	body    []byte

	track_layoutID uuid.UUID
}

func NewTrackLayoutsStage(t *testing.T) (*TrackLayoutsStage, *TrackLayoutsStage, *TrackLayoutsStage) {
	s := &TrackLayoutsStage{
		t: t,
	}

	s.require = require.New(s.t)

	var err error
	s.dbPool, err = pgxpool.New(context.Background(), testsupport.DatabaseDSN)
	s.require.NoError(err, "create new postgres db pool")

	return s, s, s
}

func (s *TrackLayoutsStage) and() *TrackLayoutsStage {
	return s
}

func (s *TrackLayoutsStage) a_request_for_listing_track_layouts() *TrackLayoutsStage {
	var err error
	s.req, err = http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1/tracks/layouts",
		testsupport.BaseURL), nil)
	s.require.NoError(err, "build get /tracks/layouts request")
	return s
}

func (s *TrackLayoutsStage) a_request_for_retrieving_track_layout_details() *TrackLayoutsStage {
	// same URL is used for both, param is to be added a separate function
	return s.a_request_for_listing_track_layouts()
}

func (s *TrackLayoutsStage) with_a_track_layout_id(id string) *TrackLayoutsStage {
	var err error
	s.req.URL.Path, err = url.JoinPath(s.req.URL.Path, fmt.Sprintf("/%s", id))
	s.require.NoError(err, "attach track_layout id to get track layout detail url")
	return s
}

func (s *TrackLayoutsStage) with_a_valid_and_existing_track_layout_id() *TrackLayoutsStage {
	row := s.dbPool.QueryRow(context.Background(), "select id from track_layouts limit 1")
	err := row.Scan(&s.track_layoutID)
	s.require.NoError(err, "retrieve a track layout id from the database")

	return s.with_a_track_layout_id(s.track_layoutID.String())
}

func (s *TrackLayoutsStage) with_an_invalid_track_layout_id() *TrackLayoutsStage {
	return s.with_a_track_layout_id("invalid id")
}

func (s *TrackLayoutsStage) with_a_valid_but_nonexisting_track_layout_id() *TrackLayoutsStage {
	return s.with_a_track_layout_id(uuid.New().String())
}

func (s *TrackLayoutsStage) the_request_is_executed() *TrackLayoutsStage {
	var err error
	s.resp, err = http.DefaultClient.Do(s.req)
	s.require.NoError(err, "execute http request")
	defer s.resp.Body.Close()

	s.body, err = ioutil.ReadAll(s.resp.Body)
	s.require.NoError(err, "read response body")
	return s
}

func (s *TrackLayoutsStage) the_response_status_is(status int) *TrackLayoutsStage {
	s.require.Equal(status, s.resp.StatusCode, "response status unexpected")
	return s
}

func (s *TrackLayoutsStage) the_response_status_is_ok() *TrackLayoutsStage {
	return s.the_response_status_is(http.StatusOK)
}

func (s *TrackLayoutsStage) all_track_layouts_are_listed() *TrackLayoutsStage {
	rows, err := s.dbPool.Query(context.Background(),
		"select id, track_id, name, default_layout from track_layouts")
	s.require.NoError(err, "fetch all track layouts from the database")

	dbTrackLayouts := entities.TrackLayoutList{}
	for rows.Next() {
		layout := entities.TrackLayout{}
		rows.Scan(
			&layout.ID,
			&layout.TrackID,
			&layout.Name,
			&layout.Default,
		)
		dbTrackLayouts = append(dbTrackLayouts, layout)
	}

	apiTrackLayouts := entities.TrackLayoutList{}
	err = json.Unmarshal(s.body, &apiTrackLayouts)
	s.require.NoError(err, "unmarshal retrieved track_layouts list")

	s.require.EqualValues(dbTrackLayouts, apiTrackLayouts)

	return s
}

func (s *TrackLayoutsStage) the_track_layout_details_are_in_expected_format() *TrackLayoutsStage {
	track_layout := entities.TrackLayout{}
	err := json.Unmarshal(s.body, &track_layout)
	s.require.NoError(err, "unmarshal track layout detail response")
	s.require.Equal(s.track_layoutID.String(), track_layout.ID.String())
	return s
}

func (s *TrackLayoutsStage) error_response_details_indicate_invalid_id() *TrackLayoutsStage {
	respErr := response.Error{}
	err := json.Unmarshal(s.body, &respErr)
	s.require.NoError(err, "unmarshal response error")
	s.require.Equal(constants.ErrorDetails_MissingOrMalformedResourceID, respErr.Details)
	return s
}
