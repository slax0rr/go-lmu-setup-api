package data

import (
	"time"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
)

var (
	TestStrategy *entities.Strategy = &entities.Strategy{
		Passphrase: "random passphrase",
		Name:       "Test strategy",
		Data: entities.StrategyData{
			Laps:     60,
			SafeLaps: 61,
			MinRequired: entities.FuelReq{
				Fuel:   30000,
				Energy: 31200,
			},
			SafeRequired: entities.FuelReq{
				Fuel:   30500,
				Energy: 31800,
			},
			FuelRatio: 1,
			RaceInfo: entities.RaceInfo{
				FuelUse:   500,
				EnergyUse: 520,
				RaceTime:  (90 * time.Minute).Milliseconds(),
				Laptime:   (1*time.Minute + 29*time.Second).Milliseconds(),
				PitLength: (45 * time.Second).Milliseconds(),
			},
			PitStrategy: entities.PitStrat{
				Type:  entities.StratType_NoStop,
				Stops: 0,
				Stints: []entities.Stint{
					{
						StintNumber: 1,
						Laps:        21,
						Save:        2,
						TargetUse: entities.FuelReq{
							Fuel:   459,
							Energy: 477,
						},
						StintRequired: entities.FuelReq{
							Fuel:   9700,
							Energy: 10000,
						},
						FuelRatio: 97,
						PitLength: 0,
					},
					{
						StintNumber: 2,
						Laps:        20,
						Save:        1,
						TargetUse: entities.FuelReq{
							Fuel:   481,
							Energy: 500,
						},
						StintRequired: entities.FuelReq{
							Fuel:   9700,
							Energy: 10000,
						},
						FuelRatio: 97,
						PitLength: 45000,
					},
					{
						StintNumber: 3,
						Laps:        20,
						Save:        1,
						TargetUse: entities.FuelReq{
							Fuel:   481,
							Energy: 500,
						},
						StintRequired: entities.FuelReq{
							Fuel:   9700,
							Energy: 10000,
						},
						FuelRatio: 97,
						PitLength: 45000,
					},
				},
			},
		},
	}

	TestRaceProgress *entities.RaceProgress = &entities.RaceProgress{
		RemainingTime: float64((time.Minute * 77).Milliseconds()),
		LapsComplete:  10,
		FuelTank:      4850,
		EnergyTank:    5000,
	}

	TestProgressAdjustedStints []entities.Stint = []entities.Stint{
		{
			StintNumber:   1,
			Laps:          11,
			CompletedLaps: 10,
			TargetUse: entities.FuelReq{
				Fuel:   441,
				Energy: 455,
			},
			StintRequired: entities.FuelReq{
				Fuel:   4850,
				Energy: 5000,
			},
			FuelRatio: 97,
			PitLength: 0,
		},
		{
			StintNumber: 2,
			Laps:        20,
			Save:        1,
			TargetUse: entities.FuelReq{
				Fuel:   481,
				Energy: 500,
			},
			StintRequired: entities.FuelReq{
				Fuel:   9700,
				Energy: 10000,
			},
			FuelRatio: 97,
			PitLength: 45000,
		},
		{
			StintNumber: 3,
			Laps:        20,
			Save:        1,
			TargetUse: entities.FuelReq{
				Fuel:   481,
				Energy: 500,
			},
			StintRequired: entities.FuelReq{
				Fuel:   9700,
				Energy: 10000,
			},
			FuelRatio: 97,
			PitLength: 45000,
		},
	}
)
