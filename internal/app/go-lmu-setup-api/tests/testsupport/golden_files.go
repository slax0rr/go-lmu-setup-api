package testsupport

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
	"testing"

	"github.com/google/uuid"
	"gotest.tools/v3/golden"
)

const (
	goldenTestFilePath string = "data/golden_files/"
)

var (
	uuidRegex     = regexp.MustCompile(`[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}`)
	filenameRegex = regexp.MustCompile(`[^\w]+`)
)

func updateGoldenFile(filename string, output []byte) error {
	if !golden.FlagUpdate() {
		return nil
	}

	return os.WriteFile(filename, output, 0644)
}

func getFullGoldenFilePath(filename string) (string, error) {
	_, currFile, _, _ := runtime.Caller(0)
	testsupportDir := filepath.Dir(currFile)

	filepath, err := filepath.Abs(filepath.Join(testsupportDir, goldenTestFilePath, filename))
	if err != nil {
		return "", err
	}

	return filepath, nil
}

func RemoveUUIDs(data string) string {
	return uuidRegex.ReplaceAllString(data, uuid.Nil.String())
}

func AssertGoldenFile(t *testing.T, testCaseName string, output []byte) error {
	filename, err := getFullGoldenFilePath(GoldenFileName(t, testCaseName))

	data := RemoveUUIDs(string(output))

	err = updateGoldenFile(filename, []byte(data))
	if err != nil {
		return err
	}

	golden.Assert(t, data, filename, "assertion of result against the golden file failed for test case '%s'",
		testCaseName)
	return nil
}

func GoldenFileName(t *testing.T, testCaseName string) string {
	safe := filenameRegex.ReplaceAllString(fmt.Sprintf("%s_%s", t.Name(), testCaseName), "_")
	return fmt.Sprintf("%s.golden.json", strings.ToLower(strings.TrimSpace(safe)))
}
