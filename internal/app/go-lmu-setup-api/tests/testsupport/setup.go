package testsupport

import (
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"time"

	"github.com/docker/go-connections/nat"
	"github.com/rs/zerolog"
	"github.com/testcontainers/testcontainers-go/modules/compose"
	"github.com/testcontainers/testcontainers-go/wait"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/application"
)

const (
	migrationsPath string = "../infra/database/migrations/"

	setupStorePath  string = "./test_setup_store/"
	DiscordClientID string = "discord_test_client_id"

	defaultServicesHost string = "localhost"
)

var (
	dcDownFunc func() error
	dbPort     string
	logger     zerolog.Logger

	ServerPort         int
	BaseURL            string
	DatabaseDSN        string
	SetupStoreLocation string

	HS256Key []byte
)

func Teardown() error {
	err := dcDownFunc()
	if err != nil {
		return fmt.Errorf("tear down docker compose: %w", err)
	}

	err = os.RemoveAll(SetupStoreLocation)
	if err != nil {
		return fmt.Errorf("remove test setup store location: %w", err)
	}

	return nil
}

func Bootstrap(ctx context.Context) error {
	dbHost := defaultServicesHost
	if ci := os.Getenv("GITLAB_CI"); ci == "true" {
		dbHost = "db"
		dbPort = "5432"
	} else {
		err := startContainers(ctx)
		if err != nil {
			return fmt.Errorf("start containers: %w", err)
		}
	}

	ServerPort, err := getFreePort()
	if err != nil {
		return fmt.Errorf("get free port for app: %w", err)
	}

	BaseURL = fmt.Sprintf("http://localhost:%d", ServerPort)
	DatabaseDSN = fmt.Sprintf("postgres://lmu_setups_user:devpass@%s:%s/lmu_setups", dbHost, dbPort)

	SetupStoreLocation, err = filepath.Abs(setupStorePath)
	if err != nil {
		return fmt.Errorf("determine setup store location absolute path: %w", err)
	}
	_, err = os.Stat(SetupStoreLocation)
	if os.IsNotExist(err) {
		os.MkdirAll(SetupStoreLocation, 0700)
	}

	HS256Key = []byte("supersecrettestsynchronouskey123")
	hs256Buffer := &bytes.Buffer{}
	encoder := base64.NewEncoder(base64.StdEncoding, hs256Buffer)
	_, err = encoder.Write(HS256Key)
	if err != nil {
		return fmt.Errorf("encoding hs256 key: %w", err)
	}
	err = encoder.Close()
	if err != nil {
		return fmt.Errorf("closing encoder of hs256 key: %w", err)
	}

	os.Setenv("DATABASE_DSN", DatabaseDSN)
	os.Setenv("DB_MIGRATIONS_LOCATION", migrationsPath)
	os.Setenv("STORE_LOCATION", SetupStoreLocation)
	os.Setenv("SERVER_PORT", fmt.Sprintf("%d", ServerPort))
	os.Setenv("USER_TOKEN_ENCODED_KEY", hs256Buffer.String())

	logger = zerolog.New(os.Stdout).
		With().Timestamp().Logger().
		Level(zerolog.DebugLevel)
	go func() {
		err = application.Bootstrap(&logger)
		if err != nil {
			log.Fatalf("test application bootstrap: %v", err)
		}
	}()

	err = waitForStartup(ctx, 10*time.Second)
	if err != nil {
		return fmt.Errorf("wait for service startup: %w", err)
	}

	return nil
}

func getFreePort() (int, error) {
	listener, err := net.Listen("tcp", ":0")
	if err != nil {
		return 0, err
	}
	defer listener.Close()

	port := listener.Addr().(*net.TCPAddr).Port
	return port, nil
}

func waitForStartup(ctx context.Context, timeout time.Duration) error {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	errChan := make(chan error)

	go func(errChan chan error) {
		log.Print("waiting for service to start up")
		healthURL, err := url.JoinPath(BaseURL, "/health")
		if err != nil {
			errChan <- fmt.Errorf("construct health endpoint url: %w", err)
			return
		}

		req, err := http.NewRequest(http.MethodGet, healthURL, nil)
		if err != nil {
			errChan <- fmt.Errorf("create health check request: %w", err)
			return
		}

		check := func(req *http.Request) error {
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				return err
			}
			defer resp.Body.Close()

			if resp.StatusCode == http.StatusOK {
				return nil
			}

			return fmt.Errorf("invalid status")
		}

		for {
			log.Print(".")

			if err := check(req); err == nil {
				log.Print("service started")
				errChan <- nil
				return
			}

			time.Sleep(1 * time.Second)
		}
	}(errChan)

	select {
	case <-ctx.Done():
		return fmt.Errorf("timed out waiting for health check to pass")
	case err := <-errChan:
		return err
	}
}

func startContainers(ctx context.Context) error {
	dc, err := compose.NewDockerCompose("../../../../docker/docker-compose.yml")
	if err != nil {
		fmt.Printf("init docker compose: %v", err)
		os.Exit(1)
	}
	dcDownFunc = func() error {
		err := dc.Down(ctx, compose.RemoveOrphans(true), compose.RemoveImagesLocal)
		if err != nil {
			return fmt.Errorf("docker compose down: %w", err)
		}
		return nil
	}

	err = dc.WaitForService("db",
		wait.NewHostPortStrategy(nat.Port("5432/tcp")).
			WithStartupTimeout(10*time.Second),
	).Up(ctx, compose.Wait(true), compose.RunServices("db"))
	if err != nil {
		return fmt.Errorf("docker compose up: %w", err)
	}

	container, err := dc.ServiceContainer(ctx, "db")
	if err != nil {
		return fmt.Errorf("find db container: %w", err)
	}
	portmap, err := container.Ports(ctx)
	if err != nil {
		return fmt.Errorf("get db container ports: %w", err)
	}

	dbPort = portmap["5432/tcp"][0].HostPort
	return nil
}
