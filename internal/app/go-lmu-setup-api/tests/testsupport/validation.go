package testsupport

type ExpectedValidationError struct {
	Field string
	Msg   string
	Value interface{}
}
