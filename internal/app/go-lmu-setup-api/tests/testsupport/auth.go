package testsupport

import (
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v5"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/auth"
)

func GenerateUserToken(user *entities.User, expiresAt time.Time) (string, error) {
	claims := auth.UserTokenClaims{
		User: user,
		RegisteredClaims: jwt.RegisteredClaims{
			Subject:   user.ID.String(),
			ExpiresAt: jwt.NewNumericDate(expiresAt),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			Issuer:    "LMU API Test",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedToken, err := token.SignedString(HS256Key)
	if err != nil {
		return "", fmt.Errorf("signing userdata token: %w", err)
	}
	return signedToken, nil
}
