package tests

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/stretchr/testify/require"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/constants"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/tests/testsupport"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/response"
)

type CarsStage struct {
	t       *testing.T
	require *require.Assertions

	dbPool *pgxpool.Pool

	payload []byte
	req     *http.Request
	resp    *http.Response
	body    []byte

	carID uuid.UUID
}

func NewCarsStage(t *testing.T) (*CarsStage, *CarsStage, *CarsStage) {
	s := &CarsStage{
		t: t,
	}

	s.require = require.New(s.t)

	var err error
	s.dbPool, err = pgxpool.New(context.Background(), testsupport.DatabaseDSN)
	s.require.NoError(err, "create new postgres db pool")

	return s, s, s
}

func (s *CarsStage) and() *CarsStage {
	return s
}

func (s *CarsStage) a_request_for_listing_cars() *CarsStage {
	var err error
	s.req, err = http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1/cars",
		testsupport.BaseURL), nil)
	s.require.NoError(err, "build get /cars request")
	return s
}

func (s *CarsStage) a_request_for_retrieving_car_details() *CarsStage {
	// same URL is used for both, param is to be added a separate function
	return s.a_request_for_listing_cars()
}

func (s *CarsStage) with_a_car_id(id string) *CarsStage {
	var err error
	s.req.URL.Path, err = url.JoinPath(s.req.URL.Path, fmt.Sprintf("/%s", id))
	s.require.NoError(err, "attach car id to get car detail url")
	return s
}

func (s *CarsStage) with_a_valid_and_existing_car_id() *CarsStage {
	row := s.dbPool.QueryRow(context.Background(), "select id from cars limit 1")
	err := row.Scan(&s.carID)
	s.require.NoError(err, "retrieve a cars id from the database")

	return s.with_a_car_id(s.carID.String())
}

func (s *CarsStage) with_an_invalid_car_id() *CarsStage {
	return s.with_a_car_id("invalid id")
}

func (s *CarsStage) with_a_valid_but_nonexisting_car_id() *CarsStage {
	return s.with_a_car_id(uuid.New().String())
}

func (s *CarsStage) the_request_is_executed() *CarsStage {
	var err error
	s.resp, err = http.DefaultClient.Do(s.req)
	s.require.NoError(err, "execute http request")
	defer s.resp.Body.Close()

	s.body, err = ioutil.ReadAll(s.resp.Body)
	s.require.NoError(err, "read response body")
	return s
}

func (s *CarsStage) the_response_status_is(status int) *CarsStage {
	s.require.Equal(status, s.resp.StatusCode, "response status unexpected")
	return s
}

func (s *CarsStage) the_response_status_is_ok() *CarsStage {
	return s.the_response_status_is(http.StatusOK)
}

func (s *CarsStage) all_cars_are_listed() *CarsStage {
	rows, err := s.dbPool.Query(context.Background(),
		"select id, class, race_class, manufacturer, model, spec, year from cars order by race_class asc, manufacturer asc")
	s.require.NoError(err, "fetch all cars from the database")

	dbCars := entities.CarList{}
	for rows.Next() {
		car := entities.Car{}
		rows.Scan(
			&car.ID,
			&car.Class,
			&car.RaceClass,
			&car.Manufacturer,
			&car.Model,
			&car.Spec,
			&car.Year,
		)
		dbCars = append(dbCars, car)
	}

	apiCars := entities.CarList{}
	err = json.Unmarshal(s.body, &apiCars)
	s.require.NoError(err, "unmarshal retrieved cars list")

	s.require.EqualValues(dbCars, apiCars)

	return s
}

func (s *CarsStage) the_car_details_are_in_expected_format() *CarsStage {
	car := entities.Car{}
	err := json.Unmarshal(s.body, &car)
	s.require.NoError(err, "unmarshal car detail response")
	s.require.Equal(s.carID.String(), car.ID.String())
	return s
}

func (s *CarsStage) error_response_details_indicate_invalid_id() *CarsStage {
	respErr := response.Error{}
	err := json.Unmarshal(s.body, &respErr)
	s.require.NoError(err, "unmarshal response error")
	s.require.Equal(constants.ErrorDetails_MissingOrMalformedResourceID, respErr.Details)
	return s
}
