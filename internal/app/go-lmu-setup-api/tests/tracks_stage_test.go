package tests

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/stretchr/testify/require"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/constants"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/tests/testsupport"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/response"
)

type TracksStage struct {
	t       *testing.T
	require *require.Assertions

	dbPool *pgxpool.Pool

	payload []byte
	req     *http.Request
	resp    *http.Response
	body    []byte

	trackID uuid.UUID
}

func NewTracksStage(t *testing.T) (*TracksStage, *TracksStage, *TracksStage) {
	s := &TracksStage{
		t: t,
	}

	s.require = require.New(s.t)

	var err error
	s.dbPool, err = pgxpool.New(context.Background(), testsupport.DatabaseDSN)
	s.require.NoError(err, "create new postgres db pool")

	return s, s, s
}

func (s *TracksStage) and() *TracksStage {
	return s
}

func (s *TracksStage) a_request_for_listing_tracks() *TracksStage {
	var err error
	s.req, err = http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1/tracks",
		testsupport.BaseURL), nil)
	s.require.NoError(err, "build get /tracks request")
	return s
}

func (s *TracksStage) a_request_for_retrieving_track_details() *TracksStage {
	// same URL is used for both, param is to be added a separate function
	return s.a_request_for_listing_tracks()
}

func (s *TracksStage) with_a_track_id(id string) *TracksStage {
	var err error
	s.req.URL.Path, err = url.JoinPath(s.req.URL.Path, fmt.Sprintf("/%s", id))
	s.require.NoError(err, "attach track id to get track detail url")
	return s
}

func (s *TracksStage) with_a_valid_and_existing_track_id() *TracksStage {
	row := s.dbPool.QueryRow(context.Background(), "select id from tracks limit 1")
	err := row.Scan(&s.trackID)
	s.require.NoError(err, "retrieve a tracks id from the database")

	return s.with_a_track_id(s.trackID.String())
}

func (s *TracksStage) with_an_invalid_track_id() *TracksStage {
	return s.with_a_track_id("invalid id")
}

func (s *TracksStage) with_a_valid_but_nonexisting_track_id() *TracksStage {
	return s.with_a_track_id(uuid.New().String())
}

func (s *TracksStage) the_request_is_executed() *TracksStage {
	var err error
	s.resp, err = http.DefaultClient.Do(s.req)
	s.require.NoError(err, "execute http request")
	defer s.resp.Body.Close()

	s.body, err = ioutil.ReadAll(s.resp.Body)
	s.require.NoError(err, "read response body")
	return s
}

func (s *TracksStage) the_response_status_is(status int) *TracksStage {
	s.require.Equal(status, s.resp.StatusCode, "response status unexpected")
	return s
}

func (s *TracksStage) the_response_status_is_ok() *TracksStage {
	return s.the_response_status_is(http.StatusOK)
}

func (s *TracksStage) all_tracks_are_listed() *TracksStage {
	rows, err := s.dbPool.Query(context.Background(),
		"select id, country, track_name, short_track_name, year from tracks order by short_track_name")
	s.require.NoError(err, "fetch all tracks from the database")

	dbTracks := entities.TrackList{}
	for rows.Next() {
		track := entities.Track{}
		rows.Scan(
			&track.ID,
			&track.Country,
			&track.TrackName,
			&track.ShortTrackName,
			&track.Year,
		)
		dbTracks = append(dbTracks, track)
	}

	apiTracks := entities.TrackList{}
	err = json.Unmarshal(s.body, &apiTracks)
	s.require.NoError(err, "unmarshal retrieved tracks list")

	s.require.EqualValues(dbTracks, apiTracks)

	return s
}

func (s *TracksStage) the_track_details_are_in_expected_format() *TracksStage {
	track := entities.Track{}
	err := json.Unmarshal(s.body, &track)
	s.require.NoError(err, "unmarshal track detail response")
	s.require.Equal(s.trackID.String(), track.ID.String())
	return s
}

func (s *TracksStage) error_response_details_indicate_invalid_id() *TracksStage {
	respErr := response.Error{}
	err := json.Unmarshal(s.body, &respErr)
	s.require.NoError(err, "unmarshal response error")
	s.require.Equal(constants.ErrorDetails_MissingOrMalformedResourceID, respErr.Details)
	return s
}
