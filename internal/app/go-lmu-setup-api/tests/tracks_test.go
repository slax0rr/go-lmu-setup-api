package tests

import (
	"net/http"
	"testing"
)

func TestTracks_List_HappyPath(t *testing.T) {
	given, when, then := NewTracksStage(t)

	given.a_request_for_listing_tracks()

	when.the_request_is_executed()

	then.the_response_status_is_ok().and().
		all_tracks_are_listed()
}

func TestTracks_GetDetails_HappyPath(t *testing.T) {
	given, when, then := NewTracksStage(t)

	given.a_request_for_retrieving_track_details().
		with_a_valid_and_existing_track_id()

	when.the_request_is_executed()

	then.the_response_status_is_ok().and().
		the_track_details_are_in_expected_format()
}

func TestTracks_GetDetailsWithInvalidID_UnhappyPath1(t *testing.T) {
	given, when, then := NewTracksStage(t)

	given.a_request_for_retrieving_track_details().
		with_an_invalid_track_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusBadRequest).and().
		error_response_details_indicate_invalid_id()
}

func TestTracks_GetDetailsOfNonExistingTrack_UnhappyPath2(t *testing.T) {
	given, when, then := NewTracksStage(t)

	given.a_request_for_retrieving_track_details().
		with_a_valid_but_nonexisting_track_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusNotFound)
}
