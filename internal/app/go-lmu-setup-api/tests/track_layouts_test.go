package tests

import (
	"net/http"
	"testing"
)

func TestTrackLayoutLayouts_List_HappyPath(t *testing.T) {
	given, when, then := NewTrackLayoutsStage(t)

	given.a_request_for_listing_track_layouts()

	when.the_request_is_executed()

	then.the_response_status_is_ok().and().
		all_track_layouts_are_listed()
}

func TestTrackLayoutLayouts_GetDetails_HappyPath(t *testing.T) {
	given, when, then := NewTrackLayoutsStage(t)

	given.a_request_for_retrieving_track_layout_details().
		with_a_valid_and_existing_track_layout_id()

	when.the_request_is_executed()

	then.the_response_status_is_ok().and().
		the_track_layout_details_are_in_expected_format()
}

func TestTrackLayoutLayouts_GetDetailsWithInvalidID_UnhappyPath1(t *testing.T) {
	given, when, then := NewTrackLayoutsStage(t)

	given.a_request_for_retrieving_track_layout_details().
		with_an_invalid_track_layout_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusBadRequest).and().
		error_response_details_indicate_invalid_id()
}

func TestTrackLayoutLayouts_GetDetailsOfNonExistingTrackLayout_UnhappyPath2(t *testing.T) {
	given, when, then := NewTrackLayoutsStage(t)

	given.a_request_for_retrieving_track_layout_details().
		with_a_valid_but_nonexisting_track_layout_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusNotFound)
}
