package tests

import (
	"net/http"
	"testing"
)

// TODO(slax0rr): add tests to for track layout handling in the setups
// the test should ensure only valid track layouts are added and to the correct
// tracks

func TestSetups_List_HappyPath(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_listing_setups()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusOK).and().
		all_setups_are_listed().and().
		results_are_paginated()
}

func TestSetups_List_IncludesUserVotes_HappyPath(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_listing_setups().and().
		a_valid_user_token()

	when.the_user_has_recorded_an_upvote().and().
		the_request_is_executed()

	then.the_response_status_is(http.StatusOK).and().
		all_setups_are_listed().and().
		results_are_paginated()
}

func TestSetups_ListUserSetups_HappyPath(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_user_created_setup().and().
		a_request_for_listing_user_saved_setups().and().
		a_valid_user_token()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusOK).and().
		all_user_created_setups_are_listed().and().
		results_are_paginated()
}

func TestSetups_GetDetails_HappyPath(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_retrieving_setup_details().
		with_a_valid_and_existing_setup_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusOK).and().
		the_setup_details_are_in_expected_format()
}

func TestSetups_GetDetails_WithInvalidID_UnhappyPath1(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_retrieving_setup_details().
		with_an_invalid_setup_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusBadRequest).and().
		error_response_details_indicate_invalid_id()
}

func TestSetups_GetDetails_OfNonExistingSetup_UnhappyPath2(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_retrieving_setup_details().
		with_a_valid_but_nonexisting_setup_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusNotFound)
}

func TestSetup_GetSetupFile_HappyPath(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_retrieving_the_setup_file().
		with_a_valid_and_existing_setup_id().and().
		the_setup_file_will_exist()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusOK).and().
		the_setup_file_is_downloaded()
}

func TestSetups_GetSetupFile_WithInvalidID_UnhappyPath3(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_retrieving_the_setup_file().
		with_an_invalid_setup_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusBadRequest).and().
		error_response_details_indicate_invalid_id()
}

func TestSetups_GetSetupFile_OfNonExistingSetup_UnhappyPath3(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_retrieving_the_setup_file().
		with_a_valid_but_nonexisting_setup_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusNotFound)
}

func TestSetups_GetSetupFile_WhereSetupFileIsNotFound_UnhappyPath3(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_retrieving_the_setup_file().
		with_a_valid_and_existing_setup_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusNotFound).and().
		error_response_details_indicate_missing_file()
}

func TestSetup_AddSetup_HappyPath1(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_adding_a_setup().
		with_a_valid_setup().without_laptime().and().
		a_valid_user_token()

	when.the_setup_data_is_added_to_the_payload().and().
		the_request_is_executed()

	then.the_response_status_is(http.StatusCreated).and().
		the_setup_is_stored_in_the_database().and().
		setup_file_is_added_to_the_database()
}

func TestSetup_AddSetup_WithExistingFileName_HappyPath1(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_adding_a_setup().
		with_a_valid_setup().and().
		a_valid_user_token()

	when.the_setup_data_is_added_to_the_payload().and().
		a_setup_with_the_same_filename_exists().and().
		the_request_is_executed()

	then.the_response_status_is(http.StatusCreated).and().
		the_setup_is_stored_in_the_database().and().
		setup_file_is_added_to_the_database()
}

func TestSetup_AddSetup_WithoutAuth_Unauthorized_UnhappyPath4(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_adding_a_setup()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusUnauthorized)
}

func TestSetup_AddSetup_WithInvalidAuth_Unauthorized_UnhappyPath5(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_adding_a_setup().and().
		a_valid_user_token_with_empty_data()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusUnauthorized)
}

func TestSetup_AddSetup_WithInvalidPayload_UnhappyPath6(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_adding_a_setup().
		with_an_invalid_setup_payload().and().
		a_valid_user_token()

	when.the_setup_data_is_added_to_the_payload().and().
		the_request_is_executed()

	then.the_response_status_is(http.StatusBadRequest).and().
		the_response_contains_validation_errors()
}

func TestSetup_AddSetup_WithEmptyFileContent_UnhappyPath12(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_adding_a_setup().
		with_empty_file_contents_in_setup_payload().and().
		a_valid_user_token()

	when.the_setup_data_is_added_to_the_payload().and().
		the_request_is_executed()

	then.the_response_status_is(http.StatusBadRequest).and().
		the_response_contains_validation_errors()
}

func TestSetup_UpdateSetup_HappyPath1(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_updating_a_setup().and().
		the_setup_file_will_exist().and().
		with_a_valid_and_existing_setup_id().and().
		with_a_valid_setup_for_update().and().
		a_valid_user_token()

	when.the_setup_data_is_added_to_the_payload().and().
		the_request_is_executed()

	then.the_response_status_is(http.StatusOK).and().
		the_updated_setup_is_stored_in_the_database().and().
		setup_file_is_added_to_the_database()
}

func TestSetup_UpdateSetup_WithEmptyFileContent_HappyPath2(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_updating_a_setup().and().
		the_setup_file_will_exist().and().
		with_a_valid_and_existing_setup_id().and().
		with_a_valid_setup_for_update().and().
		file_content_is_empty_in_the_request().and().
		a_valid_user_token()

	when.the_setup_data_is_added_to_the_payload().and().
		the_request_is_executed()

	then.the_response_status_is(http.StatusOK).and().
		the_updated_setup_is_stored_in_the_database().and().
		setup_file_is_added_to_the_database()
}

func TestSetup_UpdateSetup_WithoutAuth_Unauthorized_UnhappyPath7(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_updating_a_setup().
		with_a_valid_and_existing_setup_id()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusUnauthorized)
}

func TestSetup_UpdateSetup_WithInvalidAuth_Unauthorized_UnhappyPath8(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_updating_a_setup().
		with_a_valid_and_existing_setup_id().and().
		a_valid_user_token_with_empty_data()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusUnauthorized)
}

func TestSetup_UpdateSetup_WithExpiredAuth_Unauthorized_UnhappyPath11(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_updating_a_setup().
		with_a_valid_and_existing_setup_id().and().
		an_expired_user_token()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusUnauthorized)
}

func TestSetup_UpdateSetup_WithInvalidPayload_UnhappyPath9(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_updating_a_setup().
		with_a_valid_and_existing_setup_id().and().
		with_an_invalid_setup_payload().and().
		a_valid_user_token()

	when.the_setup_data_is_added_to_the_payload().and().
		the_request_is_executed()

	then.the_response_status_is(http.StatusBadRequest).and().
		the_response_contains_validation_errors()
}

func TestSetups_UpdateSetup_NonExistingSetup_UnhappyPath10(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_updating_a_setup().
		with_a_valid_but_nonexisting_setup_id().and().
		with_a_valid_setup_for_update().and().
		a_valid_user_token()

	when.the_setup_data_is_added_to_the_payload().and().
		the_request_is_executed()

	then.the_response_status_is(http.StatusNotFound)
}

func TestSetups_RateUpvote_HappyPath(t *testing.T) {
	given, when, then := NewSetupsStage(t)

	given.a_request_for_upvoting_a_setup().
		with_a_valid_and_existing_setup_id().and().
		a_valid_user_token()

	when.the_request_is_executed()

	then.the_response_status_is(http.StatusOK).and().
		the_new_rating_is_in_the_response().and().
		the_rating_is_updated()
}
