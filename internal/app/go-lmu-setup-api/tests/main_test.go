package tests

import (
	"context"
	"log"
	"os"
	"testing"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/tests/testsupport"
)

func TestMain(m *testing.M) {
	exit := 0
	defer func() {
		log.Print("exiting from tests")
		os.Exit(exit)
	}()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	err := testsupport.Bootstrap(ctx)
	if err != nil {
		log.Fatalf("test bootstrap: %v", err)
	}
	defer func() {
		err := testsupport.Teardown()
		if err != nil {
			log.Fatalf("test teardown: %v", err)
		}
	}()

	exit = m.Run()
}
