package validators

import (
	"reflect"
	"regexp"

	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/validators"
)

func RegisterCustomValidations() {
	validators.RegisterCustomValidation("custom_laptime_validation", func(val reflect.Value) bool {
		fieldVal := val.String()
		if fieldVal == "" {
			return true
		}
		re := regexp.MustCompile(`\d{1,2}:[0-5][0-9]:\d{3}`)
		return re.MatchString(fieldVal)
	})
}
