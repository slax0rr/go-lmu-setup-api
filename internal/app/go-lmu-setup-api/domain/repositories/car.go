package repositories

import (
	"context"

	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
)

type Car interface {
	Get(context.Context) (entities.CarList, error)
	GetByID(context.Context, uuid.UUID) (*entities.Car, error)
}
