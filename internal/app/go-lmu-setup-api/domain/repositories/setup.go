package repositories

import (
	"context"

	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
)

type Setup interface {
	Count(context.Context, ...postgresdb.ExecOption) (int64, error)
	Create(context.Context, *entities.Setup, ...postgresdb.ExecOption) error
	Update(context.Context, *entities.Setup, ...postgresdb.ExecOption) (int64, error)
	GetList(context.Context, uuid.UUID, ...postgresdb.ExecOption) (entities.SetupList, error)
	IncrementDownload(context.Context, uuid.UUID, ...postgresdb.ExecOption) error
	RecordVote(context.Context, uuid.UUID, uuid.UUID, bool, ...postgresdb.ExecOption) error
}
