package repositories

import (
	"context"

	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
)

type TrackLayout interface {
	Get(context.Context) (entities.TrackLayoutList, error)
	GetByID(context.Context, uuid.UUID) (*entities.TrackLayout, error)
	GetTrackDefault(context.Context, uuid.UUID) (*uuid.UUID, error)
}
