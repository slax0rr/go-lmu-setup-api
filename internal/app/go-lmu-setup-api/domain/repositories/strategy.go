package repositories

import (
	"context"

	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
)

type Strategy interface {
	Count(context.Context, ...postgresdb.ExecOption) (int64, error)
	Create(context.Context, *entities.Strategy, ...postgresdb.ExecOption) error
	GetByID(context.Context, uuid.UUID) (*entities.Strategy, error)
	List(context.Context, ...postgresdb.ExecOption) (entities.StrategyList, error)
	StoreRecalculatedStrategy(context.Context, *entities.Strategy, ...postgresdb.ExecOption) error
}
