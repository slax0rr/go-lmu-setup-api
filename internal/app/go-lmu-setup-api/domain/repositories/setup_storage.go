package repositories

import (
	"context"

	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
)

type SetupStorage interface {
	Store(context.Context, entities.SetupFile, ...postgresdb.ExecOption) error
	List(context.Context, uuid.UUID, ...postgresdb.ExecOption) ([]entities.SetupFile, error)
}
