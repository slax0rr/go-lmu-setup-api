package entities

import (
	"time"

	"github.com/google/uuid"
)

type SetupType = string
type SetupCondition = string
type SetupVote = string

const (
	SetupTypeSprint    SetupType = "sprint"
	SetupTypeEndurance SetupType = "endurance"
	SetupTypeHotlap    SetupType = "hotlap"

	SetupConditionDry     SetupCondition = "dry"
	SetupConditionMixed   SetupCondition = "mixed"
	SetupConditionDamp    SetupCondition = "damp"
	SetupConditionWet     SetupCondition = "wet"
	SetupConditionFlooded SetupCondition = "flooded"

	SetupVote_Upvote   SetupVote = "upvote"
	SetupVote_Downvote SetupVote = "downvote"
)

// TODO(slax0rr): add custom validation to check the track layout id against the
// stored layout data
type Setup struct {
	ID            uuid.UUID      `json:"id"                     validate:"uuid"`
	CarID         uuid.UUID      `json:"car_id"                 validate:"required,uuid"`
	TrackID       uuid.UUID      `json:"track_id"               validate:"required,uuid"`
	TrackLayoutID *uuid.UUID     `json:"track_layout_id"`
	FileName      string         `json:"file_name"              validate:"required"`
	FileContent   string         `json:"file_content,omitempty" validate:"required_without=ID"`
	Laptime       string         `json:"laptime"                validate:"custom_laptime_validation"`
	Type          SetupType      `json:"type"                   validate:"required,oneof=sprint endurance hotlap"`
	Condition     SetupCondition `json:"condition"              validate:"required,oneof=dry damp wet flooded mixed"`
	Rating        int64          `json:"rating"                 validate:"eq=0"`
	Description   *string        `json:"description,omitempty"`
	Downloads     int            `json:"downloads"              validate:"eq=0"`
	VideoLink     string         `json:"video_link,omitempty"   validate:"omitempty,http_url"`
	Version       string         `json:"version"`
	UserID        uuid.UUID      `json:"user_id"                validate:"required,uuid"`
	User          string         `json:"display_name"           validate:"required"`
	UserVote      *bool          `json:"user_vote""`
	CreatedAt     time.Time      `json:"uploaded_at"`
	UpdatedAt     *time.Time     `json:"modified,omitempty"`
}

type SetupList []Setup

type SetupFile struct {
	ID        uuid.UUID  `json:"-"`
	SetupID   uuid.UUID  `json:"-"`
	Name      string     `json:"-"`
	Content   []byte     `json:"-"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt *time.Time `json:"-"`

	// TODO(slax0rr): remove the legacy setup file parameters
	Path string `json:"-"`
}
