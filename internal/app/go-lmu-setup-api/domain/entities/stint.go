package entities

import (
	"math"
)

type Stint struct {
	StintNumber   uint    `json:"stint_number"             validate:"required"`
	Laps          float64 `json:"laps"                     validate:"required,gt=0"`
	Save          float64 `json:"save,omitempty"`
	TargetUse     FuelReq `json:"target_use"               validate:"required"`
	StintRequired FuelReq `json:"stint_required"           validate:"required"`
	FuelRatio     float64 `json:"fuel_ratio"               validate:"required"`
	PitLength     int64   `json:"pit_length"               validate:"required"`
	Complete      bool    `json:"complete,omitempty"`
	CompletedLaps float64 `json:"completed_laps,omitempty"`
	Locked        bool    `json:"-"`
}

type Stints []Stint

// CalculateFule requirements for the stint.
func (s *Stint) CalculateFuel(fuelUse, energyUse, capacity float64) {
	if energyUse > 0 {
		s.TargetUse.Energy, s.StintRequired.Energy = s.calculateTargetUseAndFill(s.Laps, capacity, energyUse)
		s.TargetUse.Fuel, s.StintRequired.Fuel, s.FuelRatio = s.calcFuelFromEnergy(
			s.Laps, fuelUse, energyUse, s.TargetUse.Energy, s.StintRequired.Energy)
	}

	if s.Save == 0 {
		// fuel requirement on a flat out stint
		// including short splash&dash stints
		fill := s.getTankFill(s.Laps, fuelUse, energyUse, capacity)
		if energyUse == 0 {
			s.TargetUse.Fuel, s.StintRequired.Fuel = s.calculateTargetUseAndFill(s.Laps, fill, fuelUse)
		}

		return
	}

	// fuel requirement on a save stint
	if energyUse == 0 {
		s.TargetUse.Fuel, s.StintRequired.Fuel = s.calculateTargetUseAndFill(s.Laps, capacity, fuelUse)
	}
}

// calculateTargetUseAndFIll calculates the target fuel or energy usage per lap
// and determines the required tank fill based.
func (*Stint) calculateTargetUseAndFill(laps, fill, maxUse float64) (targetUse, requiredFill float64) {
	targetUse = math.Ceil((fill / 100) / laps * 100)
	if targetUse <= maxUse*1.05 {
		requiredFill = fill
		return
	}

	targetUse = maxUse
	requiredFill = math.Ceil((maxUse/100)*laps) * 100
	return
}

// calcFuelFromEnergy calculates the target fuel per lap usage and required
// fuel fill, based on the equivalent energy use and fill.
func (*Stint) calcFuelFromEnergy(
	laps, fuelUse, energyUse, targetEnergyUse, requiredEnergy float64,
) (targetFuelUse, requiredFuel, ratio float64) {
	targetFuelUse = math.Ceil(targetEnergyUse * (fuelUse / energyUse))
	requiredFuel = math.Ceil(laps*targetFuelUse/100) * 100

	ratio = math.Ceil((requiredFuel / requiredEnergy) * 100)

	return
}

// getTankFill returns the required tank fill based on number of laps and fuel
// or energy use, where energy use is used for the calculation if it is greater
// than 0. If fill is calculated to be greater than the tank capacity, then the
// capacity is used instead.
func (*Stint) getTankFill(laps, fuelUse, energyUse, capacity float64) float64 {
	use := fuelUse
	if energyUse > 0 {
		use = energyUse
	}
	fill := (math.Ceil(laps * use / 100)) * 100
	if fill > capacity {
		return capacity
	}
	return fill
}
