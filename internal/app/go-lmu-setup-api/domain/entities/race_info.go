package entities

import (
	"math"
	"time"
)

const (
	fullEnergyCapacity float64 = 10000
)

type RaceInfo struct {
	TankCapacity float64 `json:"tank_capacity,omitempty" validate:"lt=100000"`
	FuelUse      float64 `json:"fuel_use"                validate:"required,gt=0,lt=100000"`
	EnergyUse    float64 `json:"energy_use,omitempty"    validate:"required_without=TankCapacity,lt=100000"`
	RaceTime     int64   `json:"race_time"               validate:"gt=0,lt=172800000"`           // less than 48hrs
	Laptime      int64   `json:"laptime"                 validate:"required,gt=9999,lt=6000000"` // less than 100mnts
	PitLength    int64   `json:"pit_length"              validate:"lt=1000000"`
	StintLaps    float64 `json:"-"`
	Laps         float64 `json:"-"`
	SafeLaps     float64 `json:"-"`
}

// GetCapacityAndUse returns the fuel tank capacity and per lap FuelUse. If the
// info *entities.RaceInfo however defines EnergyUse, then the capacity defaults
// to 10000 (100%), and EnergyUse is returned as usage.
func (i *RaceInfo) GetCapacityAndUse() (capacity, use float64) {
	if i.EnergyUse > 0 {
		return fullEnergyCapacity, i.EnergyUse
	}
	return i.TankCapacity, i.FuelUse
}

// CalculateRaceLaps based on the race length time, laptime, and pitlength time
// adjusting the race laps for the amount of required pit stops.
func (i *RaceInfo) CalculateRaceLaps() {
	capacity, use := i.GetCapacityAndUse()

	raceLenMS := float64((time.Duration(i.RaceTime) * time.Millisecond).Milliseconds())
	laptimeMS := float64((time.Duration(i.Laptime) * time.Millisecond).Milliseconds())
	pittimeMS := float64(i.PitLength)

	// maximum laps on fuel/energy use
	i.StintLaps = math.Floor(capacity / use)

	// initial laps and stops calculation
	i.Laps, i.SafeLaps = i.getRaceLaps(raceLenMS, laptimeMS)
	initialStops := math.Ceil(i.SafeLaps/i.StintLaps) - 1

	// adjust laps based on initial stops calculation
	i.Laps, i.SafeLaps = i.getRaceLaps((raceLenMS - initialStops*pittimeMS), laptimeMS)
	stops := math.Floor(i.Laps / i.StintLaps)

	i.ensureRaceLapsCoverRaceLength(raceLenMS, laptimeMS, pittimeMS, stops)
}

// GetMaxSaveStintLaps returns the maximum laps of a stint for a fuel save drive
// based on the provided maxSavePercent.
func (i *RaceInfo) GetMaxSaveStintLaps(maxSavePercent float64) float64 {
	if i.EnergyUse > 0 {
		saveUse := math.Round(i.EnergyUse / maxSavePercent)
		return math.Round(10000 / saveUse)
	}

	saveUse := math.Round(i.FuelUse / maxSavePercent)
	return math.Round(i.TankCapacity / saveUse)
}

// getRaceLaps calculates the laps required to reach the race length given the
// laptime. Additionally returns one safe lap on top of the calculated laps.
func (i *RaceInfo) getRaceLaps(raceLen, laptime float64) (laps, safeLaps float64) {
	laps = math.Ceil(raceLen / laptime)
	safeLaps = laps + 1
	return
}

// ensureRaceLapsCoverRaceLength will re-add laps if when removing the pit-time
// from the total reace length will fall short of the race length + a lap for
// safety.
func (i *RaceInfo) ensureRaceLapsCoverRaceLength(raceLen, laptime, pittime, stops float64) {
	raceLen += (laptime)
	if totalLen := i.SafeLaps*laptime + stops*pittime; totalLen < raceLen {
		i.Laps += math.Ceil((raceLen - totalLen) / laptime)
		i.SafeLaps = i.Laps + 1
	}
}
