package entities

import (
	"time"

	"github.com/google/uuid"
)

type CarClass = string
type RaceClass = string

const (
	CarClassLMH  CarClass = "LMH"
	CarClassLMDh CarClass = "LMDh"
	CarClassP2   CarClass = "P2"
	CarClassGTE  CarClass = "GTE"
	CarClassGT3  CarClass = "GT3"

	RaceClassHypercar = "HYPERCAR"
	RaceClassLMP2     = "LMP2"
	RaceClassLMGTE    = "LMGTE"
	RaceClassLMGT3    = "LMGT3"
)

type Car struct {
	ID           uuid.UUID  `json:"id"`
	Class        CarClass   `json:"class"`
	RaceClass    RaceClass  `json:"race_class"`
	Manufacturer string     `json:"manufacturer"`
	Model        string     `json:"model"`
	Spec         *string    `json:"spec,omitempty"`
	Year         uint       `json:"year"`
	CreatedAt    time.Time  `json:"-"`
	UpdatedAt    *time.Time `json:"-"`
}

type CarList []Car
