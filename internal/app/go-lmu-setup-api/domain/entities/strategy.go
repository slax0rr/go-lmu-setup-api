package entities

import (
	"time"

	"github.com/google/uuid"
)

type StratType = string

var (
	StratType_FlatOut  StratType = "flat_out"
	StratType_FuelSave StratType = "fuel_save"
	StratType_NoStop   StratType = "no_stop"
)

type StintAdjustmentType = string

var (
	StintAdjustmentType_PitLength StintAdjustmentType = "pitlength"
	StintAdjustmentType_Laps      StintAdjustmentType = "laps"
)

type RaceLaps struct {
	StintLaps float64
	Laps      float64
	SafeLaps  float64
}

type Strategy struct {
	ID               uuid.UUID         `json:"id"`
	UserID           uuid.UUID         `json:"-"`
	Passphrase       string            `json:"passphrase,omitempty"`
	Name             string            `json:"name"                     validate:"required"`
	Data             StrategyData      `json:"data"                     validate:"required"`
	Progress         *RaceProgress     `json:"race_progress,omitempty"`
	StintAdjustments []StintAdjustment `json:"-"`
	CreatedAt        time.Time         `json:"-"`
	UpdatedAt        *time.Time        `json:"-"`
}

type StrategyList []Strategy

type StrategyData struct {
	RaceInfo     RaceInfo `json:"race_info"            validate:"required"`
	Laps         float64  `json:"laps"                 validate:"required,gt=0"`
	SafeLaps     float64  `json:"safe_laps"            validate:"required,gt=0"`
	MinRequired  FuelReq  `json:"min_required"         validate:"required"`
	SafeRequired FuelReq  `json:"safe_required"        validate:"required"`
	FuelRatio    float64  `json:"fuel_ratio,omitempty"`
	PitStrategy  PitStrat `json:"pit_strategy"         validate:"required"`
}

type RaceProgress struct {
	RemainingTime float64 `json:"remaining_time" validate:"lt:172800000,required_without=LapsComplete"` // less than 48 hrs
	LapsComplete  float64 `json:"laps_complete"  validate:"lt:100000"`
	FuelTank      float64 `json:"fuel_tank"      validate:"lt:100000"`
	EnergyTank    float64 `json:"energy_tank"    validate:"lt:100000"`
}

type StrategyInfo struct {
	Laps         float64    `json:"laps"`
	SafeLaps     float64    `json:"safe_laps"`
	MinRequired  FuelReq    `json:"min_required"`
	SafeRequired FuelReq    `json:"safe_required"`
	FuelRatio    float64    `json:"fuel_ratio,omitempty"`
	Strategies   []PitStrat `json:"strategies"`
	RecommStrat  StratType  `json:"recomm_strat"`
}

type PitStrat struct {
	Type   StratType `json:"type"   validate:"required"`
	Stops  int       `json:"stops"`
	Stints []Stint   `json:"stints" validate:"required"`
}

type FuelReq struct {
	Fuel   float64 `json:"fuel"`
	Energy float64 `json:"energy,omitempty"`
}

type StintAdjustment struct {
	StintNumber uint                `json:"stint_number"              validate:"required,gt=0"`
	Type        StintAdjustmentType `json:"type"`
	Laps        float64             `json:"laps,omitempty"`
	PitLength   int64               `json:"pit_length,omitempty"`
}
