package entities

import (
	"time"

	"github.com/google/uuid"
)

type Track struct {
	ID             uuid.UUID  `json:"id"`
	Country        string     `json:"country"`
	TrackName      string     `json:"track_name"`
	ShortTrackName string     `json:"short_track_name"`
	Year           int        `json:"year"`
	CreatedAt      time.Time  `json:"-"`
	UpdatedAt      *time.Time `json:"-"`
}

type TrackList []Track

type TrackLayout struct {
	ID        uuid.UUID  `json:"id"`
	TrackID   uuid.UUID  `json:"track_id"`
	Name      string     `json:"name"`
	Default   bool       `json:"default"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt *time.Time `json:"-"`
}

type TrackLayoutList []TrackLayout
