package constants

const (
	ErrorDetails_MissingOrMalformedResourceID string = "Missing or invalid resource ID parameter"
	ErrorDetails_InvalidRequestPayload        string = "The request payload is invalid"
	ErrorDetails_SetupFileMissing             string = "Setup file could not be located"
	ErrorDetails_StintNumberInvalid           string = "The supplied stint number is invalid"
)
