package services

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/repositories"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
)

type Setup struct {
	setupStore      repositories.SetupStorage
	trackLayoutRepo repositories.TrackLayout
}

func NewSetup(
	setupStore repositories.SetupStorage,
	trackLayoutRepo repositories.TrackLayout,
) *Setup {
	return &Setup{
		setupStore:      setupStore,
		trackLayoutRepo: trackLayoutRepo,
	}
}

func (s *Setup) CanUserUpdate(setup *entities.Setup, user *entities.User) bool {
	return user.ID == setup.UserID
}

func (s *Setup) SetUser(setup *entities.Setup, user *entities.User) {
	setup.UserID = user.ID
	setup.User = user.DisplayName
}

// SetFilenameForUpdate sets the filename from the saved setup to the request
// data if it is not set there in order to allow updates with empty filename.
func (s *Setup) SetFilenameForUpdate(reqSetup, savedSetup *entities.Setup) {
	if reqSetup.FileName == "" {
		// permit empty filenames when updating and don't update it
		reqSetup.FileName = savedSetup.FileName
	}
}

func (s *Setup) ShouldUpdateFileContent(setup *entities.Setup) bool {
	return setup.FileContent != ""
}

func (s *Setup) StoreSetupFile(ctx context.Context, setup *entities.Setup, opts ...postgresdb.ExecOption) error {
	file := entities.SetupFile{
		SetupID: setup.ID,
		Name:    setup.FileName,
		Content: []byte(setup.FileContent),
	}
	err := s.setupStore.Store(ctx, file, opts...)
	if err != nil {
		return fmt.Errorf("store setup: %w", err)
	}

	return nil
}

func (s *Setup) DefaultTrackLayoutFallback(ctx context.Context, setup *entities.Setup) error {
	if setup.TrackLayoutID != nil {
		return nil
	}

	var err error
	setup.TrackLayoutID, err = s.trackLayoutRepo.GetTrackDefault(ctx, setup.TrackID)
	if err != nil {
		if errors.Is(err, postgresdb.ErrNotFound) {
			return nil
		}
		return fmt.Errorf("get track default layout: %w", err)
	}

	return nil
}
