package services

import (
	"context"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/logger"
)

type RaceProgress struct {
	stintSvc StintService
}

func NewRaceProgress(
	stintSvc StintService,
) *RaceProgress {
	return &RaceProgress{
		stintSvc: stintSvc,
	}
}

func (p *RaceProgress) UpdateStrategyWithRaceProgress(
	ctx context.Context,
	strat *entities.Strategy,
	progress *entities.RaceProgress,
) {
	log := logger.FromContext(ctx)

	strat.Progress = progress

	if strat.Data.RaceInfo.Laps == 0 {
		strat.Data.RaceInfo.CalculateRaceLaps()
	}
	p.updateStintsWithProgress(ctx, strat, strat.Data.RaceInfo.StintLaps)

	log.Debug().
		Any("stints", strat.Data.PitStrategy.Stints).
		Msg("stints post progress adjustment")

	p.stintSvc.AdjustStintsToFitRaceLength(ctx, strat)

	log.Debug().
		Any("stints", strat.Data.PitStrategy.Stints).
		Msg("stints post race length adjustment")
}

func (p *RaceProgress) updateStintsWithProgress(
	ctx context.Context,
	strat *entities.Strategy,
	maxStintLaps float64,
) {
	lapsComplete := strat.Progress.LapsComplete

	for i := range strat.Data.PitStrategy.Stints {
		lapsComplete = p.stintSvc.UpdateCompleteLaps(ctx, strat, &strat.Data.PitStrategy.Stints[i],
			lapsComplete, maxStintLaps)
		if lapsComplete == 0 {
			break
		}
	}
}
