package services

import (
	"context"
	"errors"
	"fmt"
	"math"
	"time"

	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	domainErrors "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/errors"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/repositories"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/logger"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
)

const (
	defaultPitLength time.Duration = 90 * time.Second
	maxSavePercent   float64       = 1.15
)

type RaceStrategy struct {
	stratRepo repositories.Strategy
	stintSvc  StintService
}

func NewRaceStrategy(
	stratRepo repositories.Strategy,
	stintSvc StintService,
) *RaceStrategy {
	return &RaceStrategy{
		stratRepo: stratRepo,
		stintSvc:  stintSvc,
	}
}

func (s *RaceStrategy) EnsurePitLength(info *entities.RaceInfo) {
	if info.PitLength == 0 {
		info.PitLength = defaultPitLength.Milliseconds()
	}
}

func (s *RaceStrategy) FetchStrategy(
	ctx context.Context,
	id, userID uuid.UUID,
	passphrase string,
) (*entities.Strategy, error) {
	strat, err := s.stratRepo.GetByID(ctx, id)
	if err != nil {
		if errors.Is(err, postgresdb.ErrNotFound) {
			return nil, domainErrors.ErrResourceNotFound
		}
		return nil, fmt.Errorf("get strategy by id from the repository: %w", err)
	}

	if strat.UserID != userID && strat.Passphrase != passphrase {
		return nil, domainErrors.ErrResourceWithoutPermission
	}

	return strat, nil
}

func (s *RaceStrategy) GeneratePitStrategies(ctx context.Context, info entities.RaceInfo, strat *entities.StrategyInfo) {
	s.calculateFuelRequirement(strat, info)

	flatoutStrat := s.generatePitStrategy(ctx, info, false)
	if flatoutStrat != nil {
		strat.RecommStrat = flatoutStrat.Type
		strat.Strategies = append(strat.Strategies, *flatoutStrat)
	}

	if flatoutStrat == nil ||
		(len(flatoutStrat.Stints) > 1 &&
			flatoutStrat.Stints[len(flatoutStrat.Stints)-1].Laps < info.StintLaps) {
		saveStrat := s.generatePitStrategy(ctx, info, true)
		strat.Strategies = append(strat.Strategies, *saveStrat)

		if flatoutStrat == nil ||
			(flatoutStrat.Stints[0].TargetUse.Fuel/saveStrat.Stints[0].TargetUse.Fuel < maxSavePercent ||
				flatoutStrat.Stints[0].TargetUse.Energy/saveStrat.Stints[0].TargetUse.Energy < maxSavePercent) {
			strat.RecommStrat = saveStrat.Type
		}
	}
}

func (s *RaceStrategy) generatePitStrategy(
	ctx context.Context,
	info entities.RaceInfo,
	save bool,
) *entities.PitStrat {
	log := logger.FromContext(ctx)

	strat := entities.PitStrat{}

	var saveLapsPerStint, remainderLapsPerStint float64
	if save {
		saveLapsPerStint, remainderLapsPerStint = s.getSaveLapsPerStint(info)
	}
	var err error
	strat.Stints, err = s.calculateStints(info, saveLapsPerStint, remainderLapsPerStint)
	if err != nil {
		log.Info().
			Msg("stints failed to be calculated to use up all remainder laps making the non-save strategy invalid")
		return nil
	}

	log.Debug().
		Float64("save_laps_per_stint", saveLapsPerStint).
		Float64("remainder_laps_per_stint", remainderLapsPerStint).
		Any("stints", strat.Stints).
		Msg("stints after calculation")

	strat.Stops = len(strat.Stints) - 1
	strat.Type = s.determineStrategyType(strat.Stops, save)

	log.Debug().
		Any("strat_type", strat.Type).
		Msg("strategy type")

	return &strat
}

func (s *RaceStrategy) calculateFuelRequirement(strat *entities.StrategyInfo, info entities.RaceInfo) {
	// calculate the min and safe fuel requirements for whole race
	strat.MinRequired.Fuel = math.Ceil(strat.Laps*(info.FuelUse/100)) * 100
	strat.SafeRequired.Fuel = math.Ceil(strat.SafeLaps*(info.FuelUse/100)) * 100

	// if we have energy use, calculate min and safe energy requirement for
	// whole race and additionally calculate the fuel/energy ratio
	if info.EnergyUse > 0 {
		strat.MinRequired.Energy = math.Ceil(strat.Laps*(info.EnergyUse/100)) * 100
		strat.SafeRequired.Energy = math.Ceil(strat.SafeLaps*(info.EnergyUse/100)) * 100

		ratio := (math.Ceil(strat.SafeRequired.Fuel / strat.SafeRequired.Energy * 100)) / 100
		strat.FuelRatio = ratio * 100
	}
}

func (s *RaceStrategy) calculateStints(
	info entities.RaceInfo,
	saveLapsPerStint, remainderLapsPerStint float64,
) ([]entities.Stint, error) {
	var stintsArr []entities.Stint
	i := uint(0)
	raceLaps := info.SafeLaps
	for raceLaps > 0 {
		saveLaps := saveLapsPerStint
		// adjust save laps by adding a remainder lap on top of it
		if remainderLapsPerStint > 0 {
			saveLaps += 1
			remainderLapsPerStint -= 1
		}

		stintLaps := info.StintLaps + saveLaps
		if raceLaps < stintLaps {
			stintLaps = raceLaps
			saveLaps = 0
		}
		raceLaps -= stintLaps

		i += 1
		stint := s.stintSvc.CreateStint(stintLaps-saveLaps, saveLaps, info, i)
		if i > 1 {
			stint.PitLength = info.PitLength
		}
		stintsArr = append(stintsArr, stint)
	}

	if remainderLapsPerStint > 0 {
		return nil, fmt.Errorf("remainder laps have not reached 0 - strategy is invalid")
	}

	return stintsArr, nil
}

// getSaveLapsPerStint returns the number of laps that need to be saved on a
// save strategy per stint, to remove the last stint. The returned remainder is
// greater than 0 if the number of save laps per stint couldn't have been split
// evenly, which can later be added on top of the save laps in other stints.
func (s *RaceStrategy) getSaveLapsPerStint(info entities.RaceInfo) (saveLaps, remainder float64) {
	stints := math.Floor(info.SafeLaps / info.StintLaps)
	remainderLaps := int64(info.SafeLaps) % int64(info.StintLaps)

	// how many laps we need to save each stint
	saveLaps = math.Floor(float64(remainderLaps) / stints)
	// if we can't divide evenly, we need to split the remainder between the rest of the stints
	remainder = float64(remainderLaps % int64(stints))

	return
}

func (s *RaceStrategy) determineStrategyType(stops int, save bool) entities.StratType {
	switch {
	case stops == 0:
		return entities.StratType_NoStop

	case save:
		return entities.StratType_FuelSave

	default:
		return entities.StratType_FlatOut
	}
}
