package services

import (
	"context"
	"math"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	domainErrors "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/errors"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/logger"
)

type StintService interface {
	UpdateCompleteLaps(context.Context, *entities.Strategy, *entities.Stint, float64, float64) float64
	AdjustStintsToFitRaceLength(context.Context, *entities.Strategy)
	CreateStint(float64, float64, entities.RaceInfo, uint) entities.Stint
	ApplyStintAdjustment(context.Context, *entities.Strategy, *entities.StintAdjustment) error
}

type Stint struct {
}

func NewStint() *Stint {
	return &Stint{}
}

// UpdateCompleteLaps updates the stints with the number of complete laps and
// adjusts its length based on the remaining fuel/energy in the tank, as well
// as adjusting the fuel/energy target consumption.
func (s *Stint) UpdateCompleteLaps(
	ctx context.Context,
	strat *entities.Strategy,
	stint *entities.Stint,
	lapsComplete, maxStintLaps float64,
) float64 {
	log := logger.FromContext(ctx)

	stint.Laps += stint.CompletedLaps
	stint.CompletedLaps = 0
	stint.Complete = false

	log.Debug().
		Uint("stint_number", stint.StintNumber).
		Float64("stint_laps", stint.Laps).
		Float64("save_laps", stint.Save).
		Float64("completed_laps", lapsComplete).
		Msg("adjusting stint for complete laps")

	if lapsComplete >= stint.Laps {
		log.Debug().
			Uint("stint_number", stint.StintNumber).
			Msg("stint with less laps than already complete - marking as done")

		stintLaps := stint.Laps
		s.markStintComplete(stint)

		return lapsComplete - stintLaps
	}

	remainingLaps := stint.Laps - lapsComplete
	stint.CompletedLaps = lapsComplete

	remainingLaps = s.adjustRemainingLaps(strat, remainingLaps)
	saveLaps := s.getSaveLaps(remainingLaps, maxStintLaps)

	log.Debug().
		Uint("stint_number", stint.StintNumber).
		Float64("remaining_laps", remainingLaps).
		Float64("save_laps", saveLaps).
		Msg("remaining laps post fuel/energy tank update")

	stint.Laps = remainingLaps
	stint.Save = saveLaps

	s.adjustStintTargetUse(strat, stint, remainingLaps)

	log.Debug().
		Uint("stint_number", stint.StintNumber).
		Float64("fuel_tank", strat.Progress.FuelTank).
		Float64("energy_tank", strat.Progress.EnergyTank).
		Float64("fuel_use", stint.TargetUse.Fuel).
		Float64("energy_use", stint.TargetUse.Energy).
		Msg("updated fuel and energy use post remaining laps adjustment")

	return 0
}

// AdjustStintsToFitRaceLength after applying changes to stints from race
// progress or manual edits, we may need to adjust the rest so that they still
// fit the race length.
//
// TODO: Adjusting stints to fit race length can lead to invalid adjustments, no
// matter how the race length is determined, if it is by race length or by laps
// of the race. If a subset of race laps are driven at a much slower pace, i.e.
// under FCY or SC, or a badly damaged car, then the driven laps at the end of
// the race will not be the same as when driving at normal pace, nor will the
// laps driven counted at race pace reach the same time.
// https://gitlab.com/slax0rr/go-lmu-setup-api/-/issues/73
func (s *Stint) AdjustStintsToFitRaceLength(ctx context.Context, strat *entities.Strategy) {
	log := logger.FromContext(ctx)

	info := strat.Data.RaceInfo
	//stints := strat.Data.PitStrategy.Stints

	stintLength := int64(0)
	for _, stint := range strat.Data.PitStrategy.Stints {
		stintLength += (int64(stint.Laps+stint.CompletedLaps) * info.Laptime) + stint.PitLength
	}

	switch {
	// The stints are longer or equal to race time + 2 laps
	case stintLength >= info.RaceTime+(2*info.Laptime):
		overLaps := math.Floor(float64(stintLength-(info.RaceTime+info.Laptime)) / float64(info.Laptime))
		log.Debug().
			Int64("stint_length", stintLength).
			Float64("over_laps", overLaps).
			Msg("stints length longer than race time + 2 laps")

		s.shortenStints(ctx, strat, overLaps)

	// The stints are longer than race time + 1 lap
	case stintLength > info.RaceTime+info.Laptime:
		log.Debug().Int64("stint_length", stintLength).Msg("stints length longer than race time + 1 lap")
		return

	// The stints will not suffice for the full race length + 1 reserve lap
	case stintLength <= info.RaceTime+info.Laptime:
		log.Debug().Int64("stint_length", stintLength).Msg("stints length shorter than race time + 1 lap")
		missingLaps := math.Ceil(float64((info.RaceTime+info.Laptime)-stintLength) / float64(info.Laptime))

		s.extendShortStints(ctx, strat, missingLaps)
	}

	return
}

func (s *Stint) CreateStint(
	stintLaps, saveLaps float64,
	info entities.RaceInfo,
	stintNo uint,
) entities.Stint {
	capacity, _ := info.GetCapacityAndUse()

	laps := stintLaps + saveLaps

	stint := entities.Stint{
		StintNumber:   stintNo,
		Laps:          laps,
		Save:          saveLaps,
		TargetUse:     entities.FuelReq{},
		StintRequired: entities.FuelReq{},
	}

	stint.CalculateFuel(info.FuelUse, info.EnergyUse, capacity)

	return stint
}

func (s *Stint) ApplyStintAdjustment(
	ctx context.Context,
	strat *entities.Strategy,
	stintAdj *entities.StintAdjustment,
) error {
	log := logger.FromContext(ctx)

	if strat.Data.RaceInfo.Laps == 0 {
		strat.Data.RaceInfo.CalculateRaceLaps()
	}

	if strat.Data.PitStrategy.Stints[stintAdj.StintNumber-1].Complete {
		return domainErrors.ErrStintComplete
	}

	switch stintAdj.Type {
	case entities.StintAdjustmentType_PitLength:
		s.adjustPitLength(ctx, strat, stintAdj)

	case entities.StintAdjustmentType_Laps:
		s.adjustStintLaps(ctx, strat, stintAdj)

	default:
		log.Error().Str("adjustment_type", stintAdj.Type).
			Msg("unknown adjustment type")
		return domainErrors.ErrUnknownStintAdjustment
	}

	return nil
}

func (s *Stint) adjustPitLength(ctx context.Context, strat *entities.Strategy, stintAdj *entities.StintAdjustment) {
	strat.Data.PitStrategy.Stints[stintAdj.StintNumber-1].PitLength = stintAdj.PitLength

	s.AdjustStintsToFitRaceLength(ctx, strat)
}

func (s *Stint) adjustStintLaps(
	ctx context.Context,
	strat *entities.Strategy,
	stintAdj *entities.StintAdjustment,
) error {
	log := logger.FromContext(ctx)

	if stintAdj.Laps == 0 {
		return domainErrors.ErrZeroOrNegativeStintLaps
	}

	stint := strat.Data.PitStrategy.Stints[stintAdj.StintNumber-1]

	log.Debug().
		Uint("stint_number", stintAdj.StintNumber).
		Float64("completed_laps", stint.CompletedLaps).
		Float64("adjustment_laps", stintAdj.Laps).
		Msg("applying user lap adjustments")

	stint.Laps = stintAdj.Laps - stint.CompletedLaps

	stint.Save = 0
	if stint.Laps > strat.Data.RaceInfo.StintLaps {
		stint.Save = stint.Laps - strat.Data.RaceInfo.StintLaps
	}

	if stint.Laps > 0 {
		log.Debug().Msg("calculating stint fuel requirement post lap adjustment")

		capacity, _ := strat.Data.RaceInfo.GetCapacityAndUse()

		if strat.Progress != nil && stint.CompletedLaps > 0 {
			log.Debug().
				Float64("fuel_tank", strat.Progress.FuelTank).
				Float64("energy_tank", strat.Progress.EnergyTank).
				Msg("limiting capacity to remaining tank state from progress")

			capacity = strat.Progress.FuelTank
			if strat.Progress.EnergyTank > 0 {
				capacity = strat.Progress.EnergyTank
			}
		}

		stint.CalculateFuel(strat.Data.RaceInfo.FuelUse, strat.Data.RaceInfo.EnergyUse, capacity)
	}

	strat.Data.PitStrategy.Stints[stintAdj.StintNumber-1] = stint

	log.Debug().
		Any("stints", strat.Data.PitStrategy.Stints).
		Msg("stints after lap adjustment")

	strat.Data.PitStrategy.Stints[stintAdj.StintNumber-1].Locked = true

	s.AdjustStintsToFitRaceLength(ctx, strat)

	strat.Data.PitStrategy.Stints[stintAdj.StintNumber-1].Locked = false

	log.Debug().
		Any("stints", strat.Data.PitStrategy.Stints).
		Msg("stints after race langth and lap adjustment")

	return nil
}

func (s *Stint) shortenStints(
	ctx context.Context,
	strat *entities.Strategy,
	overLaps float64,
) entities.Stints {
	log := logger.FromContext(ctx)

	stints := strat.Data.PitStrategy.Stints
	defer func() {
		strat.Data.PitStrategy.Stints = stints
	}()

	stintLaps := float64(0)
	saveLaps := float64(0)
	for i := len(stints) - 1; i >= 0 && overLaps > 0; i-- {
		stintLaps = stints[i].Laps - stints[i].Save

		// subtract overlaps from save laps
		if stints[i].Save > 0 && stints[i].Save >= overLaps {
			saveLaps = stints[i].Save - overLaps

			overLaps = 0
		} else if stints[i].Save > 0 {
			saveLaps = 0

			overLaps -= stints[i].Save
		}

		// subtract overlaps from stint laps
		if stintLaps >= overLaps {
			stintLaps = stintLaps - overLaps

			overLaps = 0
		} else {
			stintLaps = 0

			overLaps -= stints[i].Laps
		}

		log.Debug().
			Uint("stint_num", stints[i].StintNumber).
			Float64("over_laps", overLaps).
			Float64("stint_laps", stintLaps).
			Float64("save_laps", saveLaps).
			Msg("save laps adjusted for stint")

		// no laps remaining - remove the stint
		if stintLaps == 0 {
			log.Debug().
				Uint("stint_num", stints[i].StintNumber).
				Float64("over_laps", overLaps).
				Msg("removing stint due to laps reaching 0")

			stints = append(stints[:i], stints[i+1:]...)

			continue
		}

		log.Debug().
			Uint("stint_num", stints[i].StintNumber).
			Float64("stint_laps", stintLaps).
			Float64("save_laps", saveLaps).
			Msg("updating stint to new amount of laps")

		// create a new stint from the new stint numbers
		newStint := s.CreateStint(stintLaps, saveLaps, strat.Data.RaceInfo, stints[i].StintNumber)

		// carry over any completed laps and pitlength to the new stint
		newStint.CompletedLaps = stints[i].CompletedLaps
		newStint.PitLength = stints[i].PitLength

		if strat.Progress != nil {
			s.adjustStintTargetUse(strat, &newStint, stintLaps)
		}

		stints[i] = newStint

		log.Debug().
			Float64("over_laps", overLaps).
			Msg("stint length reduced")
	}

	return stints
}

func (s *Stint) extendShortStints(ctx context.Context, strat *entities.Strategy, missingLaps float64) {
	log := logger.FromContext(ctx)

	maxStintLaps := strat.Data.RaceInfo.StintLaps
	maxSaveLaps := strat.Data.RaceInfo.GetMaxSaveStintLaps(maxSavePercent) - maxStintLaps
	stints := strat.Data.PitStrategy.Stints
	defer func() {
		strat.Data.PitStrategy.Stints = stints
	}()

	log.Debug().
		Float64("missing_laps", missingLaps).
		Float64("max_save_laps", maxSaveLaps).
		Float64("max_stint_laps", maxStintLaps).
		Msg("extending stints for the amount of missing laps")

	for i, stint := range stints {
		// skip extending the stint if it's in progress or if the stint is
		// locked for edits and is not the final stint
		if stint.CompletedLaps > 0 || (stint.Locked && i != len(stints)-1) {
			log.Debug().
				Uint("stint_num", stint.StintNumber).
				Float64("completed_laps", stint.CompletedLaps).
				Bool("complete", stint.Complete).
				Bool("locked", stint.Locked).
				Msg("stint completed, locked, or is in progress")
			continue
		}

		newLaps := stint.Laps + missingLaps
		if newLaps > maxStintLaps {
			newLaps = maxStintLaps
			missingLaps -= (maxStintLaps - stint.Laps)
		} else {
			missingLaps = 0
		}

		saveLaps := float64(0)
		if newLaps == maxStintLaps && missingLaps > 0 {
			saveLaps = maxSaveLaps
			if saveLaps > missingLaps {
				saveLaps = missingLaps
			}
			missingLaps -= saveLaps
		}

		log.Debug().
			Uint("stint_num", stint.StintNumber).
			Float64("new_laps", newLaps).
			Float64("new_save_laps", saveLaps).
			Msg("extending stint to new amount of laps")

		newStint := s.CreateStint(newLaps, saveLaps, strat.Data.RaceInfo, stint.StintNumber)

		newStint.CompletedLaps = stint.CompletedLaps
		newStint.PitLength = stint.PitLength

		if strat.Progress != nil {
			s.adjustStintTargetUse(strat, &newStint, newLaps)
		}

		stints[i] = newStint

		log.Debug().
			Float64("missing_laps", missingLaps).
			Msg("remaining missing laps post stint extension")

		if missingLaps == 0 {
			return
		}
	}

	stints = s.addStintsPostStintExtensions(ctx, stints, strat.Data.RaceInfo, missingLaps, maxStintLaps, maxSaveLaps)

	return
}

// addStintsPostStintExtension adds additional stints if the strategy is still
// missing some laps.
func (s *Stint) addStintsPostStintExtensions(
	ctx context.Context,
	stints entities.Stints,
	info entities.RaceInfo,
	missingLaps, maxStintLaps, maxSaveLaps float64,
) entities.Stints {
	for missingLaps == 0 {
		return stints
	}

	log := logger.FromContext(ctx)

	laps := missingLaps
	saveLaps := float64(0)
	if missingLaps >= maxStintLaps+maxSaveLaps {
		laps = maxStintLaps + maxSaveLaps
		saveLaps = maxSaveLaps
	} else if missingLaps > maxStintLaps {
		saveLaps = missingLaps - maxStintLaps
	}

	missingLaps -= laps

	log.Debug().
		Float64("laps", laps).
		Float64("save_laps", saveLaps).
		Msg("creating new stint")

	stintNum := stints[len(stints)-1].StintNumber + 1
	stint := s.CreateStint(laps, saveLaps, info, stintNum)
	stint.PitLength = info.PitLength

	stints = append(stints, stint)

	log.Debug().
		Float64("missing_laps", missingLaps).
		Msg("remaining missing laps post stint creation")

	return stints
}

// adjustStintTargetUse adjusts the fuel and energy usage per lap for the stint
// based on the remaining laps.
func (*Stint) adjustStintTargetUse(strat *entities.Strategy, stint *entities.Stint, remainingLaps float64) {
	stint.TargetUse.Fuel = math.Ceil(strat.Progress.FuelTank / remainingLaps)
	stint.StintRequired.Fuel = strat.Progress.FuelTank

	if strat.Progress.EnergyTank > 0 {
		stint.TargetUse.Energy = math.Ceil(strat.Progress.EnergyTank / remainingLaps)
		stint.StintRequired.Energy = strat.Progress.EnergyTank
		if strat.Progress.FuelTank == 0 {
			stint.TargetUse.Fuel = stint.TargetUse.Energy * stint.FuelRatio
		}
	}
}

// markStintComplete and reset laps, stint fuel/energy requirement, and target
// usage to 0.
func (*Stint) markStintComplete(stint *entities.Stint) {
	stint.Complete = true
	stint.CompletedLaps = stint.Laps
	stint.Laps = 0
	stint.Save = 0
	stint.StintRequired.Fuel = 0
	stint.StintRequired.Energy = 0
	stint.TargetUse.Fuel = 0
	stint.TargetUse.Energy = 0
}

// adjustRemainingLaps determines if the remaining stint laps can be extended
// any further in case the fuel or virtual energy tank have more fuel or virtual
// energy remaining in the tank.
func (*Stint) adjustRemainingLaps(strat *entities.Strategy, remainingLaps float64) float64 {
	tank := strat.Progress.FuelTank
	use := strat.Data.RaceInfo.FuelUse
	if strat.Progress.EnergyTank > 0 {
		tank = strat.Progress.EnergyTank
		use = strat.Data.RaceInfo.EnergyUse
	}
	if tankLaps := math.Round(tank / use); tankLaps > remainingLaps {
		return tankLaps
	}
	return remainingLaps
}

// getSaveLaps returns the save laps of the stint if the remaining laps are
// greater than the maximum stint laps.
func (*Stint) getSaveLaps(remainingLaps, maxStintLaps float64) (saveLaps float64) {
	if remainingLaps > maxStintLaps {
		saveLaps = remainingLaps - maxStintLaps
	}
	return
}
