package errors

import "fmt"

var (
	ErrResourceNotFound          = fmt.Errorf("Requested resource could not be found")
	ErrResourceWithoutPermission = fmt.Errorf("User does not have permission to access or modify this resource")

	ErrStintLapsZero = fmt.Errorf("Invalid strategy data - resulting Laps Per Stint are zero or negative - adjust your consumption and/or tank capacity")

	ErrStintComplete           = fmt.Errorf("Changes on completed stints are not possible")
	ErrUnknownStintAdjustment  = fmt.Errorf("Requested adjustment is unknown")
	ErrZeroOrNegativeStintLaps = fmt.Errorf("Stint can not have zero or negative laps")

	ErrTrackLayoutInvalid = fmt.Errorf("The suplied track layout is invalid for the selected track")
)
