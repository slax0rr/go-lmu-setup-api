package settings

import (
	"cmp"
	"fmt"
	"os"

	"github.com/rs/zerolog"

	postgresdbComponent "gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/env"
)

const (
	defaultServiceName        string        = "go-lmu-setup-api"
	defaultLogLevel           zerolog.Level = zerolog.DebugLevel
	defaultPort               int           = 8000
	defaultDatabasePort       uint          = 5432
	defaultMigrationsLocation string        = "./db/migrations/"
	defaultUserTokenHeader    string        = "X-AuthAPI-UserToken"
)

var (
	ServiceName string
	ServerPort  int
	LogLevel    zerolog.Level

	DatabaseDSN        string
	MigrationsLocation string

	UserTokenHeader     string
	EncodedUserTokenKey []byte
)

func Configure() error {
	ServiceName = cmp.Or(os.Getenv("SERVICE_NAME"), defaultServiceName)
	ServerPort = int(env.GetInt64OrDefault("SERVER_PORT", int64(defaultPort)))
	UserTokenHeader = defaultUserTokenHeader

	DatabaseDSN = cmp.Or(os.Getenv("DATABASE_DSN"), postgresdbComponent.BuildDSN(
		os.Getenv("DATABASE_USER"),
		os.Getenv("DATABASE_PASSWORD"),
		os.Getenv("DATABASE_HOST"),
		os.Getenv("DATABASE_NAME"),
		uint(env.GetUintOrDefault("DATABASE_PORT", uint64(defaultDatabasePort))),
	).String())

	level, err := zerolog.ParseLevel(cmp.Or(os.Getenv("LOG_LEVEL"),
		defaultLogLevel.String()))
	if err != nil {
		return fmt.Errorf("parse log level: %w", err)
	}
	zerolog.SetGlobalLevel(level)
	LogLevel = level

	MigrationsLocation = cmp.Or(os.Getenv("DB_MIGRATIONS_LOCATION"),
		defaultMigrationsLocation)

	EncodedUserTokenKey = []byte(os.Getenv("USER_TOKEN_ENCODED_KEY"))

	return nil
}
