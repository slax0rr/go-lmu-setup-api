package application

import (
	"fmt"

	"github.com/rs/zerolog"
)

func Bootstrap(log *zerolog.Logger) error {
	service, err := NewService(log)
	if err != nil {
		return fmt.Errorf("create new service: %w", err)
	}
	return service.Start()
}
