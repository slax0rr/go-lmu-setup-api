package application

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/application/routes"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/application/services"
	domainServices "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/services"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/validators"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/auth"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/database/repositories"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/settings"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify"
	healthComponent "gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/health"
	httpComponent "gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/middleware"
	postgresdbComponent "gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/data"
)

type Application struct {
	log    *zerolog.Logger
	router *gin.RouterGroup

	dao *data.AccessObject
}

func NewService(log *zerolog.Logger) (*bootstrapify.Service, error) {
	err := settings.Configure()
	if err != nil {
		return nil, fmt.Errorf("configure settings: %w", err)
	}

	app := &Application{
		log: log,
	}

	service := bootstrapify.NewService(
		bootstrapify.Config{
			Logger: log,
		},
		bootstrapify.Component(
			httpComponent.New(),
			httpComponent.WithServerPort(settings.ServerPort),
			httpComponent.WithRequestLogger(),
		),
		bootstrapify.Component(
			healthComponent.New(
				postgresdbComponent.ComponentName,
			),
		),
		bootstrapify.Component(
			postgresdbComponent.New(),
			postgresdbComponent.WithDSN(settings.DatabaseDSN),
			postgresdbComponent.WithMigrations(settings.MigrationsLocation),
		),
		bootstrapify.Component(
			healthComponent.New(
				postgresdbComponent.ComponentName,
			),
		),
		bootstrapify.Run(
			settings.ServiceName,
			bootstrapify.RunAfter(
				httpComponent.ComponentName,
				postgresdbComponent.ComponentName,
			),
			bootstrapify.RunAfterStart(app.Setup),
		),
	)

	return service, nil
}

func (app *Application) Setup(c *bootstrapify.RunComponent) error {
	app.log.Debug().Msg("application setup")

	pgComponent, ok := c.Dependency(postgresdbComponent.ComponentName).(*postgresdbComponent.Component)
	if !ok {
		return fmt.Errorf("unable to load '%s' component", postgresdbComponent.ComponentName)
	}

	hc, ok := c.Dependency(httpComponent.ComponentName).(*httpComponent.Component)
	if !ok {
		return fmt.Errorf("unable to load '%s' component", httpComponent.ComponentName)
	}

	var err error
	hc.Router().Use(middleware.ErrorResponse(app.log))
	app.router = hc.Router().Group("/api")
	dbpool := pgComponent.Pool()
	app.dao = data.NewAccessObject(dbpool)

	err = auth.Configure(settings.EncodedUserTokenKey)
	if err != nil {
		return fmt.Errorf("configure auth lib: %w", err)
	}

	app.setupRoutes()

	// register domain custom validators
	validators.RegisterCustomValidations()

	return nil
}

func (app *Application) setupRoutes() error {
	v1Router := app.router.Group("/v1")

	setupRepo := repositories.NewSetup(app.dao.GetDBPool())
	setupFileRepo := repositories.NewSetupFile(app.dao.GetDBPool())
	strategyRepo := repositories.NewStrategy(app.dao.GetDBPool())
	trackLayoutRepo := repositories.NewTrackLayout(app.dao.GetDBPool())

	setupDomainSvc := domainServices.NewSetup(setupFileRepo, trackLayoutRepo)
	stintDomSvc := domainServices.NewStint()
	raceStratDomSvc := domainServices.NewRaceStrategy(strategyRepo, stintDomSvc)
	raceProgressDomSvc := domainServices.NewRaceProgress(stintDomSvc)

	carSvc := services.NewCars(app.dao, repositories.NewCar(app.dao.GetDBPool()))
	trackSvc := services.NewTracks(app.dao, repositories.NewTrack(app.dao.GetDBPool()))
	trackLayoutSvc := services.NewTrackLayouts(app.dao, trackLayoutRepo)
	setupSvc := services.NewSetup(app.dao, setupDomainSvc, setupRepo, setupFileRepo)
	strategySvc := services.NewStrategy(strategyRepo, raceStratDomSvc, raceProgressDomSvc, stintDomSvc)

	routes.RegisterCarsRoutes(v1Router, carSvc)
	routes.RegisterTracksRoutes(v1Router, trackSvc)
	routes.RegisterTrackLayoutsRoutes(v1Router, trackLayoutSvc)
	routes.RegisterSetupsRoutes(v1Router, setupSvc)
	routes.RegisterStrategiesRoutes(v1Router, strategySvc)

	return nil
}
