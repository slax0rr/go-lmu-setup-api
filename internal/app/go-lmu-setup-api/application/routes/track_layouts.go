package routes

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/application/services"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http/controllers"
)

func RegisterTrackLayoutsRoutes(router *gin.RouterGroup, trackSvc services.TrackLayoutService) {
	group := router.Group("/tracks/layouts")
	group.GET("", controllers.ListTrackLayouts(trackSvc))
	group.GET("/:id", controllers.GetTrackLayout(trackSvc))
}
