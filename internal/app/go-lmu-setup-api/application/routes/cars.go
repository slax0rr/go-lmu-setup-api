package routes

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/application/services"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http/controllers"
)

func RegisterCarsRoutes(router *gin.RouterGroup, carSvc services.CarService) {
	group := router.Group("/cars")
	group.GET("", controllers.ListCars(carSvc))
	group.GET("/:id", controllers.GetCar(carSvc))
}
