package routes

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/application/services"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http/controllers"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http/middleware"
)

func RegisterStrategiesRoutes(router *gin.RouterGroup, stratSvc services.StrategyService) {
	group := router.Group("/strategies")
	group.POST("", controllers.CalculateStrategy(stratSvc))

	enriched := router.Group("/strategies/:strategy_id").Use(middleware.EnrichUserData())
	enriched.GET("", controllers.GetStrategy(stratSvc))
	enriched.POST("/progress", controllers.AdjustStrategyWithProgress(stratSvc))

	stintGroup := router.Group("/strategies/:strategy_id/stint/:stint_num").Use(middleware.EnrichUserData())
	stintGroup.PUT("/:type", controllers.EditStint(stratSvc))

	protected := router.Group("/strategies").Use(middleware.CheckAuth())
	protected.POST("/save", controllers.SaveStrategy(stratSvc))
	protected.GET("", middleware.Pagination(), controllers.GetUserStrategies(stratSvc))
}
