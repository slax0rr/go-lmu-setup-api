package routes

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/application/services"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http/controllers"
)

func RegisterTracksRoutes(router *gin.RouterGroup, trackSvc services.TrackService) {
	group := router.Group("/tracks")
	group.GET("", controllers.ListTracks(trackSvc))
	group.GET("/:id", controllers.GetTrack(trackSvc))
}
