package routes

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/application/services"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http/controllers"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http/middleware"
)

func RegisterSetupsRoutes(router *gin.RouterGroup, setupSvc services.SetupService) {
	group := router.Group("/setups", middleware.EnrichUserData())
	group.GET("", middleware.QueryFilter(), middleware.Pagination(), controllers.ListSetups(setupSvc))
	group.GET("/:setup_id", controllers.GetSetup(setupSvc))

	group.GET("/download/:setup_id", middleware.NoCache(), controllers.GetSetupFile(setupSvc))

	authGroup := router.Group("/setups", middleware.CheckAuth())
	authGroup.GET("/me", middleware.Pagination(), controllers.GetUserSetups(setupSvc))
	authGroup.POST("", controllers.AddSetup(setupSvc))
	authGroup.PUT("/:setup_id", controllers.UpdateSetup(setupSvc))
	authGroup.POST("/rate/:vote/:setup_id", controllers.RecordVote(setupSvc))
}
