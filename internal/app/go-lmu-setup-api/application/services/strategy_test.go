package services_test

import (
	"context"
	"encoding/json"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/application/services"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/repositories"
	domainServices "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/services"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/tests/testsupport"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/tests/testsupport/mocks"
)

type stintExpectation struct {
	count int
	stint entities.Stint
}

func getStrategyService(stratRepo repositories.Strategy) *services.Strategy {
	stintDomSvc := domainServices.NewStint()
	raceStratDomSvc := domainServices.NewRaceStrategy(stratRepo, stintDomSvc)
	raceProgressDomSvc := domainServices.NewRaceProgress(stintDomSvc)

	return services.NewStrategy(stratRepo, raceStratDomSvc, raceProgressDomSvc, stintDomSvc)
}

func buildStintExpectation(
	stintExp []stintExpectation,
) []entities.Stint {
	stints := make([]entities.Stint, 0)
	stintNum := uint(1)

	for _, exp := range stintExp {
		for i := 0; i < exp.count; i++ {
			exp.stint.StintNumber = stintNum
			stints = append(stints, exp.stint)
			if stintNum == 1 {
				stints[i].PitLength = 0
			}
			stintNum++
		}
	}

	return stints
}

func TestCalculatePitStrategy(t *testing.T) {
	tcs := []struct {
		name        string
		info        entities.RaceInfo
		strategies  []entities.PitStrat
		recommStrat entities.StratType
	}{
		// 1 splash&dash lap at the end
		{
			name: "1 splash&dash lap at the end",
			info: entities.RaceInfo{
				FuelUse:   441,
				EnergyUse: 452,
				RaceTime:  (45 * time.Minute).Milliseconds(),
				Laptime:   (2*time.Minute + 2*time.Second).Milliseconds(),
			},
		},
		// long lap high consumption race
		{
			name: "long lap high consumption race",
			info: entities.RaceInfo{
				FuelUse:      687,
				TankCapacity: 9700,
				RaceTime:     (240 * time.Minute).Milliseconds(),
				Laptime:      (3*time.Minute + 52*time.Second).Milliseconds(),
			},
		},
		// long lap high consumption race with energy
		{
			name: "long lap high consumption race with energy",
			info: entities.RaceInfo{
				FuelUse:   771,
				EnergyUse: 901,
				RaceTime:  (240 * time.Minute).Milliseconds(),
				Laptime:   (3*time.Minute + 28*time.Second).Milliseconds(),
			},
		},
		// save strat with one less pitstop very close to race length
		// when one pitstop is subtracted to calculate the save strategy it can
		// leave the total laps + one less pitstop to fall short of the race
		// length or be very close to it, in such cases and extra lap should be
		// added.
		{
			name: "save strat with one less pitstop very close to race length",
			info: entities.RaceInfo{
				FuelUse:   295,
				EnergyUse: 328,
				RaceTime:  (100 * time.Minute).Milliseconds(),
				Laptime:   (1*time.Minute + 31*time.Second).Milliseconds(),
			},
		},
		// longer stop than lap
		// not sure this scenario is realistically viable, we want to ensure that
		// after the estimated pit time is deducted from the race laps, the
		// remaining laps still get us to the end
		// example:
		// 100min race with 1min lap
		// 100 lap race with 11 laps per stint - 9 pitstops @ 90s per stop
		// adjusting the 100 laps for the 9 pitstops leaves us with 87 laps
		// 87 laps with 11 laps per stint - 7 stops @ 90s per stop
		// 87 laps * 1 min + 7 * 1.5min stop = 97.5min
		// now we're missing 2.5 min in the race time, meaning the race will be
		// 90 laps long instead
		// now we add those 3 remaining laps and calculate it in as mandatory
		// fuel save laps
		{
			name: "longer stop than lap",
			info: entities.RaceInfo{
				FuelUse:      1000,
				TankCapacity: 11000,
				RaceTime:     (100 * time.Minute).Milliseconds(),
				Laptime:      (1 * time.Minute).Milliseconds(),
			},
		},
		// low timer when crossing the finish line on last lap
		{
			name: "low timer when crossing the finish line on last lap",
			info: entities.RaceInfo{
				FuelUse:   826,
				EnergyUse: 852,
				RaceTime:  (35 * time.Minute).Milliseconds(),
				Laptime:   (3*time.Minute + 28*time.Second).Milliseconds(),
			},
		},
		{
			name: "re-added lap post initial stint length adjustment",
			info: entities.RaceInfo{
				FuelUse:   480,
				EnergyUse: 480,
				RaceTime:  (108 * time.Minute).Milliseconds(),
				Laptime:   (1*time.Minute + 47*time.Second + 289*time.Millisecond).Milliseconds(),
			},
		},
		// no stop race
		{
			name: "no stop race",
			info: entities.RaceInfo{
				FuelUse:   771,
				EnergyUse: 901,
				RaceTime:  (25 * time.Minute).Milliseconds(),
				Laptime:   (3*time.Minute + 28*time.Second).Milliseconds(),
			},
		},
	}

	s := getStrategyService(nil)
	for _, tc := range tcs {
		strat, err := s.CalculatePitRequirement(context.Background(), tc.info)
		require.NoErrorf(t, err, "unexpected error for test '%s'", tc.name)

		output, err := json.MarshalIndent(strat, "", "  ")
		require.NoError(t, err, "failed to marshal output for test '%s'", tc.name)

		testsupport.AssertGoldenFile(t, tc.name, output)
	}
}

func TestApplyRaceProgress(t *testing.T) {
	tcs := []struct {
		name     string
		info     entities.RaceInfo
		progress entities.RaceProgress
	}{
		{
			name: "90 minute race - save strat",
			info: entities.RaceInfo{
				FuelUse:   500,
				EnergyUse: 520,
				RaceTime:  (90 * time.Minute).Milliseconds(),
				Laptime:   (1*time.Minute + 29*time.Second).Milliseconds(),
				PitLength: 45000,
			},
			progress: entities.RaceProgress{
				RemainingTime: float64((time.Minute * 77).Milliseconds()),
				LapsComplete:  10,
				FuelTank:      4850,
				EnergyTank:    5000,
			},
		},
		{
			name: "single stint - progress",
			info: entities.RaceInfo{
				FuelUse:   400,
				EnergyUse: 400,
				RaceTime:  (60 * time.Minute).Milliseconds(),
				Laptime:   (2*time.Minute + 51*time.Second).Milliseconds(),
			},
			progress: entities.RaceProgress{
				LapsComplete: 10,
				FuelTank:     5200,
				EnergyTank:   5400,
			},
		},
	}

	for _, tc := range tcs {
		stratRepo := mocks.NewMockStrategy(t)
		s := getStrategyService(stratRepo)

		ctx := context.Background()
		stratInfo, err := s.CalculatePitRequirement(ctx, tc.info)
		require.NoErrorf(t, err, "unexpected error calculating strategy for test '%s'", tc.name)

		strat := &entities.Strategy{
			ID:         uuid.New(),
			UserID:     uuid.New(),
			Passphrase: "foobar",
			Name:       tc.name,
			Data: entities.StrategyData{
				RaceInfo:     tc.info,
				Laps:         stratInfo.Laps,
				SafeLaps:     stratInfo.SafeLaps,
				MinRequired:  stratInfo.MinRequired,
				SafeRequired: stratInfo.SafeRequired,
				FuelRatio:    stratInfo.FuelRatio,
			},
			CreatedAt: time.Now().UTC(),
		}
		for _, pitStrat := range stratInfo.Strategies {
			if pitStrat.Type == stratInfo.RecommStrat {
				strat.Data.PitStrategy = pitStrat
			}
		}
		stratRepo.EXPECT().GetByID(ctx, strat.ID).Return(strat, nil)
		stratRepo.EXPECT().StoreRecalculatedStrategy(ctx, strat).Return(nil)

		replaced, err := s.ApplyRaceProgress(ctx, strat.ID, strat.UserID, strat.Passphrase, &tc.progress)
		require.NoErrorf(t, err, "unexpected error recalculating strategy for test '%s'", tc.name)

		output, err := json.MarshalIndent(replaced, "", "  ")
		require.NoError(t, err, "failed to marshal output for test '%s'", tc.name)

		testsupport.AssertGoldenFile(t, tc.name, output)
	}
}

func TestEditStint(t *testing.T) {
	tcs := []struct {
		name     string
		info     entities.RaceInfo
		stintAdj entities.StintAdjustment
	}{
		{
			name: "90 minute race - save strat - adjust pit length",
			info: entities.RaceInfo{
				FuelUse:   500,
				EnergyUse: 520,
				RaceTime:  (90 * time.Minute).Milliseconds(),
				Laptime:   (1*time.Minute + 29*time.Second).Milliseconds(),
				PitLength: 45000,
			},
			stintAdj: entities.StintAdjustment{
				Type:        entities.StintAdjustmentType_PitLength,
				StintNumber: 2,
				PitLength:   180000,
			},
		},
		{
			name: "90 minute race - save strat - adjust stint laps",
			info: entities.RaceInfo{
				FuelUse:   500,
				EnergyUse: 520,
				RaceTime:  (90 * time.Minute).Milliseconds(),
				Laptime:   (1*time.Minute + 29*time.Second).Milliseconds(),
				PitLength: 45000,
			},
			stintAdj: entities.StintAdjustment{
				Type:        entities.StintAdjustmentType_Laps,
				StintNumber: 2,
				Laps:        14,
			},
		},
	}

	for _, tc := range tcs {
		stratRepo := mocks.NewMockStrategy(t)
		s := getStrategyService(stratRepo)

		ctx := context.Background()
		stratInfo, err := s.CalculatePitRequirement(ctx, tc.info)
		require.NoErrorf(t, err, "unexpected error calculating strategy for test '%s'", tc.name)

		strat := &entities.Strategy{
			ID:         uuid.New(),
			UserID:     uuid.New(),
			Passphrase: "foobar",
			Name:       tc.name,
			Data: entities.StrategyData{
				RaceInfo:     tc.info,
				Laps:         stratInfo.Laps,
				SafeLaps:     stratInfo.SafeLaps,
				MinRequired:  stratInfo.MinRequired,
				SafeRequired: stratInfo.SafeRequired,
				FuelRatio:    stratInfo.FuelRatio,
			},
			CreatedAt: time.Now().UTC(),
		}
		for _, pitStrat := range stratInfo.Strategies {
			if pitStrat.Type == stratInfo.RecommStrat {
				strat.Data.PitStrategy = pitStrat
			}
		}
		stratRepo.EXPECT().GetByID(ctx, strat.ID).Return(strat, nil)
		stratRepo.EXPECT().StoreRecalculatedStrategy(ctx, strat).Return(nil)

		replaced, err := s.EditStint(ctx, strat.ID, strat.UserID, strat.Passphrase, tc.stintAdj)
		require.NoErrorf(t, err, "unexpected error adjust stint pit length for test '%s'", tc.name)

		output, err := json.MarshalIndent(replaced, "", "  ")
		require.NoError(t, err, "failed to marshal output for test '%s'", tc.name)

		testsupport.AssertGoldenFile(t, tc.name, output)
	}
}

func TestEditPitStrategy(t *testing.T) {
	tcs := []struct {
		name       string
		info       entities.RaceInfo
		stintAdjs  []entities.StintAdjustment
		progresses []entities.RaceProgress
	}{
		{
			name: "extended stints with partially complete stint",
			info: entities.RaceInfo{
				FuelUse:   422,
				EnergyUse: 510,
				RaceTime:  (120 * time.Minute).Milliseconds(),
				Laptime:   (2*time.Minute + 19*time.Second).Milliseconds(),
				PitLength: 90000,
			},
			stintAdjs: []entities.StintAdjustment{
				{
					Type:        entities.StintAdjustmentType_Laps,
					StintNumber: 1,
					Laps:        21,
				},
				{
					Type:        entities.StintAdjustmentType_Laps,
					StintNumber: 2,
					Laps:        21,
				},
			},
			progresses: []entities.RaceProgress{
				{
					RemainingTime: float64((time.Minute * 95).Milliseconds()),
					LapsComplete:  10,
					FuelTank:      4500,
					EnergyTank:    5500,
				},
			},
		},
		{
			name: "extended stints with double progress applied",
			info: entities.RaceInfo{
				FuelUse:   422,
				EnergyUse: 510,
				RaceTime:  (120 * time.Minute).Milliseconds(),
				Laptime:   (2*time.Minute + 19*time.Second).Milliseconds(),
				PitLength: 90000,
			},
			stintAdjs: []entities.StintAdjustment{
				{
					Type:        entities.StintAdjustmentType_Laps,
					StintNumber: 1,
					Laps:        21,
				},
				{
					Type:        entities.StintAdjustmentType_Laps,
					StintNumber: 2,
					Laps:        21,
				},
			},
			progresses: []entities.RaceProgress{
				{
					RemainingTime: float64((time.Minute * 95).Milliseconds()),
					LapsComplete:  10,
					FuelTank:      4500,
					EnergyTank:    5500,
				},
				{
					LapsComplete: 21,
					FuelTank:     8300,
					EnergyTank:   10000,
				},
			},
		},
	}

	for _, tc := range tcs {
		stratRepo := mocks.NewMockStrategy(t)
		s := getStrategyService(stratRepo)

		ctx := context.Background()
		stratInfo, err := s.CalculatePitRequirement(ctx, tc.info)
		require.NoErrorf(t, err, "unexpected error calculating strategy for test '%s'", tc.name)

		strat := &entities.Strategy{
			ID:         uuid.New(),
			UserID:     uuid.New(),
			Passphrase: "foobar",
			Name:       tc.name,
			Data: entities.StrategyData{
				RaceInfo:     tc.info,
				Laps:         stratInfo.Laps,
				SafeLaps:     stratInfo.SafeLaps,
				MinRequired:  stratInfo.MinRequired,
				SafeRequired: stratInfo.SafeRequired,
				FuelRatio:    stratInfo.FuelRatio,
			},
			CreatedAt: time.Now().UTC(),
		}
		for _, pitStrat := range stratInfo.Strategies {
			if pitStrat.Type == stratInfo.RecommStrat {
				strat.Data.PitStrategy = pitStrat
			}
		}

		stratRepo.EXPECT().StoreRecalculatedStrategy(ctx, mock.Anything).Return(nil)

		adjustedStrat := *strat
		for _, stintAdj := range tc.stintAdjs {
			stratRepo.EXPECT().GetByID(ctx, adjustedStrat.ID).Return(strat, nil)

			replaced, err := s.EditStint(ctx, adjustedStrat.ID, adjustedStrat.UserID,
				adjustedStrat.Passphrase, stintAdj)
			require.NoErrorf(t, err, "unexpected error adjust stint pit length for test '%s'", tc.name)

			adjustedStrat.StintAdjustments = replaced.StintAdjustments
			adjustedStrat.Data.PitStrategy.Stints = replaced.Data.PitStrategy.Stints
		}

		stratRepo.EXPECT().GetByID(ctx, strat.ID).Return(strat, nil)

		var replaced *entities.Strategy
		for _, progress := range tc.progresses {
			replaced, err = s.ApplyRaceProgress(ctx, strat.ID, strat.UserID, strat.Passphrase, &progress)
			require.NoErrorf(t, err, "unexpected error recalculating strategy for test '%s'", tc.name)
		}

		output, err := json.MarshalIndent(replaced, "", "  ")
		require.NoError(t, err, "failed to marshal output for test '%s'", tc.name)

		testsupport.AssertGoldenFile(t, tc.name, output)
	}
}
