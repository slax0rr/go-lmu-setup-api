package services

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	domainErrors "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/errors"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/repositories"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/services"
	infraHttp "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/logger"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb/query"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/data"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/validators"
)

type SetupService interface {
	Create(context.Context, *entities.Setup) error
	Update(context.Context, *entities.Setup, *entities.User) error
	GetAll(context.Context, uuid.UUID, infraHttp.QueryFilter, *infraHttp.Pagination) (entities.SetupList, error)
	GetByID(context.Context, uuid.UUID, uuid.UUID) (*entities.Setup, error)
	GetUserSetups(context.Context, uuid.UUID, *infraHttp.Pagination) (entities.SetupList, error)
	GetSetupFile(context.Context, uuid.UUID) (*entities.SetupFile, error)
	RecordVote(context.Context, uuid.UUID, uuid.UUID, bool) (int64, error)
}

type Setup struct {
	dao            *data.AccessObject
	setupDomainSvc *services.Setup
	setupRepo      repositories.Setup
	setupStore     repositories.SetupStorage
}

func NewSetup(
	dao *data.AccessObject,
	setupDomainSvc *services.Setup,
	setupRepo repositories.Setup,
	setupStore repositories.SetupStorage,
) *Setup {
	return &Setup{
		dao:            dao,
		setupDomainSvc: setupDomainSvc,
		setupRepo:      setupRepo,
		setupStore:     setupStore,
	}
}

func (s *Setup) Create(ctx context.Context, setup *entities.Setup) error {
	err := validators.ValidateStruct(setup)
	if err != nil {
		return err
	}

	err = s.setupDomainSvc.DefaultTrackLayoutFallback(ctx, setup)
	if err != nil {
		return fmt.Errorf("fallback to default track layout: %w", err)
	}

	tx, closeTx, err := s.dao.GetDB().Begin(ctx)
	if err != nil {
		return fmt.Errorf("begin setup transaction: %w", err)
	}
	defer func() {
		closeTx(err)
	}()

	err = s.setupRepo.Create(ctx, setup, postgresdb.WithTransaction(tx))
	if err != nil {
		return fmt.Errorf("persist setup data: %w", err)
	}

	s.setupDomainSvc.StoreSetupFile(ctx, setup, postgresdb.WithTransaction(tx))
	if err != nil {
		return fmt.Errorf("create setup in storage: %w", err)
	}

	return nil
}

func (s *Setup) Update(ctx context.Context, setup *entities.Setup, user *entities.User) error {
	log := logger.FromContext(ctx)

	// uuid.Nil: no need to get user voting details when updating the setup
	dbSetup, err := s.GetByID(ctx, uuid.Nil, setup.ID)
	if err != nil {
		return err
	}

	if !s.setupDomainSvc.CanUserUpdate(dbSetup, user) {
		log.Warn().Stringer("db_user", dbSetup.UserID).Stringer("user", user.ID).
			Msg("an non-permitted set update was made")
		return domainErrors.ErrResourceWithoutPermission
	}

	s.setupDomainSvc.SetUser(setup, user)
	s.setupDomainSvc.SetFilenameForUpdate(setup, dbSetup)

	err = validators.ValidateStruct(setup)
	if err != nil {
		return err
	}

	err = s.setupDomainSvc.DefaultTrackLayoutFallback(ctx, setup)
	if err != nil {
		return fmt.Errorf("fallback to default track layout: %w", err)
	}

	tx, closeTx, err := s.dao.GetDB().Begin(ctx)
	if err != nil {
		return fmt.Errorf("begin setup transaction: %w", err)
	}
	defer func() {
		closeTx(err)
	}()

	affected, err := s.setupRepo.Update(ctx, setup, postgresdb.WithTransaction(tx))
	if err != nil {
		return fmt.Errorf("update setup: %w", err)
	}

	if affected == 0 {
		log.Debug().Fields(map[string]interface{}{
			"db_setup_id": dbSetup.ID,
			"db_user_id":  dbSetup.UserID,
			"setup_id":    setup.ID,
			"user_id":     user.ID,
		}).Msg("no rows got updated during update")
		return fmt.Errorf("no rows were updated")
	}

	if s.setupDomainSvc.ShouldUpdateFileContent(setup) {
		err = s.setupDomainSvc.StoreSetupFile(ctx, setup, postgresdb.WithTransaction(tx))
		if err != nil {
			return fmt.Errorf("update setup in storage: %w", err)
		}
	}

	return nil
}

func (r *Setup) GetAll(
	ctx context.Context,
	userID uuid.UUID,
	qf infraHttp.QueryFilter,
	pg *infraHttp.Pagination,
) (entities.SetupList, error) {
	if pg != nil {
		var err error
		pg.Total, err = r.setupRepo.Count(ctx, postgresdb.WithFilter(qf))
		if err != nil {
			return nil, fmt.Errorf("get setup item count: %w", err)
		}
	}

	list, err := r.setupRepo.GetList(ctx, userID, postgresdb.WithFilter(qf),
		postgresdb.WithLimit(pg.ToLimit()))
	if err != nil {
		return nil, fmt.Errorf("retrieve list of all setups from database: %w", err)
	}

	return list, nil
}

func (r *Setup) GetUserSetups(
	ctx context.Context,
	userID uuid.UUID,
	pg *infraHttp.Pagination,
) (entities.SetupList, error) {
	qf := infraHttp.QueryFilter{"setups.user_id": []string{userID.String()}}

	return r.GetAll(ctx, userID, qf, pg)
}

func (r *Setup) GetByID(ctx context.Context, userID, id uuid.UUID) (*entities.Setup, error) {
	qf := infraHttp.QueryFilter{
		"setups.id": []string{id.String()},
	}
	setups, err := r.setupRepo.GetList(ctx, userID, postgresdb.WithFilter(qf))
	if err != nil {
		return nil, fmt.Errorf("get setup by id from the repository: %w", err)
	}
	if len(setups) == 0 {
		return nil, domainErrors.ErrResourceNotFound
	}

	return &setups[0], nil
}

func (r *Setup) GetSetupFile(ctx context.Context, setupID uuid.UUID) (*entities.SetupFile, error) {
	log := logger.FromContext(ctx)

	orderBy := query.OrderBy{
		{Column: "updated_at", Direction: query.OrderByDescending},
	}
	files, err := r.setupStore.List(ctx, setupID, postgresdb.WithOrderBy(orderBy))
	if err != nil {
		log.Error().Err(err).Msg("failed to obtain a list of files")
		return nil, fmt.Errorf("obtain list of setup files from repository")
	}
	if len(files) == 0 {
		log.Debug().Msg("length of files in database is 0")
		return nil, domainErrors.ErrResourceNotFound
	}

	err = r.setupRepo.IncrementDownload(ctx, setupID)
	if err != nil {
		log.Error().Err(err).Msg("increment download failed")
	}

	return &files[0], nil
}

func (r *Setup) RecordVote(ctx context.Context, setupID, userID uuid.UUID, upvote bool) (int64, error) {
	log := logger.FromContext(ctx)

	err := r.setupRepo.RecordVote(ctx, setupID, userID, upvote)
	if err != nil {
		if errors.Is(err, postgresdb.ErrNotFound) {
			return 0, domainErrors.ErrResourceNotFound
		}

		return 0, fmt.Errorf("record vote in repository: %w", err)
	}

	setup, err := r.GetByID(ctx, userID, setupID)
	if err != nil {
		log.Error().Err(err).Msg("unable to retrieve setup with updated rating")
		return 0, nil
	}

	return setup.Rating, nil
}
