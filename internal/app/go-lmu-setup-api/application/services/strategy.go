package services

// TODO(slax0rr): complete refactor and split calculations out into a separate
// package and make the calculations more intuitive, right now it is a complete
// and utter mess. There be dragons.

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	domainErrors "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/errors"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/repositories"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/services"
	infraHttp "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/logger"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/validators"
)

type StrategyService interface {
	CalculatePitRequirement(context.Context, entities.RaceInfo) (*entities.StrategyInfo, error)
	Save(context.Context, *entities.Strategy) error
	Get(context.Context, uuid.UUID, uuid.UUID) (*entities.Strategy, error)
	GetUserStrategies(context.Context, uuid.UUID, *infraHttp.Pagination) (entities.StrategyList, error)
	ApplyRaceProgress(context.Context, uuid.UUID, uuid.UUID, string, *entities.RaceProgress) (*entities.Strategy, error)
	EditStint(context.Context, uuid.UUID, uuid.UUID, string, entities.StintAdjustment) (*entities.Strategy, error)
}

const (
	maxSavePercent float64 = 1.15
)

type stintInfo struct {
	count     int64
	remainder int64
	laps      float64
}

type baseRaceInfo struct {
	StintLaps float64
	Laps      float64
	SafeLaps  float64
}

type Strategy struct {
	stratRepo          repositories.Strategy
	raceStratDomSvc    *services.RaceStrategy
	raceProgressDomSvc *services.RaceProgress
	stintDomSvc        services.StintService
}

func NewStrategy(
	stratRepo repositories.Strategy,
	raceStratDomSvc *services.RaceStrategy,
	raceProgressDomSvc *services.RaceProgress,
	stintDomSvc services.StintService,
) *Strategy {
	return &Strategy{
		stratRepo:          stratRepo,
		raceStratDomSvc:    raceStratDomSvc,
		raceProgressDomSvc: raceProgressDomSvc,
		stintDomSvc:        stintDomSvc,
	}
}

func (s *Strategy) Save(ctx context.Context, strat *entities.Strategy) error {
	if strat == nil {
		return fmt.Errorf("strategy is not set")
	}

	err := validators.ValidateStruct(*strat)
	if err != nil {
		return err
	}

	b := make([]byte, 20)
	_, err = rand.Read(b)
	if err != nil {
		return fmt.Errorf("read random bytes: %w", err)
	}
	strat.Passphrase = base64.RawURLEncoding.EncodeToString(b)[:20]

	s.raceStratDomSvc.EnsurePitLength(&strat.Data.RaceInfo)

	err = s.stratRepo.Create(ctx, strat)
	if err != nil {
		return fmt.Errorf("create strategy in repository: %w", err)
	}

	return nil
}

func (s *Strategy) Get(ctx context.Context, id, userID uuid.UUID) (*entities.Strategy, error) {
	strat, err := s.stratRepo.GetByID(ctx, id)
	if err != nil {
		if errors.Is(err, postgresdb.ErrNotFound) {
			return nil, domainErrors.ErrResourceNotFound
		}
		return nil, fmt.Errorf("get strategy by id from the repository: %w", err)
	}

	if strat.UserID != userID {
		strat.Passphrase = ""
	}

	return strat, nil
}

func (s *Strategy) List(
	ctx context.Context,
	qf infraHttp.QueryFilter,
	pg *infraHttp.Pagination,
) (entities.StrategyList, error) {
	if pg != nil {
		var err error
		pg.Total, err = s.stratRepo.Count(ctx, postgresdb.WithFilter(qf))
		if err != nil {
			return nil, fmt.Errorf("get setup item count: %w", err)
		}
	}

	strats, err := s.stratRepo.List(ctx, postgresdb.WithFilter(qf), postgresdb.WithLimit(pg.ToLimit()))
	if err != nil {
		return nil, fmt.Errorf("read users saved strategies from repository: %w", err)
	}

	return strats, nil
}

func (s *Strategy) GetUserStrategies(
	ctx context.Context,
	userID uuid.UUID,
	pg *infraHttp.Pagination,
) (entities.StrategyList, error) {
	qf := infraHttp.QueryFilter{"s.user_id": []string{userID.String()}}

	return s.List(ctx, qf, pg)
}

func (s *Strategy) CalculatePitRequirement(
	ctx context.Context,
	info entities.RaceInfo,
) (*entities.StrategyInfo, error) {
	log := logger.FromContext(ctx)

	err := validators.ValidateStruct(info)
	if err != nil {
		return nil, err
	}

	s.raceStratDomSvc.EnsurePitLength(&info)
	info.CalculateRaceLaps()
	log.Debug().Any("race_info", info).Msg("calculated base information for the race")

	strat := entities.StrategyInfo{
		Laps:         info.Laps,
		SafeLaps:     info.SafeLaps,
		MinRequired:  entities.FuelReq{},
		SafeRequired: entities.FuelReq{},
		Strategies:   make([]entities.PitStrat, 0, 4),
	}

	if info.StintLaps == 0 {
		return nil, domainErrors.ErrStintLapsZero
	}

	s.raceStratDomSvc.GeneratePitStrategies(ctx, info, &strat)

	return &strat, nil
}

func (s *Strategy) ApplyRaceProgress(
	ctx context.Context,
	strategyID, userID uuid.UUID,
	passphrase string,
	progress *entities.RaceProgress,
) (*entities.Strategy, error) {
	strat, err := s.raceStratDomSvc.FetchStrategy(ctx, strategyID, userID, passphrase)
	if err != nil {
		return nil, fmt.Errorf("fetch strategy for race progress recalculation: %w", err)
	}

	s.raceProgressDomSvc.UpdateStrategyWithRaceProgress(ctx, strat, progress)

	if err := s.stratRepo.StoreRecalculatedStrategy(ctx, strat); err != nil {
		return nil, fmt.Errorf("store updated strategy: %w", err)
	}

	return strat, nil
}

func (s *Strategy) EditStint(
	ctx context.Context,
	strategyID, userID uuid.UUID,
	passphrase string,
	stintAdj entities.StintAdjustment,
) (*entities.Strategy, error) {
	if err := validators.ValidateStruct(stintAdj); err != nil {
		return nil, err
	}

	strat, err := s.raceStratDomSvc.FetchStrategy(ctx, strategyID, userID, passphrase)
	if err != nil {
		return nil, fmt.Errorf("fetch strategy for stint adjustment: %w", err)
	}

	err = s.stintDomSvc.ApplyStintAdjustment(ctx, strat, &stintAdj)
	if err != nil {
		return nil, fmt.Errorf("apply stint adjustment: %w", err)
	}

	if err := s.saveStintAdjustment(ctx, strat, stintAdj); err != nil {
		return nil, fmt.Errorf("save stint adjustment: %w", err)
	}

	return strat, nil
}

func (s *Strategy) saveStintAdjustment(
	ctx context.Context,
	strat *entities.Strategy,
	stintAdj entities.StintAdjustment,
) error {
	if strat.StintAdjustments == nil {
		strat.StintAdjustments = make([]entities.StintAdjustment, 0)
		strat.StintAdjustments = append(strat.StintAdjustments, stintAdj)
	} else {
		for i, adj := range strat.StintAdjustments {
			if adj.Type == stintAdj.Type && adj.StintNumber == stintAdj.StintNumber {
				strat.StintAdjustments[i] = stintAdj
				continue
			}

			strat.StintAdjustments = append(strat.StintAdjustments, stintAdj)
		}
	}

	if err := s.stratRepo.StoreRecalculatedStrategy(ctx, strat); err != nil {
		return fmt.Errorf("store updated strategy: %w", err)
	}

	return nil
}
