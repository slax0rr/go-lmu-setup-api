package services

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	domainErrors "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/errors"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/repositories"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/data"
)

type TrackService interface {
	Get(context.Context) (entities.TrackList, error)
	GetByID(context.Context, uuid.UUID) (*entities.Track, error)
}

type Tracks struct {
	dao  *data.AccessObject
	repo repositories.Track
}

func NewTracks(dao *data.AccessObject, repo repositories.Track) *Tracks {
	return &Tracks{
		dao:  dao,
		repo: repo,
	}
}

func (o *Tracks) Get(ctx context.Context) (entities.TrackList, error) {
	return o.repo.Get(ctx)
}

func (o *Tracks) GetByID(ctx context.Context, id uuid.UUID) (*entities.Track, error) {
	track, err := o.repo.GetByID(ctx, id)
	if err != nil {
		if errors.Is(err, postgresdb.ErrNotFound) {
			return nil, domainErrors.ErrResourceNotFound
		}

		return nil, fmt.Errorf("get track by id from the repository: %w", err)
	}

	return track, nil
}
