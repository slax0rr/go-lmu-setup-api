package services

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	domainErrors "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/errors"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/repositories"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/data"
)

type TrackLayoutService interface {
	Get(context.Context) (entities.TrackLayoutList, error)
	GetByID(context.Context, uuid.UUID) (*entities.TrackLayout, error)
}

type TrackLayouts struct {
	dao  *data.AccessObject
	repo repositories.TrackLayout
}

func NewTrackLayouts(dao *data.AccessObject, repo repositories.TrackLayout) *TrackLayouts {
	return &TrackLayouts{
		dao:  dao,
		repo: repo,
	}
}

func (o *TrackLayouts) Get(ctx context.Context) (entities.TrackLayoutList, error) {
	return o.repo.Get(ctx)
}

func (o *TrackLayouts) GetByID(ctx context.Context, id uuid.UUID) (*entities.TrackLayout, error) {
	track, err := o.repo.GetByID(ctx, id)
	if err != nil {
		if errors.Is(err, postgresdb.ErrNotFound) {
			return nil, domainErrors.ErrResourceNotFound
		}

		return nil, fmt.Errorf("get track by id from the repository: %w", err)
	}

	return track, nil
}
