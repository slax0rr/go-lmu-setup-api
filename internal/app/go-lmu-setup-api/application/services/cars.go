package services

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	domainErrors "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/errors"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/repositories"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/data"
)

type CarService interface {
	Get(context.Context) (entities.CarList, error)
	GetByID(context.Context, uuid.UUID) (*entities.Car, error)
}

type Cars struct {
	dao  *data.AccessObject
	repo repositories.Car
}

func NewCars(dao *data.AccessObject, repo repositories.Car) *Cars {
	return &Cars{
		dao:  dao,
		repo: repo,
	}
}

func (o *Cars) Get(ctx context.Context) (entities.CarList, error) {
	return o.repo.Get(ctx)
}

func (o *Cars) GetByID(ctx context.Context, id uuid.UUID) (*entities.Car, error) {
	car, err := o.repo.GetByID(ctx, id)
	if err != nil {
		if errors.Is(err, postgresdb.ErrNotFound) {
			return nil, domainErrors.ErrResourceNotFound
		}

		return nil, fmt.Errorf("get car by id from the repository: %w", err)
	}

	return car, nil
}
