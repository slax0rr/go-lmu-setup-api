package contextkey

type ContextKey struct {
	header string
	name   string
}

func (ck ContextKey) Name() string {
	return ck.name
}

var (
	User        = ContextKey{name: "authenticated_user"}
	QueryFilter = ContextKey{name: "query_filter"}
	Pagination  = ContextKey{name: "pagination"}
	StrategyID  = ContextKey{name: "strategy_id"}
)
