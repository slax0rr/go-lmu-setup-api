package repositories

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb/query"
)

const (
	tracksTableName    string = "tracks"
	tracksWriteColumns string = `"country","track_name","short_track_name","year","updated_at"`
	tracksReadColumns  string = `"id",` + tracksWriteColumns + `,"created_at"`
)

type Track struct {
	BaseRepo
}

func NewTrack(dbpool *pgxpool.Pool) *Track {
	t := Track{}
	t.dbpool = dbpool
	return &t
}

func (t *Track) GetByID(ctx context.Context, id uuid.UUID) (*entities.Track, error) {
	row, err := t.executor().QueryRow(ctx,
		fmt.Sprintf("SELECT %s FROM %s WHERE id = $1", tracksReadColumns, tracksTableName),
		id)
	if err != nil {
		return nil, fmt.Errorf("query track row: %w", err)
	}

	track := entities.Track{}
	err = row.Scan(
		&track.ID,
		&track.Country,
		&track.TrackName,
		&track.ShortTrackName,
		&track.Year,
		&track.UpdatedAt,
		&track.CreatedAt,
	)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, postgresdb.ErrNotFound
		}
		return nil, fmt.Errorf("scan track row: %w", err)
	}

	return &track, nil
}

func (t *Track) Get(ctx context.Context) (entities.TrackList, error) {
	rows, err := t.executor(
		postgresdb.WithOrderBy(query.OrderBy{
			{Column: "tracks.short_track_name"},
		}),
	).Query(ctx,
		fmt.Sprintf("SELECT %s FROM %s", tracksReadColumns, tracksTableName))
	if err != nil {
		return nil, fmt.Errorf("retrieve list of tracks: %w", err)
	}
	defer rows.Close()

	list := entities.TrackList{}
	for rows.Next() {
		track := entities.Track{}
		err := rows.Scan(
			&track.ID,
			&track.Country,
			&track.TrackName,
			&track.ShortTrackName,
			&track.Year,
			&track.UpdatedAt,
			&track.CreatedAt,
		)
		if err != nil {
			return nil, fmt.Errorf("scan track row from list: %w", err)
		}

		list = append(list, track)
	}

	if err := rows.Err(); err != nil {
		return list, fmt.Errorf("scan list of tracks: %w", err)
	}

	return list, nil
}
