package repositories

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
)

const (
	trackLayoutsTableName    string = "track_layouts"
	trackLayoutsWriteColumns string = `"track_id","name","default_layout","updated_at"`
	trackLayoutsReadColumns  string = `"id",` + trackLayoutsWriteColumns + `,"created_at"`
)

type TrackLayout struct {
	BaseRepo
}

func NewTrackLayout(dbpool *pgxpool.Pool) *TrackLayout {
	t := TrackLayout{}
	t.dbpool = dbpool
	return &t
}

func (t *TrackLayout) GetByID(ctx context.Context, id uuid.UUID) (*entities.TrackLayout, error) {
	row, err := t.executor().QueryRow(ctx,
		fmt.Sprintf("SELECT %s FROM %s WHERE id = $1", trackLayoutsReadColumns, trackLayoutsTableName),
		id)
	if err != nil {
		return nil, fmt.Errorf("query track layout row: %w", err)
	}

	layout := entities.TrackLayout{}
	err = row.Scan(
		&layout.ID,
		&layout.TrackID,
		&layout.Name,
		&layout.Default,
		&layout.UpdatedAt,
		&layout.CreatedAt,
	)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, postgresdb.ErrNotFound
		}
		return nil, fmt.Errorf("scan track layout row: %w", err)
	}

	return &layout, nil
}

func (t *TrackLayout) Get(ctx context.Context) (entities.TrackLayoutList, error) {
	rows, err := t.executor().Query(ctx,
		fmt.Sprintf("SELECT %s FROM %s", trackLayoutsReadColumns, trackLayoutsTableName))
	if err != nil {
		return nil, fmt.Errorf("retrieve list of track layouts: %w", err)
	}
	defer rows.Close()

	list := entities.TrackLayoutList{}
	for rows.Next() {
		layout := entities.TrackLayout{}
		err := rows.Scan(
			&layout.ID,
			&layout.TrackID,
			&layout.Name,
			&layout.Default,
			&layout.UpdatedAt,
			&layout.CreatedAt,
		)
		if err != nil {
			return nil, fmt.Errorf("scan track layout row from list: %w", err)
		}

		list = append(list, layout)
	}

	if err := rows.Err(); err != nil {
		return list, fmt.Errorf("scan list of track layouts: %w", err)
	}

	return list, nil
}

func (t *TrackLayout) GetTrackDefault(ctx context.Context, trackID uuid.UUID) (*uuid.UUID, error) {
	row, err := t.executor().QueryRow(
		ctx,
		fmt.Sprintf(`SELECT id FROM %s WHERE track_id = $1 AND default_layout = true`, trackLayoutsTableName),
		trackID,
	)
	if err != nil {
		return nil, fmt.Errorf("retrieve track default layout: %w", err)
	}

	var layoutID uuid.UUID
	err = row.Scan(&layoutID)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, postgresdb.ErrNotFound
		}
		return nil, fmt.Errorf("scan default track layout row: %w", err)

	}

	return &layoutID, nil
}
