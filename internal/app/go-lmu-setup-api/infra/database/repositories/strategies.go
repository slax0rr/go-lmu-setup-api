package repositories

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb/query"
)

type Strategy struct {
	BaseRepo

	filters map[string]interface{}
}

func NewStrategy(dbpool *pgxpool.Pool) *Strategy {
	s := &Strategy{}
	s.dbpool = dbpool
	return s
}

func (s *Strategy) Count(ctx context.Context, opts ...postgresdb.ExecOption) (int64, error) {
	row, err := s.executor(opts...).QueryRow(ctx, `SELECT COUNT(s.id) FROM strategies s`)
	if err != nil {
		return 0, fmt.Errorf("execute strategy count query: %w", err)
	}

	count := int64(0)
	err = row.Scan(&count)
	if err != nil {
		return 0, fmt.Errorf("scan strategy count: %w", err)
	}

	return count, nil
}

func (s *Strategy) Create(ctx context.Context, strategy *entities.Strategy, opts ...postgresdb.ExecOption) error {
	data, err := json.Marshal(strategy.Data)
	if err != nil {
		return fmt.Errorf("encode strategy data: %w", err)
	}

	row, err := s.executor(opts...).QueryRow(ctx, `
INSERT INTO strategies (user_id, passphrase, name, data, created_at)
VALUES ($1, $2, $3, $4, $5)
RETURNING id`,
		strategy.UserID,
		strategy.Passphrase,
		strategy.Name,
		data,
		time.Now().UTC(),
	)
	if err != nil {
		return fmt.Errorf("insert strategy: %w", err)
	}

	err = row.Scan(&strategy.ID)
	if err != nil {
		return fmt.Errorf("scan returned strategy id: %w", err)
	}

	return nil
}

func (s *Strategy) GetByID(ctx context.Context, id uuid.UUID) (*entities.Strategy, error) {
	row, err := s.executor().QueryRow(ctx, `
SELECT
    s.id, s.user_id, s.passphrase, s.name, s.data, rs.data AS recalculated_data,
    rs.progress, rs.stint_adjustments, s.created_at, s.updated_at
FROM
    strategies s
LEFT JOIN
    recalculated_strategies rs ON s.id = rs.strategy_id
WHERE
    s.id = $1
`, id)
	if err != nil {
		return nil, fmt.Errorf("query strategy row: %w", err)
	}

	strat := entities.Strategy{}
	var data json.RawMessage
	var recalculatedData, progress, stintAdjustments *json.RawMessage

	err = row.Scan(
		&strat.ID,
		&strat.UserID,
		&strat.Passphrase,
		&strat.Name,
		&data,
		&recalculatedData,
		&progress,
		&stintAdjustments,
		&strat.CreatedAt,
		&strat.UpdatedAt,
	)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, postgresdb.ErrNotFound
		}
		return nil, fmt.Errorf("scan strategy row: %w", err)
	}

	err = json.Unmarshal(data, &strat.Data)
	if err != nil {
		return nil, fmt.Errorf("decode strategy data: %w", err)
	}

	if recalculatedData != nil {
		strat.Data = entities.StrategyData{}
		err = json.Unmarshal(*recalculatedData, &strat.Data)
		if err != nil {
			return nil, fmt.Errorf("decode recalcualted strategy: %w", err)
		}
	}

	if progress != nil {
		err = json.Unmarshal(*progress, &strat.Progress)
		if err != nil {
			return nil, fmt.Errorf("decode race_progress: %w", err)
		}
	}

	if stintAdjustments != nil {
		strat.StintAdjustments = make([]entities.StintAdjustment, 0)
		err = json.Unmarshal(*stintAdjustments, &strat.StintAdjustments)
		if err != nil {
			return nil, fmt.Errorf("decode stint_adjustments: %w", err)
		}
	}

	return &strat, nil
}

func (s *Strategy) List(
	ctx context.Context,
	opts ...postgresdb.ExecOption,
) (entities.StrategyList, error) {
	ob := query.OrderBy{{Column: "s.created_at", Direction: query.OrderByDescending}}
	opts = append(opts, postgresdb.WithOrderBy(ob))

	rows, err := s.executor(opts...).Query(ctx, `
SELECT
    s.id, s.user_id, s.passphrase, s.name, rs.progress, s.created_at, s.updated_at
FROM
    strategies s
LEFT JOIN
    recalculated_strategies rs ON s.id = rs.strategy_id`)
	if err != nil {
		return nil, fmt.Errorf("execute select for user saved strategies: %w", err)
	}
	defer rows.Close()

	strats := make([]entities.Strategy, 0)
	for rows.Next() {
		strat := entities.Strategy{}
		var progress *json.RawMessage

		err := rows.Scan(
			&strat.ID,
			&strat.UserID,
			&strat.Passphrase,
			&strat.Name,
			&progress,
			&strat.CreatedAt,
			&strat.UpdatedAt,
		)
		if err != nil {
			return nil, fmt.Errorf("scan row of user saved strategy: %w", err)
		}

		if progress != nil {
			err = json.Unmarshal(*progress, &strat.Progress)
			if err != nil {
				return nil, fmt.Errorf("decode race_progress: %w", err)
			}
		}
		strats = append(strats, strat)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("scan for user saved strategies: %w", err)
	}

	return strats, nil
}

func (s *Strategy) StoreRecalculatedStrategy(
	ctx context.Context,
	strat *entities.Strategy,
	opts ...postgresdb.ExecOption,
) error {
	data, err := json.Marshal(strat.Data)
	if err != nil {
		return fmt.Errorf("encode strategy data: %w", err)
	}

	var raceProgress json.RawMessage
	raceProgress, err = json.Marshal(strat.Progress)
	if err != nil {
		return fmt.Errorf("encode race progress: %w", err)
	}

	var stintAdjustments json.RawMessage
	stintAdjustments, err = json.Marshal(strat.StintAdjustments)
	if err != nil {
		return fmt.Errorf("encode stint adjustments: %w", err)
	}

	_, err = s.executor(opts...).Exec(ctx, `
INSERT INTO recalculated_strategies (strategy_id, data, progress, stint_adjustments, created_at)
VALUES ($1, $2, $3, $4, $5)
ON CONFLICT (strategy_id) DO UPDATE SET
    data = EXCLUDED.data,
    progress = EXCLUDED.progress,
	stint_adjustments = EXCLUDED.stint_adjustments,
    updated_at = EXCLUDED.created_at`,
		strat.ID,
		data,
		raceProgress,
		stintAdjustments,
		time.Now().UTC(),
	)
	if err != nil {
		return fmt.Errorf("exec update recalculated strategy query: %w", err)
	}

	return nil
}
