package repositories

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb/query"
)

const (
	carsTableName    string = "cars"
	carsWriteColumns string = `"class","race_class","manufacturer","model","spec","year","updated_at"`
	carsReadColumns  string = `"id",` + carsWriteColumns + `,"created_at"`
)

type Car struct {
	BaseRepo
}

func NewCar(dbpool *pgxpool.Pool) *Car {
	c := Car{}
	c.dbpool = dbpool
	return &c
}

func (c *Car) GetByID(ctx context.Context, id uuid.UUID) (*entities.Car, error) {
	row, err := c.executor().QueryRow(ctx,
		fmt.Sprintf("SELECT %s FROM %s WHERE id = $1", carsReadColumns, carsTableName),
		id)
	if err != nil {
		return nil, fmt.Errorf("query car row: %w", err)
	}

	car := entities.Car{}
	err = row.Scan(
		&car.ID,
		&car.Class,
		&car.RaceClass,
		&car.Manufacturer,
		&car.Model,
		&car.Spec,
		&car.Year,
		&car.UpdatedAt,
		&car.CreatedAt,
	)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, postgresdb.ErrNotFound
		}
		return nil, fmt.Errorf("scan car row: %w", err)
	}

	return &car, nil
}

func (c *Car) Get(ctx context.Context) (entities.CarList, error) {
	rows, err := c.executor(
		postgresdb.WithOrderBy(query.OrderBy{
			{Column: "cars.race_class"},
			{Column: "cars.manufacturer"},
		}),
	).Query(ctx,
		fmt.Sprintf("SELECT %s FROM %s", carsReadColumns, carsTableName))
	if err != nil {
		return nil, fmt.Errorf("retrieve list of cars: %w", err)
	}
	defer rows.Close()

	list := entities.CarList{}
	for rows.Next() {
		car := entities.Car{}
		err := rows.Scan(
			&car.ID,
			&car.Class,
			&car.RaceClass,
			&car.Manufacturer,
			&car.Model,
			&car.Spec,
			&car.Year,
			&car.UpdatedAt,
			&car.CreatedAt,
		)
		if err != nil {
			return nil, fmt.Errorf("scan car row from list: %w", err)
		}

		list = append(list, car)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("scan list of cars: %w", err)
	}

	return list, nil
}
