package repositories

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
)

type SetupFile struct {
	BaseRepo
}

func NewSetupFile(dbpool *pgxpool.Pool) *SetupFile {
	sf := &SetupFile{}
	sf.dbpool = dbpool
	return sf
}

func (sf *SetupFile) Store(ctx context.Context, file entities.SetupFile, opts ...postgresdb.ExecOption) error {
	row, err := sf.executor(opts...).QueryRow(ctx, `
INSERT INTO setup_files (setup_id,file_name,content,updated_at)
VALUES ($1, $2, $3, $4)
RETURNING id`,
		file.SetupID,
		file.Name,
		file.Content,
		time.Now().UTC(),
	)
	if err != nil {
		return fmt.Errorf("insert setup file: %w", err)
	}

	err = row.Scan(&file.ID)
	if err != nil {
		return fmt.Errorf("scan returned setup file id: %w", err)
	}

	return nil
}

func (sf *SetupFile) List(
	ctx context.Context,
	setupID uuid.UUID,
	opts ...postgresdb.ExecOption,
) ([]entities.SetupFile, error) {
	rows, err := sf.executor(opts...).Query(ctx, `
SELECT id, setup_id, file_name, content, created_at, updated_at FROM setup_files WHERE setup_id = $1
`, setupID)
	if err != nil {
		return nil, fmt.Errorf("list setup files: %w", err)
	}
	defer rows.Close()

	files := make([]entities.SetupFile, 0)
	for rows.Next() {
		file := entities.SetupFile{}
		err := rows.Scan(&file.ID, &file.SetupID, &file.Name, &file.Content, &file.CreatedAt, &file.UpdatedAt)
		if err != nil {
			return nil, fmt.Errorf("scan setup file row from list: %w", err)
		}

		files = append(files, file)
	}

	if err := rows.Err(); err != nil {
		return files, fmt.Errorf("scan list of setup files: %w", err)
	}

	return files, nil
}
