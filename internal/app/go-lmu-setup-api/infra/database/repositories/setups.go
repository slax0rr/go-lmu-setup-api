package repositories

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	domainErrors "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/errors"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb/query"
)

const (
	ForeignKeyConstraint_SetupLayoutID = "setups_track_layout_id_fkey"
)

type Setup struct {
	BaseRepo

	filters map[string]interface{}
}

func NewSetup(dbpool *pgxpool.Pool) *Setup {
	s := &Setup{}
	s.dbpool = dbpool
	return s
}

func (s *Setup) Count(ctx context.Context, opts ...postgresdb.ExecOption) (int64, error) {
	row, err := s.executor(opts...).QueryRow(ctx, `SELECT COUNT(setups.id) FROM setups`)
	if err != nil {
		return 0, fmt.Errorf("execute setup count query: %w", err)
	}

	count := int64(0)
	err = row.Scan(&count)
	if err != nil {
		return 0, fmt.Errorf("scan setup count: %w", err)
	}

	return count, nil
}

func (s *Setup) Create(ctx context.Context, setup *entities.Setup, opts ...postgresdb.ExecOption) error {
	row, err := s.executor(opts...).QueryRow(ctx, `
INSERT INTO setups (car_id,track_id,track_layout_id,file_name,laptime,type,condition,
  description,downloads,video_link,user_id,"user",version,updated_at)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)
RETURNING id`,
		setup.CarID,
		setup.TrackID,
		setup.TrackLayoutID,
		setup.FileName,
		setup.Laptime,
		setup.Type,
		setup.Condition,
		setup.Description,
		setup.Downloads,
		setup.VideoLink,
		setup.UserID,
		setup.User,
		setup.Version,
		time.Now().UTC(),
	)
	if err != nil {
		return fmt.Errorf("insert setup: %w", err)
	}

	err = row.Scan(&setup.ID)
	if err != nil {
		var pgErr *pgconn.PgError
		if ok := errors.As(err, &pgErr); ok {
			if pgErr.Code == "23503" && pgErr.ConstraintName == ForeignKeyConstraint_SetupLayoutID {
				return domainErrors.ErrTrackLayoutInvalid
			}
		}
		return fmt.Errorf("scan returned setup id: %w", err)
	}

	return nil
}

func (s *Setup) Update(
	ctx context.Context,
	setup *entities.Setup,
	opts ...postgresdb.ExecOption,
) (int64, error) {
	cmdTag, err := s.executor(opts...).Exec(ctx, `
UPDATE setups SET
  car_id = $1, track_id = $2, track_layout_id = $3, file_name = $4, laptime = $5, type = $6, condition = $7,
  description = $8, video_link = $9, "user" = $10, version = $11, updated_at = $12
WHERE
  id = $13 AND user_id = $14`,
		setup.CarID,
		setup.TrackID,
		setup.TrackLayoutID,
		setup.FileName,
		setup.Laptime,
		setup.Type,
		setup.Condition,
		setup.Description,
		setup.VideoLink,
		setup.User,
		setup.Version,
		time.Now().UTC(),
		setup.ID,
		setup.UserID,
	)
	if err != nil {
		return 0, fmt.Errorf("execute update statement: %w", err)
	}

	return cmdTag.RowsAffected(), nil
}

func (s *Setup) GetList(
	ctx context.Context,
	userID uuid.UUID,
	opts ...postgresdb.ExecOption,
) (entities.SetupList, error) {
	sql := `
SELECT
  setups.id, setups.car_id, setups.track_id, setups.track_layout_id, setups.file_name, setups.laptime, setups.type,
  setups.condition,
  (SELECT COUNT(sr.setup_id) FROM setup_rating sr WHERE sr.setup_id = setups.id AND upvote = true) AS upvotes,
  (SELECT COUNT(sr.setup_id) FROM setup_rating sr WHERE sr.setup_id = setups.id AND upvote = false) AS downvotes,
  setups.description, setups.downloads, setups.video_link, setups.user_id, setups."user", setups.version,
  setups.updated_at, setups.created_at, setup_rating.upvote as user_vote
FROM setups
LEFT OUTER JOIN setup_rating ON setups.id = setup_rating.setup_id AND setup_rating.user_id = $1`
	list := entities.SetupList{}

	ob := query.OrderBy{{Column: "setups.created_at"}}
	opts = append(opts, postgresdb.WithOrderBy(ob))

	rows, err := s.executor(opts...).Query(ctx, sql, userID)
	if err != nil {
		return nil, fmt.Errorf("retrieve list of setups: %w", err)
	}
	defer rows.Close()

	for rows.Next() {
		setup := entities.Setup{}
		err := s.scanRow(rows, &setup)
		if err != nil {
			return nil, fmt.Errorf("scan setup row from list: %w", err)
		}

		list = append(list, setup)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("scan list of setups: %w", err)
	}

	return list, nil
}

func (s *Setup) IncrementDownload(ctx context.Context, id uuid.UUID, opts ...postgresdb.ExecOption) error {
	_, err := s.executor(opts...).Exec(ctx, `UPDATE setups SET downloads = downloads + 1 WHERE id = $1`, id)
	if err != nil {
		return fmt.Errorf("update download counter: %w", err)
	}

	return nil
}

func (s *Setup) RecordVote(
	ctx context.Context,
	setupID, userID uuid.UUID,
	upvote bool,
	opts ...postgresdb.ExecOption,
) error {
	_, err := s.executor(opts...).Exec(ctx, `
INSERT INTO
  setup_rating (setup_id, user_id, upvote)
VALUES ($1, $2, $3)
ON CONFLICT (setup_id, user_id) DO UPDATE SET
  upvote = EXCLUDED.upvote,
  updated_at = now() at time zone 'utc'
WHERE
  setup_rating.setup_id = EXCLUDED.setup_id
  AND setup_rating.user_id = EXCLUDED.user_id
`, setupID, userID, upvote)
	if err != nil {
		return fmt.Errorf("record rating vote: %w", err)
	}
	return nil
}

func (s *Setup) scanRow(rows pgx.Row, setup *entities.Setup) error {
	var upvotes, downvotes float64

	err := rows.Scan(
		&setup.ID,
		&setup.CarID,
		&setup.TrackID,
		&setup.TrackLayoutID,
		&setup.FileName,
		&setup.Laptime,
		&setup.Type,
		&setup.Condition,
		&upvotes,
		&downvotes,
		&setup.Description,
		&setup.Downloads,
		&setup.VideoLink,
		&setup.UserID,
		&setup.User,
		&setup.Version,
		&setup.UpdatedAt,
		&setup.CreatedAt,
		&setup.UserVote,
	)
	if err != nil {
		return err
	}

	if upvotes+downvotes > 0 {
		setup.Rating = int64(upvotes / (upvotes + downvotes) * 1000)
	}

	return nil
}
