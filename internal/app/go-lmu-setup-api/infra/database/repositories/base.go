package repositories

import (
	"github.com/jackc/pgx/v5/pgxpool"

	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
)

type BaseRepo struct {
	dbpool *pgxpool.Pool
}

func (b *BaseRepo) executor(opts ...postgresdb.ExecOption) *postgresdb.Executor {
	return postgresdb.NewExecutor(b.dbpool, opts...)
}
