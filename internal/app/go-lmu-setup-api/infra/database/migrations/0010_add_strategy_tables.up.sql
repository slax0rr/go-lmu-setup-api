CREATE TABLE IF NOT EXISTS strategies (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    user_id UUID NOT NULL,
    passphrase VARCHAR,
    name VARCHAR NOT NULL,
    data JSONB NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT (now() at time zone 'utc'),
    updated_at TIMESTAMP,
    CONSTRAINT pkey_strategy PRIMARY KEY ( id )
);

CREATE TABLE IF NOT EXISTS recalculated_strategies (
    strategy_id UUID NOT NULL REFERENCES strategies ( id ),
    data JSONB NOT NULL,
    progress JSONB NULL,
    created_at TIMESTAMP NOT NULL DEFAULT (now() at time zone 'utc'),
    updated_at TIMESTAMP,
    CONSTRAINT pkey_recalculated_strategy PRIMARY KEY ( strategy_id )
);
