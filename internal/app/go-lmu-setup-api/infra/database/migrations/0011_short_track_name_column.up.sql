ALTER TABLE "tracks" ADD COLUMN short_track_name VARCHAR(255) DEFAULT '';

UPDATE "tracks" SET short_track_name = 'Spa' WHERE id = '059a488a-42c3-4629-bc78-c63f607235f4';
UPDATE "tracks" SET short_track_name = 'Le Mans' WHERE id = '6379e3a5-b774-4a67-b5b7-99aab5506980';
UPDATE "tracks" SET short_track_name = 'Sebring' WHERE id = '886869b5-3483-4054-9c63-b61731651b14';
UPDATE "tracks" SET short_track_name = 'Monza' WHERE id = 'e1716cfc-bbf9-40e9-a65c-74de3198da7c';
UPDATE "tracks" SET short_track_name = 'Fuji' WHERE id = '2e9f47f9-ff0b-4a29-bf4b-b79abc3a8e0e';
UPDATE "tracks" SET short_track_name = 'Portimão' WHERE id = '82893aa7-4e23-4f3d-a35f-8ff5678ca10d';
UPDATE "tracks" SET short_track_name = 'Bahrain' WHERE id = '12ccc0a5-257a-4de9-8f70-e70c31515ba6';
UPDATE "tracks" SET short_track_name = 'Imola' WHERE id = 'cc1af759-8e07-48ec-8054-a655a8d344f6';
UPDATE "tracks" SET short_track_name = 'Interlagos' WHERE id = 'cf9be9e8-3c74-4a67-b8a3-f42f4355e97c';
