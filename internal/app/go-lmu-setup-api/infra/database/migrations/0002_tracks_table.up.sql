CREATE TABLE IF NOT EXISTS "tracks" (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    country VARCHAR(100) NOT NULL,
    track_name VARCHAR(255) NOT NULL,
    year INT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT (now() at time zone 'utc'),
    updated_at TIMESTAMP NULL,
    CONSTRAINT pkey_tracks PRIMARY KEY ( id )
);
