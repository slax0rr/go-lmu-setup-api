INSERT INTO "cars" (id, class, race_class, manufacturer, model, spec, year) VALUES
    ('752e94d2-e20e-4c01-a9ff-51543179e1b8', 'GT3', 'LMGT3', 'McLaren', '720s GT3', 'Evo', 2024),
    ('051cd261-57bd-481a-9c91-4baeadd6d064', 'GT3', 'LMGT3', 'Ferrari', '296 GT3', '', 2024),
    ('984a5d8f-c691-46c0-afed-99d80df039a2', 'GT3', 'LMGT3', 'BMW', 'M4 GT3', '', 2024),
    ('4bab6fe3-448e-48e8-b11f-f20b5c3c962c', 'GT3', 'LMGT3', 'Chevrolet', 'Corvette Z06 GT3.R', '', 2024);

INSERT INTO "tracks" (id, country, track_name, year) VALUES
    ('cf9be9e8-3c74-4a67-b8a3-f42f4355e97c', 'br', 'Autodromo Jose Carlos Pace (Interlagos)', 2024);
