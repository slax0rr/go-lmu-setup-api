CREATE TABLE IF NOT EXISTS "track_layouts" (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    track_id UUID NOT NULL REFERENCES tracks ( id ),
    name VARCHAR(255) NOT NULL,
    default_layout BOOLEAN NOT NULL DEFAULT false,
    created_at TIMESTAMP NOT NULL DEFAULT (now() at time zone 'utc'),
    updated_at TIMESTAMP NULL,
    CONSTRAINT pkey_track_layouts PRIMARY KEY ( id )
);

CREATE UNIQUE INDEX unique_default_track_layout
ON track_layouts ( track_id )
WHERE default_layout = TRUE;

ALTER TABLE setups ADD COLUMN track_layout_id UUID NULL REFERENCES track_layouts ( id );
