CREATE TYPE "race_class" AS ENUM ('HYPERCAR', 'LMP2', 'LMGTE', 'LMGT3');
CREATE TYPE "car_class" AS ENUM ('LMH', 'LMDh', 'P2', 'GTE', 'GT3');

CREATE TABLE IF NOT EXISTS "cars" (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    class car_class NOT NULL,
    race_class race_class NOT NULL,
    manufacturer VARCHAR(128) NOT NULL,
    model VARCHAR(128) NOT NULL,
    spec VARCHAR(255),
    year INT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT (now() at time zone 'utc'),
    updated_at TIMESTAMP NULL,
    CONSTRAINT pkey_cars PRIMARY KEY ( id )
);
