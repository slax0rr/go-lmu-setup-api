CREATE TYPE "setup_type" AS ENUM ('sprint', 'endurance', 'hotlap');
CREATE TYPE "setup_condition" AS ENUM ('dry', 'damp', 'wet', 'flooded');

CREATE TABLE "setups" (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    car_id UUID NOT NULL REFERENCES cars( id ),
    track_id UUID NOT NULL REFERENCES tracks( id ),
    file_name VARCHAR(255) NOT NULL,
    laptime VARCHAR(50) NOT NULL,
    "type" setup_type NOT NULL,
    condition setup_condition NOT NULL,
    rating DECIMAL NOT NULL,
    description TEXT,
    downloads NUMERIC NOT NULL DEFAULT 0,
    video_link VARCHAR(255),
    version VARCHAR(50) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT (now() at time zone 'utc'),
    updated_at TIMESTAMP,
    CONSTRAINT pkey_setups PRIMARY KEY ( id )
);
