CREATE TABLE IF NOT EXISTS "setup_files" (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    setup_id UUID NOT NULL REFERENCES setups( id ),
    file_name VARCHAR(255) NOT NULL,
    content TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT (now() at time zone 'utc'),
    updated_at TIMESTAMP,
    CONSTRAINT pkey_setup_files PRIMARY KEY ( id )
);

CREATE INDEX idx_setup_files_setup_id ON setup_files (setup_id);
