CREATE TABLE IF NOT EXISTS "setup_rating" (
    setup_id UUID NOT NULL REFERENCES setups( id ),
    user_id UUID NOT NULL,
    upvote BOOLEAN NOT NULL DEFAULT true,
    created_at TIMESTAMP NOT NULL DEFAULT (now() at time zone 'utc'),
    updated_at TIMESTAMP,

    CONSTRAINT unique_vote UNIQUE (setup_id, user_id)
);

ALTER TABLE "setups" DROP COLUMN rating;
