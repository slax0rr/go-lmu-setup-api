INSERT INTO track_layouts (id, track_id, name, default_layout) VALUES
-- Spa
('d683f780-fbe8-4bac-b359-d103f0fd2281', '059a488a-42c3-4629-bc78-c63f607235f4', 'WEC', true),
('badfade6-e36f-4d1a-b6ad-d55a4d06efad', '059a488a-42c3-4629-bc78-c63f607235f4', 'Endurance', false),
-- Le Mans
('4363f45c-d2a1-4cf9-8092-cb5f90e07ded', '6379e3a5-b774-4a67-b5b7-99aab5506980', 'WEC', true),
('8a85149d-5ec2-40c5-a467-8b6aac9a4a3f', '6379e3a5-b774-4a67-b5b7-99aab5506980', 'Mulsane', false),
-- Sebring
('292126a2-1b22-4f6c-8f08-c9491ce19050', '886869b5-3483-4054-9c63-b61731651b14', 'WEC', true),
('67306b8b-00f4-4079-b590-d0eda90110c4', '886869b5-3483-4054-9c63-b61731651b14', 'School', false),
-- Monza
('9bb19eec-6564-4688-8ee8-2efb2554ce81', 'e1716cfc-bbf9-40e9-a65c-74de3198da7c', 'WEC', true),
('1d1c9861-dc8b-48d3-90c8-a85e5adcd83a', 'e1716cfc-bbf9-40e9-a65c-74de3198da7c', 'Curva Grande', false),
-- Fuji
('4e31ca2d-1877-4b61-956d-46a087698a7b', '2e9f47f9-ff0b-4a29-bf4b-b79abc3a8e0e', 'WEC', true),
('577da313-8f10-4f03-8fa0-875425152625', '2e9f47f9-ff0b-4a29-bf4b-b79abc3a8e0e', 'Classic (No chicane)', false),
-- Bahrain
('9a3f6f59-7bc9-4b26-a6f3-3c180be785db', '12ccc0a5-257a-4de9-8f70-e70c31515ba6', 'WEC', true),
('2add08bd-9e36-4f45-829f-01629c37759d', '12ccc0a5-257a-4de9-8f70-e70c31515ba6', 'Endurance', false),
('1105610e-5cf8-4ea1-8ef4-386f8158dea7', '12ccc0a5-257a-4de9-8f70-e70c31515ba6', 'Outer', false),
('76fb9313-e23b-4a3a-a75d-ba1b851b52e2', '12ccc0a5-257a-4de9-8f70-e70c31515ba6', 'Paddock', false),
-- COTA
('24c3746c-a162-47e1-ad19-d568b9b2893b', '8f921a3c-37d7-4cd8-9e2b-10455c5ad90d', 'WEC', true),
('7434611d-6180-4890-9468-5366464af42d', '8f921a3c-37d7-4cd8-9e2b-10455c5ad90d', 'National', false);

UPDATE setups
SET track_layout_id = (SELECT id FROM track_layouts WHERE track_layouts.track_id = setups.track_id AND track_layouts.default_layout = true);
