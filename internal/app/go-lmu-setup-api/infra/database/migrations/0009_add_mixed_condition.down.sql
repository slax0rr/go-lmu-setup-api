CREATE TYPE setup_condition_old AS ENUM ('dry', 'damp', 'wet', 'flooded');

ALTER TABLE setups
    ALTER COLUMN condition TYPE setup_condition_old
    USING condition::text::setup_condition_old;

DROP TYPE setup_condition;

CREATE TYPE setup_condition AS ENUM ('dry', 'damp', 'wet', 'flooded');

ALTER TABLE setups
    ALTER COLUMN condition TYPE setup_condition
    USING condition::text::setup_condition;

DROP TYPE setup_condition_old;
