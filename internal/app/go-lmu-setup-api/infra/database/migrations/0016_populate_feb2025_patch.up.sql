INSERT INTO "cars" (id, class, race_class, manufacturer, model, spec, year) VALUES
    ('ce8dd202-a0a7-4a77-b465-f1781751adeb', 'GT3', 'LMGT3', 'Ford', 'Mustang', '', 2024),
    ('102bc7e7-6717-41ff-abb4-7e9ea5b9c102', 'GT3', 'LMGT3', 'Porsche', '992 GT3R-24', '', 2024),
    ('d87b645a-3b8c-4720-ae2c-9d62f098c699', 'GT3', 'LMGT3', 'Aston Martin', 'Vantage AMR GT3 Evo', 'Evo', 2024);
