package controllers

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/application/services"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/constants"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	domainErrors "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/errors"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/contextkey"
	infraHttp "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/logger"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/response"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/validators"
)

const MAX_UPLOAD_SIZE = 1 << 18 // 256KB upload size limit

func ListSetups(setupSvc services.SetupService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		userID := uuid.Nil
		user, ok := ctx.Value(contextkey.User).(*entities.User)
		if ok {
			log.Debug().Msg("user data set, using for list")
			userID = user.ID
		}

		qf, _ := ctx.Value(contextkey.QueryFilter).(infraHttp.QueryFilter)
		pg, ok := ctx.Value(contextkey.Pagination).(*infraHttp.Pagination)
		if !ok {
			pg = &infraHttp.Pagination{}
		}

		setups, err := setupSvc.GetAll(ctx, userID, qf, pg)
		if err != nil {
			log.Error().Err(err).Msg("get all setups")
			c.Error(response.ErrInternalServerError)
			return
		}

		c.JSON(http.StatusOK, setups)
	}
}

func GetUserSetups(setupSvc services.SetupService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		user, ok := ctx.Value(contextkey.User).(*entities.User)
		if !ok {
			log.Error().Msg("user data in context invalid")
			c.Error(response.ErrInternalServerError)
			return
		}

		pg, ok := ctx.Value(contextkey.Pagination).(*infraHttp.Pagination)
		if !ok {
			pg = &infraHttp.Pagination{}
		}

		setups, err := setupSvc.GetUserSetups(ctx, user.ID, pg)
		if err != nil {
			log.Error().Err(err).Msg("get saved user strategies")
			c.Error(response.ErrInternalServerError)
			return
		}

		c.JSON(http.StatusOK, setups)
	}
}

func GetSetup(setupSvc services.SetupService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		id, err := uuid.Parse(c.Param("setup_id"))
		if err != nil {
			log.Debug().Err(err).Msg("invalid setup_id parameter")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_MissingOrMalformedResourceID))
			return
		}

		userID := uuid.Nil
		user, ok := ctx.Value(contextkey.User).(*entities.User)
		if ok {
			log.Debug().Msg("user data set, using for setup retrieval")
			userID = user.ID
		}

		setup, err := setupSvc.GetByID(c, userID, id)
		if err != nil {
			if errors.Is(err, domainErrors.ErrResourceNotFound) {
				c.Error(response.ErrNotFound)
				return
			}
			log.Error().Err(err).Msg("get setups by id")
			c.Error(response.ErrInternalServerError)
			return
		}

		c.JSON(http.StatusOK, setup)
	}
}

func GetSetupFile(setupSvc services.SetupService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		setupIDstr := c.Param("setup_id")
		setupID, err := uuid.Parse(setupIDstr)
		if err != nil {
			log.Debug().Err(err).Msg("invalid setup_id parameter")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_MissingOrMalformedResourceID))
			return
		}

		setupFile, err := setupSvc.GetSetupFile(ctx, setupID)
		if err != nil {
			if errors.Is(err, domainErrors.ErrResourceNotFound) {
				c.Error(response.ErrNotFound.
					WithDetails(constants.ErrorDetails_SetupFileMissing))
				return
			}

			log.Error().Err(err).Msg("get setup by setup_id")
			c.Error(response.ErrInternalServerError)
			return
		}

		if setupFile.Name != "" && len(setupFile.Content) > 0 {
			infraHttp.ServeFile(c, http.StatusOK, setupFile.Name, setupFile.Content)
			return
		}
		// TODO(slax0rr): remove the fallback to legacy setup file on the
		// filesystem
		c.FileAttachment(setupFile.Path, setupFile.Name)
	}
}

func AddSetup(setupSvc services.SetupService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		user, ok := ctx.Value(contextkey.User).(*entities.User)
		if !ok {
			log.Error().Msg("cast user from context")
			c.Error(response.ErrInternalServerError)
			return
		}

		c.Request.Body = http.MaxBytesReader(c.Writer, c.Request.Body, MAX_UPLOAD_SIZE)

		var setup entities.Setup
		if err := c.ShouldBindJSON(&setup); err != nil {
			log.Info().Err(err).Msg("bind setup JSON")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_InvalidRequestPayload))
			return
		}

		setup.UserID = user.ID
		setup.User = user.DisplayName
		if setup.User == "" {
			setup.User = user.Username
		}

		err := setupSvc.Create(ctx, &setup)
		if err != nil {
			if vErr, ok := err.(validators.ValidationErrors); ok {
				c.Error(response.ErrBadRequest.WithDetails(vErr))
				return
			}

			if errors.Is(err, domainErrors.ErrTrackLayoutInvalid) {
				c.Error(response.ErrBadRequest.WithDetails(err.Error()))
				return
			}

			log.Error().Err(err).Msg("create setup")
			c.Error(response.ErrInternalServerError)
			return
		}

		c.JSON(http.StatusCreated, setup)
	}
}

func UpdateSetup(setupSvc services.SetupService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		user, ok := ctx.Value(contextkey.User).(*entities.User)
		if !ok {
			log.Error().Msg("cast user from context")
			c.Error(response.ErrInternalServerError)
			return
		}

		setupID, err := uuid.Parse(c.Param("setup_id"))
		if err != nil {
			log.Debug().Err(err).Msg("invalid setup_id parameter")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_MissingOrMalformedResourceID))
			return
		}

		c.Request.Body = http.MaxBytesReader(c.Writer, c.Request.Body, MAX_UPLOAD_SIZE)

		var setup entities.Setup
		if err := c.ShouldBindJSON(&setup); err != nil {
			log.Info().Err(err).Msg("bind setup json")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_InvalidRequestPayload))
			return
		}
		setup.ID = setupID

		err = setupSvc.Update(ctx, &setup, user)
		if err != nil {
			if vErr, ok := err.(validators.ValidationErrors); ok {
				c.Error(response.ErrBadRequest.WithDetails(vErr))
				return
			}

			if errors.Is(err, domainErrors.ErrResourceWithoutPermission) {
				log.Warn().Msg("an unauthorized setup update was made")
				c.Error(response.ErrForbidden)
				return
			}

			if errors.Is(err, domainErrors.ErrResourceNotFound) {
				c.Error(response.ErrNotFound)
				return
			}

			log.Error().Err(err).Msg("create setup")
			c.Error(response.ErrInternalServerError)
			return
		}

		c.JSON(http.StatusOK, setup)
	}
}

func RecordVote(setupSvc services.SetupService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		user, ok := ctx.Value(contextkey.User).(*entities.User)
		if !ok {
			log.Error().Msg("cast user from context")
			c.Error(response.ErrInternalServerError)
			return
		}

		setupID, err := uuid.Parse(c.Param("setup_id"))
		if err != nil {
			log.Debug().Err(err).Msg("invalid setup_id parameter")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_MissingOrMalformedResourceID))
			return
		}

		vote := c.Param("vote")
		switch vote {
		case entities.SetupVote_Upvote, entities.SetupVote_Downvote:

		default:
			log.Debug().Msg("invalid vote parameter")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_MissingOrMalformedResourceID))
			return
		}

		rating, err := setupSvc.RecordVote(ctx, setupID, user.ID, vote == entities.SetupVote_Upvote)
		if err != nil {
			if errors.Is(err, domainErrors.ErrResourceNotFound) {
				c.Error(response.ErrNotFound)
				return
			}

			log.Error().Err(err).Msg("record vote for setup")
			c.Status(http.StatusInternalServerError)
			return
		}

		c.JSON(http.StatusOK, gin.H{"rating": rating})
	}
}
