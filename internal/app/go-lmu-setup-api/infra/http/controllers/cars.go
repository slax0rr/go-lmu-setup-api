package controllers

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/application/services"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/constants"
	domainErrors "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/errors"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/logger"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/response"
)

func ListCars(carSvc services.CarService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)
		cars, err := carSvc.Get(ctx)
		if err != nil {
			log.Error().Err(err).Msg("get cars")
			c.Error(response.ErrInternalServerError)
			return
		}

		c.JSON(http.StatusOK, cars)
	}
}

func GetCar(carSvc services.CarService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)
		id := c.Param("id")
		carID, err := uuid.Parse(id)
		if err != nil {
			log.Debug().Err(err).Msg("id parameter not a valid uuid")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_MissingOrMalformedResourceID))
			return
		}

		car, err := carSvc.GetByID(ctx, carID)
		if err != nil {
			if errors.Is(err, domainErrors.ErrResourceNotFound) {
				c.Error(response.ErrNotFound)
				return
			}
			log.Error().Err(err).Msg("get car by id")
			c.Error(response.ErrInternalServerError)
			return
		}

		c.JSON(http.StatusOK, car)
	}
}
