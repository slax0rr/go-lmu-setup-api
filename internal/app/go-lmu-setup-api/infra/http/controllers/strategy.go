package controllers

import (
	"context"
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/application/services"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/constants"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	domainErrors "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/errors"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/contextkey"
	infraHttp "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/logger"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/response"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/validators"
)

func CalculateStrategy(stratSvc services.StrategyService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		var raceInfo entities.RaceInfo
		if err := c.ShouldBindJSON(&raceInfo); err != nil {
			log.Info().Err(err).Msg("bind strategy race info json")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_InvalidRequestPayload))
			return
		}

		log.Debug().Any("strat_info", raceInfo).Msg("calculating strategy")
		strat, err := stratSvc.CalculatePitRequirement(ctx, raceInfo)
		if err != nil {
			if vErr, ok := err.(validators.ValidationErrors); ok {
				c.Error(response.ErrBadRequest.WithDetails(vErr))
				return
			}
			c.Error(response.ErrBadRequest.
				WithDetails(err.Error()))
			return
		}

		c.JSON(http.StatusOK, strat)
	}
}

func SaveStrategy(stratSvc services.StrategyService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		user, ok := ctx.Value(contextkey.User).(*entities.User)
		if !ok {
			log.Error().Msg("user data in context invalid")
			c.Error(response.ErrInternalServerError)
			return
		}

		var strat entities.Strategy
		if err := c.ShouldBindJSON(&strat); err != nil {
			log.Info().Err(err).Msg("bind strategy json")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_InvalidRequestPayload))
			return
		}

		strat.UserID = user.ID

		err := stratSvc.Save(ctx, &strat)
		if err != nil {
			if vErr, ok := err.(validators.ValidationErrors); ok {
				c.Error(response.ErrBadRequest.WithDetails(vErr))
				return
			}
			log.Error().Err(err).Msg("save strategy")
			c.Error(response.ErrInternalServerError)
			return
		}

		c.JSON(http.StatusOK, strat)
	}
}

func GetStrategy(stratSvc services.StrategyService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		id, err := uuid.Parse(c.Param("strategy_id"))
		if err != nil {
			log.Debug().Err(err).Msg("invalid strategy_id parameter")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_MissingOrMalformedResourceID))
			return
		}

		userID := uuid.Nil
		user, ok := ctx.Value(contextkey.User).(*entities.User)
		if ok {
			log.Debug().Msg("user data set, using for retrieval of strategy")
			userID = user.ID
		} else {
			log.Warn().Msg("user data in context not in valid format")
		}

		strat, err := stratSvc.Get(ctx, id, userID)
		if err != nil {
			if errors.Is(err, domainErrors.ErrResourceNotFound) {
				c.Error(response.ErrNotFound)
				return
			}
			log.Error().Err(err).Msg("get strtegy by id")
			c.Error(response.ErrInternalServerError)
			return
		}

		c.JSON(http.StatusOK, strat)
	}
}

func GetUserStrategies(stratSvc services.StrategyService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		user, ok := ctx.Value(contextkey.User).(*entities.User)
		if !ok {
			log.Error().Msg("user data in context invalid")
			c.Error(response.ErrInternalServerError)
			return
		}

		pg, ok := ctx.Value(contextkey.Pagination).(*infraHttp.Pagination)
		if !ok {
			pg = &infraHttp.Pagination{}
		}

		strats, err := stratSvc.GetUserStrategies(ctx, user.ID, pg)
		if err != nil {
			log.Error().Err(err).Msg("get saved user strategies")
			c.Error(response.ErrInternalServerError)
			return
		}

		c.JSON(http.StatusOK, strats)
	}
}

func AdjustStrategyWithProgress(stratSvc services.StrategyService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		id, err := uuid.Parse(c.Param("strategy_id"))
		if err != nil {
			log.Debug().Err(err).Msg("invalid strategy_id parameter")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_MissingOrMalformedResourceID))
			return
		}

		userID := uuid.Nil
		user, ok := ctx.Value(contextkey.User).(*entities.User)
		if ok {
			log.Debug().Msg("user data set, using for retrieval of strategy")
			userID = user.ID
		} else {
			log.Warn().Msg("user data in context not in valid format")
		}

		var progress entities.RaceProgress
		if err := c.ShouldBindJSON(&progress); err != nil {
			log.Info().Err(err).Msg("bind race progress as json failed")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_InvalidRequestPayload))
			return
		}

		strat, err := stratSvc.ApplyRaceProgress(ctx, id, userID, c.Query("passphrase"), &progress)
		if err != nil {
			switch {
			case errors.Is(err, domainErrors.ErrResourceNotFound):
				c.Error(response.ErrNotFound)

			case errors.Is(err, domainErrors.ErrResourceWithoutPermission):
				log.Info().Msg("an unauthorized strategy adjustment was made")
				c.Error(response.ErrForbidden)

			default:
				log.Error().Err(err).Msg("error recalculating stints for strategy")
				c.Error(response.ErrInternalServerError)
			}

			return
		}

		c.JSON(http.StatusOK, strat)
	}
}

func EditStint(stratSvc services.StrategyService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		strategyID, err := uuid.Parse(c.Param("strategy_id"))
		if err != nil {
			log.Debug().Err(err).Msg("invalid strategy_id parameter")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_MissingOrMalformedResourceID))
			return
		}

		ctx = logger.WithContext(context.WithValue(ctx, contextkey.StrategyID, strategyID))
		log = logger.FromContext(ctx)

		userID := uuid.Nil
		user, ok := ctx.Value(contextkey.User).(*entities.User)
		if ok {
			log.Debug().Msg("user data set, using for retrieval of strategy")
			userID = user.ID
		} else {
			log.Warn().Msg("user data in context not in valid format")
		}

		var stintAdj entities.StintAdjustment
		if err := c.ShouldBindJSON(&stintAdj); err != nil {
			log.Info().Err(err).Msg("bind stint adjustment as json failed")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_InvalidRequestPayload))
			return
		}

		stintNum, err := strconv.ParseUint(c.Param("stint_num"), 10, 0)
		if err != nil {
			log.Debug().Err(err).Msg("invalid stint_num parameter")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_StintNumberInvalid))
			return
		}
		stintAdj.StintNumber = uint(stintNum)
		stintAdj.Type = c.Param("type")
		switch stintAdj.Type {
		case entities.StintAdjustmentType_Laps,
			entities.StintAdjustmentType_PitLength:

		default:
			c.Error(response.ErrNotFound)
		}

		strat, err := stratSvc.EditStint(ctx, strategyID, userID, c.Query("passphrase"), stintAdj)
		if err != nil {
			switch {
			case errors.Is(err, domainErrors.ErrResourceNotFound):
				c.Error(response.ErrNotFound)

			case errors.Is(err, domainErrors.ErrUnknownStintAdjustment):
				c.Error(response.ErrNotFound.WithDetails(err.Error()))

			case errors.Is(err, domainErrors.ErrResourceWithoutPermission):
				log.Info().Msg("an unauthorized strategy stint adjustment was made")
				c.Error(response.ErrForbidden)

			case errors.Is(err, domainErrors.ErrStintComplete):
				c.Error(response.ErrConflict.WithDetails(err.Error()))

			default:
				log.Error().Err(err).Msg("error recalculating stints for strategy")
				c.Error(response.ErrInternalServerError)
			}

			return
		}

		c.JSON(http.StatusOK, strat)
	}
}
