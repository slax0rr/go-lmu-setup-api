package controllers

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/application/services"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/constants"
	domainErrors "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/errors"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/logger"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/response"
)

func ListTrackLayouts(trackSvc services.TrackLayoutService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		tracks, err := trackSvc.Get(ctx)
		if err != nil {
			log.Error().Err(err).Msg("get tracks")
			c.Error(response.ErrInternalServerError)
			return
		}

		c.JSON(http.StatusOK, tracks)
	}
}

func GetTrackLayout(trackSvc services.TrackLayoutService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		id := c.Param("id")
		trackID, err := uuid.Parse(id)
		if err != nil {
			log.Debug().Err(err).Msg("id parameter not a valid uuid")
			c.Error(response.ErrBadRequest.
				WithDetails(constants.ErrorDetails_MissingOrMalformedResourceID))
			return
		}

		track, err := trackSvc.GetByID(ctx, trackID)
		if err != nil {
			if errors.Is(err, domainErrors.ErrResourceNotFound) {
				c.Error(response.ErrNotFound)
				return
			}
			log.Error().Err(err).Msg("get track by id")
			c.Error(response.ErrInternalServerError)
			return
		}

		c.JSON(http.StatusOK, track)
	}
}
