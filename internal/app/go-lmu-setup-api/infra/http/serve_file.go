package http

import (
	"net/url"
	"strings"
	"unicode"

	"github.com/gin-gonic/gin"
)

var quoteEscaper = strings.NewReplacer("\\", "\\\\", `"`, "\\\"")

func escapeQuotes(s string) string {
	return quoteEscaper.Replace(s)
}
func isASCII(s string) bool {
	for i := 0; i < len(s); i++ {
		if s[i] > unicode.MaxASCII {
			return false
		}
	}
	return true
}

// ServeFile is a variation of the Gin Web Framework FileAttachment to support
// serving of a file for download from a []byte.
// https://github.com/gin-gonic/gin/blob/v1.10.0/context.go#L1097
func ServeFile(c *gin.Context, code int, filename string, content []byte) {
	if isASCII(filename) {
		c.Writer.Header().Set("Content-Disposition", `attachment; filename="`+escapeQuotes(filename)+`"`)
	} else {
		c.Writer.Header().Set("Content-Disposition", `attachment; filename*=UTF-8''`+url.QueryEscape(filename))
	}
	c.Data(code, "application/octet-stream", content)
}
