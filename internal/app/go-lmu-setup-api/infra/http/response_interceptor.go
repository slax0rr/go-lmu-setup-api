package http

import (
	"bytes"

	"github.com/gin-gonic/gin"
)

type ResponseInterceptor struct {
	gin.ResponseWriter
	body *bytes.Buffer

	Rewrite bool
}

func (w *ResponseInterceptor) Intercept(ctx *gin.Context) {
	w.ResponseWriter = ctx.Writer
	w.body = bytes.NewBufferString("")
	ctx.Writer = w
}

func (w *ResponseInterceptor) Stop(ctx *gin.Context) {
	ctx.Writer = w.ResponseWriter
	if w.body.Len() > 0 {
		ctx.Writer.Write(w.body.Bytes())
	}
}

func (w *ResponseInterceptor) Write(p []byte) (n int, err error) {
	if w.Rewrite {
		w.body = bytes.NewBufferString("")
	}
	w.body.Write(p)
	return len(p), nil
}

func (w *ResponseInterceptor) GetBody() []byte {
	return w.body.Bytes()
}
