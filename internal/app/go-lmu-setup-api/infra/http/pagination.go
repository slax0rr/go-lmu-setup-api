package http

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"

	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb/query"
)

const (
	Pagination_Offset string = "offset"
	Pagination_Limit  string = "limit"

	DefaultLimit uint64 = 10
)

// TODO(slax0rr): Move to a more appropriate location, having it in infra is not
// appropriate with the onion architecture but is being kept here for the
// purposes of quicker delivery.
type Pagination struct {
	Offset uint64 `json:"offset"`
	Limit  uint64 `json:"limit"`
	Total  int64  `json:"total"`
}

func (p *Pagination) ToLimit() query.Limit {
	l := query.Limit{
		Limit:  int64(p.Limit),
		Offset: int64(p.Offset),
	}

	return l
}

type PaginatedItemsResponse struct {
	Items      json.RawMessage `json:"items"`
	Pagination Pagination      `json:"pagination"`
}

func ParsePagination(c *gin.Context) (*Pagination, error) {
	params := map[string]string{}
	err := c.BindQuery(&params)
	if err != nil {
		return nil, fmt.Errorf("bind pagination query: %w", err)
	}

	pagination := Pagination{
		Limit:  0,
		Offset: 0,
	}
	if limit, ok := params[fmt.Sprintf("pagination[%s]", Pagination_Limit)]; ok {
		pagination.Limit, err = strconv.ParseUint(limit, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("unable to parse page: %w", err)
		}
	}

	if offset, ok := params[fmt.Sprintf("pagination[%s]", Pagination_Offset)]; ok {
		pagination.Offset, err = strconv.ParseUint(offset, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("unable to parse page size: %w", err)
		}
	}

	if pagination.Limit == 0 {
		pagination.Limit = DefaultLimit
	}

	return &pagination, nil
}
