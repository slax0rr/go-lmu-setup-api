package middleware

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/rs/zerolog"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/auth"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/contextkey"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/logger"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/settings"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/response"
)

func CheckAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		log.Debug().Msg("checking user authentication")

		user, ok := ctx.Value(contextkey.User).(*entities.User)
		if !ok {
			var err error
			user, err = getUserDataFromRequestHeaders(log, c.Request.Header)
			switch {
			case err != nil:
				log.Error().Err(err).Msg("unmarshal authapi user information")
				c.Error(response.ErrUnauthorized)
				c.Abort()
				return

			case user == nil:
				log.Info().Msg("user information not set")
				c.Error(response.ErrUnauthorized)
				c.Abort()
				return
			}
		}

		if user.ID == uuid.Nil || user.Username == "" {
			log.Info().Str("user_id", user.ID.String()).Str("username", user.Username).
				Msg("user information empty")
			c.Error(response.ErrUnauthorized)
			c.Abort()
			return
		}

		ctx = context.WithValue(ctx, contextkey.User, user)
		c.Request = c.Request.WithContext(ctx)

		c.Next()
	}
}

func EnrichUserData() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := logger.FromContext(c.Request.Context())
		log.Debug().Msg("enriching request with user data")

		user, err := getUserDataFromRequestHeaders(log, c.Request.Header)
		if err != nil {
			log.Debug().Err(err).Msg("unable to enrich request with user data")
		}

		if user != nil {
			ctx := context.WithValue(c.Request.Context(), contextkey.User, user)
			c.Request = c.Request.WithContext(ctx)
		}

		c.Next()
	}
}

func getUserDataFromRequestHeaders(log *zerolog.Logger, headers http.Header) (*entities.User, error) {
	log.Debug().Msg("get user data from headers")

	tokenString := headers.Get(settings.UserTokenHeader)
	if tokenString == "" {
		return nil, fmt.Errorf("user token not set")
	}

	claims, err := auth.ValidateHS256Token(tokenString)
	if err != nil {
		log.Debug().Err(err).Msg("validate user token failed")
		return nil, fmt.Errorf("validate user token: %w", err)
	}

	return claims.User, nil
}
