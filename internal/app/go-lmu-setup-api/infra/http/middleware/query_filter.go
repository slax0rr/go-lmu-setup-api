package middleware

import (
	"context"

	"github.com/gin-gonic/gin"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/contextkey"
	infraHttp "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/logger"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/response"
)

func QueryFilter() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)
		qf, err := infraHttp.ParseQueryFilter(c)
		if err != nil {
			log.Error().Err(err).Msg("unable to parse query filters")
			c.Error(response.ErrInternalServerError)
			c.Abort()
			return
		}

		ctx = context.WithValue(ctx, contextkey.QueryFilter, qf)
		c.Request = c.Request.WithContext(ctx)

		c.Next()
	}
}
