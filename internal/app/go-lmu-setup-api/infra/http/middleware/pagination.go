package middleware

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/contextkey"
	infraHttp "gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/http"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/logger"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/response"
)

func Pagination() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()
		log := logger.FromContext(ctx)

		pg, err := infraHttp.ParsePagination(c)
		if err != nil {
			log.Error().Err(err).Msg("unable to parse pagination from uri")
			c.Error(response.ErrBadRequest)
			c.Abort()
			return
		}

		log.Debug().Any("pagination", pg).Msg("setting pagination to context")

		ctx = context.WithValue(ctx, contextkey.Pagination, pg)
		c.Request = c.Request.WithContext(ctx)

		writer := &infraHttp.ResponseInterceptor{Rewrite: true}
		writer.Intercept(c)
		defer writer.Stop(c)

		c.Next()

		if c.Writer.Status() == http.StatusOK && len(c.Errors) == 0 {
			log.Debug().Msg("status on writer set to 200 - rewriting response with pagination")

			c.JSON(writer.Status(), infraHttp.PaginatedItemsResponse{
				Items:      writer.GetBody(),
				Pagination: *pg,
			})
		}
	}
}
