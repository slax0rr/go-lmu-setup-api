package http

import (
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
)

// TODO(slax0rr): Move to a more appropriate location, having it in infra is not
// appropriate with the onion architecture but is being kept here for the
// purposes of quicker delivery.
type QueryFilter = map[string][]string

func ParseQueryFilter(c *gin.Context) (QueryFilter, error) {
	params := map[string]string{}
	err := c.BindQuery(&params)
	if err != nil {
		return nil, fmt.Errorf("bind filter query: %w", err)
	}

	filters := QueryFilter{}
	for key, value := range params {
		if !strings.HasPrefix(key, "filter") {
			continue
		}
		start := strings.Index(key, "[")
		end := strings.Index(key, "]")

		if start != -1 && end != -1 && start < end {
			name := key[start+1 : end]
			filters[name] = strings.Split(value, ",")
		}
	}

	return filters, nil
}
