package auth

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"io"

	"github.com/golang-jwt/jwt/v5"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/domain/entities"
)

var (
	sharedKey []byte
)

type UserTokenClaims struct {
	jwt.RegisteredClaims

	User *entities.User
}

func Configure(encodedKey []byte) error {
	buf := bytes.NewBuffer(encodedKey)
	decoder := base64.NewDecoder(base64.StdEncoding, buf)
	sharedKey = make([]byte, 32)
	n, err := io.ReadFull(decoder, sharedKey)
	if err != nil {
		return fmt.Errorf("decode user token key: %w", err)
	}
	if n < 32 {
		return fmt.Errorf("key size (%d) is not sufficient, must be at least 32-bytes", n)
	}

	return nil
}

func ValidateHS256Token(tokenString string) (*UserTokenClaims, error) {
	token, err := jwt.ParseWithClaims(tokenString, &UserTokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return sharedKey, nil
	})
	if err != nil {
		return nil, fmt.Errorf("parse token with claims: %w", err)
	}

	if claims, ok := token.Claims.(*UserTokenClaims); ok && token.Valid {
		return claims, nil
	} else {
		return nil, fmt.Errorf("token claims or token invalid")
	}
}
