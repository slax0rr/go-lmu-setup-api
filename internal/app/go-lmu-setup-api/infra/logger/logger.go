package logger

import (
	"context"

	"github.com/rs/zerolog"

	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/infra/contextkey"
)

var loggedContextKeys = []contextkey.ContextKey{
	contextkey.StrategyID,
}

func WithContext(ctx context.Context) context.Context {
	log := FromContext(ctx)
	trackContextKeys(ctx, log)
	return log.WithContext(ctx)
}

func FromContext(ctx context.Context) *zerolog.Logger {
	return zerolog.Ctx(ctx)
}

func trackContextKeys(ctx context.Context, log *zerolog.Logger) {
	kvs := map[string]interface{}{}

	for _, ck := range loggedContextKeys {
		v := ctx.Value(ck)
		if v == nil {
			continue
		}
		kvs[ck.Name()] = v
	}

	if len(kvs) == 0 {
		return
	}

	log.UpdateContext(func(c zerolog.Context) zerolog.Context {
		for k, v := range kvs {
			c = c.Any(k, v)
		}
		return c
	})
}
