package validators

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
	"sync"

	"github.com/go-playground/validator/v10"
)

type ValidationError struct {
	Field string      `json:"field"`
	Param string      `json:"param,omitempty"`
	Value interface{} `json:"value"`
	Err   string      `json:"error"`
}

type ValidationErrors []ValidationError

func (ve ValidationErrors) Error() string {
	bs, _ := json.Marshal(ve)
	return string(bs)
}

var (
	vldOnce sync.Once
	vld     *validator.Validate
)

func getValidator() *validator.Validate {
	vldOnce.Do(func() {
		vld = validator.New()
	})
	return vld
}

func RegisterCustomValidation(name string, vldFunc func(reflect.Value) bool) {
	getValidator().RegisterValidation(name, func(fl validator.FieldLevel) bool {
		return vldFunc(fl.Field())
	})
}

func ValidateStruct(s interface{}) error {
	err := getValidator().Struct(s)
	if err == nil {
		return nil
	}

	return formatValidationErrors(err, s)
}

func formatValidationErrors(err error, structType any) error {
	str := reflect.TypeOf(structType)
	if str.Kind() == reflect.Ptr {
		str = str.Elem()
	}

	if validationErrors, ok := err.(validator.ValidationErrors); ok {
		vErrs := make(ValidationErrors, len(validationErrors))

		for idx, vErr := range validationErrors {
			fieldName := getFieldJSONTag(vErr.Field(), str)

			vErrs[idx] = ValidationError{
				Field: fieldName,
				Param: getFieldJSONTag(vErr.Param(), str),
				Value: vErr.Value(),
				Err:   formatMessage(vErr),
			}
		}

		return vErrs
	}

	return fmt.Errorf("validate setup: %w", err)
}

func getFieldJSONTag(fieldName string, str reflect.Type) string {
	field, ok := str.FieldByName(fieldName)
	if ok {
		fieldName = strings.Split(field.Tag.Get("json"), ",")[0]
	}
	return fieldName
}

var (
	ErrorMessage_Default = "field value is invalid"

	ErrorMessage_Required        = "value is required"
	ErrorMessage_UUID            = "value must be a valid UUID"
	ErrorMessage_Equals          = "value must be equal to '%s'"
	ErrorMessage_Laptime         = "value must be a valid laptime 'MM:SS:mmm'"
	ErrorMessage_HTTP_URL        = "value must a valid http url"
	ErrorMessage_GreaterThan     = "value must be greater than '%s'"
	ErrorMessage_RequiredWithout = "value or '%s' is required"
)

func formatMessage(fe validator.FieldError) string {
	tag := fe.Tag()
	value := fe.Param()

	switch tag {
	case "required":
		return ErrorMessage_Required

	case "uuid":
		return ErrorMessage_UUID

	case "eq":
		return fmt.Sprintf(ErrorMessage_Equals, value)

	case "custom_laptime_validation":
		return ErrorMessage_Laptime

	case "http_url":
		return ErrorMessage_HTTP_URL

	case "gt":
		return fmt.Sprintf(ErrorMessage_GreaterThan, value)

	case "required_without":
		return fmt.Sprintf(ErrorMessage_RequiredWithout, value)

	default:
		return ErrorMessage_Default
	}
}
