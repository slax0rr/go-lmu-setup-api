package data

import (
	"github.com/jackc/pgx/v5/pgxpool"
)

type AccessObject struct {
	db *db
}

// TODO(slax0rr): instead of requiring all to be added at construction add methods
// to allow for dynamic additions of data "elements'"
func NewAccessObject(dbpool *pgxpool.Pool) *AccessObject {
	return &AccessObject{
		db: newDB(dbpool),
	}
}

func (ao *AccessObject) GetDB() *db {
	return ao.db
}

func (ao *AccessObject) GetDBPool() *pgxpool.Pool {
	return ao.db.dbpool
}
