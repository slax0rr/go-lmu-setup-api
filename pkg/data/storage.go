package data

type Storage interface {
	Store(string, []byte) error
	Read(string) ([]byte, error)
	GetFilepath(string) (string, error)
}
