package data

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5/pgxpool"

	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb"
)

type db struct {
	dbpool *pgxpool.Pool
}

func newDB(dbpool *pgxpool.Pool) *db {
	return &db{dbpool}
}

func (d *db) Begin(ctx context.Context) (postgresdb.Tx, postgresdb.CloseTx, error) {
	if d.dbpool == nil {
		return nil, nil, postgresdb.ErrNoDBConnection
	}

	tx, err := d.dbpool.Begin(ctx)
	if err != nil {
		return nil, nil, fmt.Errorf("begin transaction: %w", err)
	}

	return tx, func(err error) error {
		if p := recover(); p != nil {
			return d.Rollback(ctx, tx)
		} else if err != nil {
			return d.Rollback(ctx, tx)
		}
		return d.Commit(ctx, tx)
	}, nil
}

func (d *db) Commit(ctx context.Context, tx postgresdb.Tx) error {
	if tx == nil {
		return postgresdb.ErrNoTransaction
	}

	err := tx.Commit(ctx)
	if err != nil {
		return fmt.Errorf("commit transaction: %w", err)
	}

	return nil
}

func (d *db) Rollback(ctx context.Context, tx postgresdb.Tx) error {
	if tx == nil {
		return postgresdb.ErrNoTransaction
	}

	err := tx.Rollback(ctx)
	if err != nil {
		return fmt.Errorf("rollback transaction: %w", err)
	}

	return nil
}
