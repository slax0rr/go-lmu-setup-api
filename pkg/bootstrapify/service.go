package bootstrapify

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/rs/zerolog"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify/components"
)

type Service struct {
	orchestrator *components.Orchestrator
	logger       *zerolog.Logger
}

type Option func(*Service) error

func NewService(cfg Config, opts ...Option) *Service {
	service := &Service{
		orchestrator: components.NewOrchestrator(),
		logger:       cfg.Logger,
	}

	for _, opt := range opts {
		err := opt(service)
		if err != nil {
			panic(fmt.Errorf("component setup failed: %w", err))
		}
	}

	return service
}

func (svc Service) Start() error {
	err := svc.orchestrator.OrderComponents()
	if err != nil {
		return fmt.Errorf("ordering components: %w", err)
	}

	err = svc.orchestrator.Configure()
	if err != nil {
		return fmt.Errorf("configuring components: %w", err)
	}

	err = svc.orchestrator.Start()
	if err != nil {
		return fmt.Errorf("starting components: %w", err)
	}

	svc.waitForSignals()

	return nil
}

func (svc Service) waitForSignals() {
	sig := make(chan os.Signal, 1024)
	signal.Notify(sig, syscall.SIGHUP, syscall.SIGTERM, syscall.SIGINT)
	for {
		select {
		case s := <-sig:
			switch s {
			case syscall.SIGHUP:
				svc.logger.Debug().Any("signal", s).Msg("sighup caught")
				return

			case syscall.SIGTERM, syscall.SIGINT:
				svc.logger.Debug().Any("signal", s).Msg("term/int signal caught")
				return

			default:
				svc.logger.Warn().Any("signal", s).Msg("unhandled signal caught")
			}
		}
	}
}

func Component(c components.Component, opts ...components.Option) Option {
	return func(service *Service) error {
		opts = append(opts, components.WithLogger(service.logger))
		service.orchestrator.Register(c, opts...)
		return nil
	}
}

func Run(name string, opts ...RunComponentOption) Option {
	return Component(
		NewRunComponent(
			name,
			opts...,
		),
	)
}

func RunAfter(dependencies ...string) RunComponentOption {
	return func(c *RunComponent) {
		c.dependencies = append(c.dependencies, dependencies...)
	}
}

func RunAfterConfigure(fn RunComponentHook) RunComponentOption {
	return func(c *RunComponent) {
		c.hooks.afterConfigure = fn
	}
}

func RunAfterStart(fn RunComponentHook) RunComponentOption {
	return func(c *RunComponent) {
		c.hooks.afterStart = fn
	}
}

func RunAfterShutdown(fn RunComponentHook) RunComponentOption {
	return func(c *RunComponent) {
		c.hooks.afterShutdown = fn
	}
}
