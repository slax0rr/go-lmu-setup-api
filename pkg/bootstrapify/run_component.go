package bootstrapify

import (
	"fmt"

	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify/components"
)

type RunComponentOption func(*RunComponent)

type RunComponentHook func(*RunComponent) error

type RunComponentHooks struct {
	afterConfigure RunComponentHook
	afterStart     RunComponentHook
	afterShutdown  RunComponentHook
}

type RunComponent struct {
	components.Base

	hooks        RunComponentHooks
	dependencies []string
}

func NewRunComponent(name string, opts ...RunComponentOption) components.Component {
	c := &RunComponent{
		hooks: RunComponentHooks{},
	}

	c.SetName(name)

	RunComponentOptions(opts...)(c)

	for _, dep := range c.dependencies {
		c.AddDependencies(dep)
	}

	return c
}

func RunComponentOptions(opts ...RunComponentOption) RunComponentOption {
	return func(c *RunComponent) {
		for _, opt := range opts {
			opt(c)
		}
	}
}

func (c *RunComponent) Configure(opts ...components.Option) error {
	for _, opt := range opts {
		err := opt.Apply(c)
		if err != nil {
			return fmt.Errorf("apply RunComponent options: %w", err)
		}
	}

	if c.hooks.afterConfigure != nil {
		return c.hooks.afterConfigure(c)
	}

	return nil
}

func (c *RunComponent) Start() error {
	if c.hooks.afterStart != nil {
		return c.hooks.afterStart(c)
	}

	return nil
}

func (c *RunComponent) Shutdown() error {
	if c.hooks.afterShutdown != nil {
		return c.hooks.afterShutdown(c)
	}
	return nil
}
