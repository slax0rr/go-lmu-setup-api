package components

import (
	"github.com/rs/zerolog"
)

type Base struct {
	name         string
	logger       *zerolog.Logger
	dependencies map[string]Component
}

func (b *Base) SetName(name string) {
	b.name = name
}

func (b *Base) Name() string {
	return b.name
}

func (b *Base) AddDependencies(deps ...string) {
	if b.dependencies == nil {
		b.dependencies = make(map[string]Component)
	}

	for _, dep := range deps {
		b.dependencies[dep] = nil
	}
}

func (b *Base) SetDependency(c Component) {
	if b.dependencies == nil {
		b.dependencies = make(map[string]Component)
	}

	b.dependencies[c.Name()] = c
}

func (b *Base) Dependency(name string) Component {
	if b.dependencies == nil {
		return nil
	}
	return b.dependencies[name]
}

func (b *Base) Dependencies() map[string]Component {
	return b.dependencies
}

func (b *Base) SetLogger(l *zerolog.Logger) {
	b.logger = l
}

func (b *Base) Logger() *zerolog.Logger {
	return b.logger
}

func (b *Base) Log(lvl zerolog.Level, message string, fields ...any) {
	if b.Logger() == nil {
		return
	}

	b.Logger().WithLevel(lvl).Fields(fields).Msg(message)
}
