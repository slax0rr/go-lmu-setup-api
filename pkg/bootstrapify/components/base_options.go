package components

import "github.com/rs/zerolog"

type withLogger struct {
	logger *zerolog.Logger
}

func (o withLogger) Apply(c Component) error {
	c.SetLogger(o.logger)
	return nil
}

func WithLogger(log *zerolog.Logger) Option {
	return withLogger{
		logger: log,
	}
}
