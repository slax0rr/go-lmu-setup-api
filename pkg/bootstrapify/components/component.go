package components

import "github.com/rs/zerolog"

type Component interface {
	Configure(...Option) error
	Start() error
	Shutdown() error

	SetName(string)
	Name() string

	AddDependencies(...string)
	SetDependency(Component)
	Dependency(string) Component
	Dependencies() map[string]Component

	SetLogger(*zerolog.Logger)
	Logger() *zerolog.Logger
	Log(zerolog.Level, string, ...any)
}
