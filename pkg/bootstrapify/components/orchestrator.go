package components

import (
	"fmt"
	"sync"
)

type Orchestrator struct {
	mu sync.Mutex

	registeredComponents []Component
	components           map[string]Component
	componentOptions     map[string][]Option

	orderedComponents []Component
}

func NewOrchestrator() *Orchestrator {
	return &Orchestrator{
		mu: sync.Mutex{},

		components:           make(map[string]Component),
		registeredComponents: make([]Component, 0),
		componentOptions:     make(map[string][]Option),

		orderedComponents: make([]Component, 0),
	}
}

func (o *Orchestrator) Register(c Component, opts ...Option) {
	o.mu.Lock()
	defer o.mu.Unlock()

	o.registeredComponents = append(o.registeredComponents, c)
	o.components[c.Name()] = c
	o.componentOptions[c.Name()] = opts
}

func (o *Orchestrator) Component(name string) Component {
	component, ok := o.components[name]
	if !ok {
		return nil
	}
	return component
}

func (o *Orchestrator) OrderComponents() error {
	for _, c := range o.registeredComponents {
		if err := o.orderComponent(c, map[string]struct{}{}); err != nil {
			return fmt.Errorf("ordering component %s: %w", c.Name(), err)
		}
	}

	return nil
}

func (o *Orchestrator) orderComponent(c Component, seen map[string]struct{}) error {
	name := c.Name()
	for _, orderedComponenent := range o.orderedComponents {
		if orderedComponenent.Name() == name {
			return nil
		}
	}

	if seen == nil {
		seen = map[string]struct{}{}
	}

	if _, ok := seen[name]; ok {
		return fmt.Errorf("circular component dependency")
	}
	seen[name] = struct{}{}

	for depName := range c.Dependencies() {
		dep := o.Component(depName)
		if dep == nil {
			return fmt.Errorf("component %s not registered", depName)
		}

		if err := o.orderComponent(dep, seen); err != nil {
			return err
		}

		c.SetDependency(dep)
	}

	o.orderedComponents = append(o.orderedComponents, c)

	return nil
}

func (o *Orchestrator) Configure() error {
	for _, c := range o.orderedComponents {
		err := c.Configure(o.componentOptions[c.Name()]...)
		if err != nil {
			return fmt.Errorf("configuring component %s: %w", c.Name(), err)
		}
	}
	return nil
}

func (o *Orchestrator) Start() error {
	for _, c := range o.orderedComponents {
		err := c.Start()
		if err != nil {
			return fmt.Errorf("component '%s' failed to start: %w", c.Name(), err)
		}
	}
	return nil
}
