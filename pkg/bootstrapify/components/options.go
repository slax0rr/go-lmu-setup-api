package components

type Option interface {
	Apply(Component) error
}
