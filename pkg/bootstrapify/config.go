package bootstrapify

import "github.com/rs/zerolog"

type Config struct {
	Logger *zerolog.Logger
}
