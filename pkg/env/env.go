package env

import (
	"fmt"
	"os"
	"strconv"
)

func GetStringOrDefault(envVar, defaultValue string) string {
	if val := os.Getenv(envVar); val != "" {
		return val
	}
	return defaultValue
}

func MustGetString(envVar string) string {
	if val := os.Getenv(envVar); val != "" {
		return val
	}
	panic(fmt.Sprintf("Environment variable '%s' must be set", envVar))
}

func GetInt64OrDefault(envVar string, defaultValue int64) int64 {
	if val := os.Getenv(envVar); val != "" {
		int64Val, err := strconv.ParseInt(val, 10, 64)
		if err != nil {
			return defaultValue
		}

		return int64Val
	}
	return defaultValue
}

func GetUintOrDefault(envVar string, defaultValue uint64) uint64 {
	if val := os.Getenv(envVar); val != "" {
		uintVal, err := strconv.ParseUint(val, 10, 64)
		if err != nil {
			return defaultValue
		}

		return uintVal
	}
	return defaultValue
}

func MustGetUint(envVar string) uint64 {
	if val := os.Getenv(envVar); val != "" {
		uintVal, err := strconv.ParseUint(val, 10, 64)
		if err != nil {
			panic(fmt.Sprintf("Environment variable '%s' must be an unsigned integer value", envVar))
		}
		return uintVal
	}
	panic(fmt.Sprintf("Environment variable '%s' must be set", envVar))
}
