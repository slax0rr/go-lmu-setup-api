package http

import "fmt"

var (
	ErrUnknownMethod error = fmt.Errorf("unknown http method")
)
