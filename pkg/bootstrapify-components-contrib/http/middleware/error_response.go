package middleware

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/rs/zerolog"

	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/response"
)

func ErrorResponse(log *zerolog.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		if len(c.Errors) == 0 {
			return
		}

		err := c.Errors[0]
		errResp := response.ErrorResponse{
			Error:   err.Error(),
			ErrorID: uuid.New(),
		}

		if apiErr, ok := err.Err.(response.Error); ok {
			errResp.Code = apiErr.StatusCode
			var jsonErr error
			errResp.Details, jsonErr = json.Marshal(apiErr.Details)
			if jsonErr != nil {
				log.Error().Err(jsonErr).Msg("failed marshaling error details")
			}

			if apiErr.Type != 0 {
				err.SetType(apiErr.Type)
			}
		}

		if errResp.Code == 0 {
			log.Warn().Any("response_error", err).
				Msg("error does not contain a status code")
			errResp.Code = http.StatusInternalServerError
		}

		if err.IsType(gin.ErrorTypePrivate) {
			log.Debug().Any("error_response", errResp).
				Msg("private error - not writing to response")
			c.Status(errResp.Code)
			return
		}

		log.Info().Any("error_response", errResp).
			Msg("writing error response")
		c.JSON(errResp.Code, errResp)
	}
}
