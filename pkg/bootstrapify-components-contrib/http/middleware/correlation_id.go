package middleware

import (
	"context"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"

	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/contextkey"
)

func CorrelationID() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := c.Request.Context()

		corrID := c.GetHeader(contextkey.CorrelationIDHeader.Header())
		if corrID == "" {
			corrID = uuid.NewString()
			c.Header(contextkey.CorrelationIDHeader.Header(), corrID)
		}

		ctx = context.WithValue(ctx, contextkey.CorrelationIDHeader, corrID)
		c.Request = c.Request.WithContext(ctx)

		c.Next()
	}
}
