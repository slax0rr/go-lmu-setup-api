package middleware

import (
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"

	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/contextkey"
)

func RequestLogger(logger *zerolog.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		if logger == nil {
			c.Next()
			return
		}

		start := time.Now()

		log := logger.With().Logger()

		ctx := c.Request.Context()
		if corrID, ok := ctx.Value(contextkey.CorrelationIDHeader).(string); ok {
			log = log.With().Str(contextkey.CorrelationIDHeader.Name(), corrID).Logger()
		}

		c.Request = c.Request.WithContext(log.WithContext(ctx))

		c.Next()

		dur := time.Since(start)

		log.Info().
			Dur("duration", dur).
			Str("method", c.Request.Method).
			Str("uri", c.Request.RequestURI).
			Str("proto", c.Request.Proto).
			Int("status", c.Writer.Status()).
			Msg("Request")
	}
}
