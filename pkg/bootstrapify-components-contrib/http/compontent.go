package http

import (
	"context"
	"fmt"
	"net"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"

	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http/middleware"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify/components"
)

const (
	ComponentName string = "http-server"

	defaultServerHost string = "0.0.0.0"
	defaultServerPort int    = 8080
)

type Component struct {
	components.Base

	router     *gin.Engine
	server     *http.Server
	serverHost string
	serverPort int

	routes      []Route
	middlewares []gin.HandlerFunc
}

var ErrUnknownComponent = fmt.Errorf("component is not the %s component", ComponentName)

type Route struct {
	Group      string
	Handler    gin.HandlerFunc
	URI        string
	HTTPMethod string
}

func New() *Component {
	gin.SetMode(gin.ReleaseMode)

	component := &Component{
		router:     gin.New(),
		serverHost: defaultServerHost,
		serverPort: defaultServerPort,
	}
	component.SetName(ComponentName)

	return component
}

func (c *Component) Configure(opts ...components.Option) error {
	for _, opt := range opts {
		err := opt.Apply(c)
		if err != nil {
			return fmt.Errorf("apply %s component options: %w", ComponentName, err)
		}
	}

	c.router.Use(middleware.CorrelationID(), middleware.RequestLogger(c.Logger()))

	err := c.RegisterRoutes(c.routes...)
	if err != nil {
		return fmt.Errorf("registering routes: %w", err)
	}

	return nil
}

func (c *Component) Start() error {
	c.Log(zerolog.DebugLevel, fmt.Sprintf("%s component start", ComponentName))

	address := fmt.Sprintf("%s:%d", c.serverHost, c.serverPort)
	c.server = &http.Server{
		Addr:    address,
		Handler: c.router,
	}

	listener, err := net.Listen("tcp", address)
	if err != nil {
		return fmt.Errorf("starting http-server listener: %w", err)
	}

	go func() error {
		err = c.server.Serve(listener)
		if err != nil {
			// TODO(slax0rr): server failure error not handled - on error must
			// stop the service main loop
			return fmt.Errorf("http server failed: %w", err)
		}
		return nil
	}()

	return nil
}

func (c *Component) Shutdown() error {
	if c.server == nil {
		return nil
	}

	err := c.server.Shutdown(context.TODO())
	if err != nil {
		return fmt.Errorf("shutting down http-server: %w", err)
	}

	return nil
}

func (c *Component) RegisterRoutes(routes ...Route) error {
	for _, route := range routes {
		err := c.RegisterRoute(route)
		if err != nil {
			return fmt.Errorf("register route: %w", err)
		}
	}

	return nil
}

func (c *Component) RegisterRoute(route Route) error {
	c.Log(zerolog.InfoLevel, "registering route",
		"group", route.Group,
		"uri", route.URI,
		"method", route.HTTPMethod,
	)

	group := c.router.Group(route.Group)
	switch route.HTTPMethod {
	case http.MethodGet:
		group.GET(route.URI, route.Handler)

	case http.MethodPost:
		group.POST(route.URI, route.Handler)

	case http.MethodPut:
		group.PUT(route.URI, route.Handler)

	case http.MethodPatch:
		group.PATCH(route.URI, route.Handler)

	case http.MethodDelete:
		group.DELETE(route.URI, route.Handler)

	case http.MethodOptions:
		group.OPTIONS(route.URI, route.Handler)

	case http.MethodHead:
		group.HEAD(route.URI, route.Handler)

	default:
		return fmt.Errorf("%w '%s'", ErrUnknownMethod, route.HTTPMethod)
	}

	return nil
}

func (c *Component) Router() *gin.Engine {
	return c.router
}
