package http

import (
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/logger"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify/components"
)

type withRoute struct {
	route Route
}

func (o withRoute) Apply(c components.Component) error {
	httpComponent, ok := c.(*Component)
	if !ok {
		return ErrUnknownComponent
	}

	if httpComponent.routes == nil {
		httpComponent.routes = []Route{}
	}

	httpComponent.routes = append(httpComponent.routes, o.route)
	return nil
}

func WithRoute(route Route) components.Option {
	return withRoute{
		route: route,
	}
}

type withServerPort struct {
	port int
}

func (o withServerPort) Apply(c components.Component) error {
	httpComponent, ok := c.(*Component)
	if !ok {
		return ErrUnknownComponent
	}
	httpComponent.serverPort = o.port
	return nil
}

func WithServerPort(port int) components.Option {
	return withServerPort{
		port: port,
	}
}

type withRequestLogger struct{}

func (o withRequestLogger) Apply(c components.Component) error {
	httpComponent, ok := c.(*Component)
	if !ok {
		return ErrUnknownComponent
	}
	httpComponent.AddDependencies(logger.ComponentName)
	return nil
}

func WithRequestLogger() components.Option {
	return withRequestLogger{}
}
