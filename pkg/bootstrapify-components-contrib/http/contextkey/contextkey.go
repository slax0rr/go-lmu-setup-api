package contextkey

type contextKey struct {
	header string
	name   string
}

func (k *contextKey) Header() string {
	return k.header
}

func (k *contextKey) Name() string {
	return k.name
}

var (
	CorrelationIDHeader = contextKey{header: "X-Bootstrapify-CorrelationID", name: "correlation_id"}
)
