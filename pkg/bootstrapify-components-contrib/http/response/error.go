package response

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type Error struct {
	StatusCode int
	Err        string
	Details    interface{}
	Type       gin.ErrorType
}

func (err Error) Error() string {
	return err.Err
}

func (err Error) WithDetails(details interface{}) Error {
	err.Details = details
	return err
}

var _ error = (*Error)(nil)

type ErrorResponse struct {
	Error   string          `json:"error"`
	Code    int             `json:"code"`
	Details json.RawMessage `json:"details,omitempty"`
	ErrorID uuid.UUID       `json:"error_id"`
}

var (
	// 400 errors
	ErrBadRequest = Error{
		StatusCode: http.StatusBadRequest,
		Err:        "The request was malformed or missing required parameters.",
		Type:       gin.ErrorTypePublic,
	}

	ErrUnauthorized = Error{
		StatusCode: http.StatusUnauthorized,
		Err:        "Authentication token is missing or is invalid.",
		Type:       gin.ErrorTypePublic,
	}

	ErrForbidden = Error{
		StatusCode: http.StatusForbidden,
		Err:        "Access Denied: You do not have permission to access this resource.",
		Type:       gin.ErrorTypePublic,
	}

	ErrNotFound = Error{
		StatusCode: http.StatusNotFound,
		Err:        "Requested resource not found on the server.",
		Type:       gin.ErrorTypePublic,
	}

	ErrConflict = Error{
		StatusCode: http.StatusConflict,
		Err:        "Resource request conflicts with the current state of the server.",
		Type:       gin.ErrorTypePublic,
	}

	// 500 errors
	ErrInternalServerError = Error{
		StatusCode: http.StatusInternalServerError,
	}
)
