package query_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb/query"
)

func TestOrderBy(t *testing.T) {
	tcs := []struct {
		name   string
		sql    string
		orders query.OrderBy
		result string
	}{
		{
			name:   "empty orders",
			sql:    `select * from table`,
			orders: nil,
			result: `select * from table`,
		},
		{
			name:   "no where",
			sql:    `select * from table`,
			orders: query.OrderBy{{Column: "column1"}, {"column2", query.OrderByDescending}},
			result: `select * from table ORDER BY column1 ASC, column2 DESC`,
		},
		{
			name:   "no where",
			sql:    `select * from table`,
			orders: query.OrderBy{{Column: "column1"}, {"column2", query.OrderByDescending}},
			result: `select * from table ORDER BY column1 ASC, column2 DESC`,
		},
		{
			name:   "no where",
			sql:    `select * from table`,
			orders: query.OrderBy{{Column: "column1"}, {"column2", query.OrderByDescending}},
			result: `select * from table ORDER BY column1 ASC, column2 DESC`,
		},
		{
			name:   "no where",
			sql:    `select * from table`,
			orders: query.OrderBy{{Column: "column1"}, {"column2", query.OrderByDescending}},
			result: `select * from table ORDER BY column1 ASC, column2 DESC`,
		},
		{
			name:   "no where",
			sql:    `select * from table`,
			orders: query.OrderBy{{Column: "column1"}, {"column2", query.OrderByDescending}},
			result: `select * from table ORDER BY column1 ASC, column2 DESC`,
		},
		{
			name:   "with where",
			sql:    `select * from table where foo = "bar"`,
			orders: query.OrderBy{{"column1", query.OrderByAscending}, {"column2", query.OrderByDescending}},
			result: `select * from table where foo = "bar" ORDER BY column1 ASC, column2 DESC`,
		},
		{
			name:   "with where and group by",
			sql:    `select * from table where foo = "bar" group by "column1"`,
			orders: query.OrderBy{{"column1", query.OrderByAscending}, {"column2", query.OrderByDescending}},
			result: `select * from table where foo = "bar" group by "column1" ORDER BY column1 ASC, column2 DESC`,
		},
		{
			name:   "with limit",
			sql:    `select * from table where foo = "bar" group by "column1" limit 1`,
			orders: query.OrderBy{{"column1", query.OrderByAscending}, {"column2", query.OrderByDescending}},
			result: `select * from table where foo = "bar" group by "column1" ORDER BY column1 ASC, column2 DESC limit 1`,
		},
		{
			name:   "with existing order by and limit",
			sql:    `select * from table order by id asc limit 1`,
			orders: query.OrderBy{{"column1", query.OrderByAscending}, {"column2", query.OrderByDescending}},
			result: `select * from table order by id asc, column1 ASC, column2 DESC limit 1`,
		},
		{
			name:   "with existing order by",
			sql:    `select * from table order by id asc`,
			orders: query.OrderBy{{"column1", query.OrderByAscending}, {"column2", query.OrderByDescending}},
			result: `select * from table order by id asc, column1 ASC, column2 DESC`,
		},
	}

	for _, tc := range tcs {
		result := tc.orders.AddOrderBy(tc.sql)
		require.Equalf(t, tc.result, result, "case name '%s'", tc.name)
	}
}
