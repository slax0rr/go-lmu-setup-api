package query

import (
	"fmt"
	"strings"
)

type OrderByDirection = string

var (
	OrderByAscending  OrderByDirection = "ASC"
	OrderByDescending OrderByDirection = "DESC"
)

type OrderClause struct {
	Column    string
	Direction OrderByDirection
}

type OrderBy []OrderClause

func (o *OrderBy) AddOrderBy(sql string) string {
	if o == nil || len(*o) == 0 {
		return sql
	}

	orderParts := make([]string, 0, len(*o))
	for _, clause := range *o {
		if clause.Direction != OrderByAscending && clause.Direction != OrderByDescending {
			clause.Direction = OrderByAscending
		}
		orderParts = append(orderParts, fmt.Sprintf("%s %s", clause.Column, clause.Direction))
	}
	orderBy := strings.Join(orderParts, ", ")

	limitIdx := strings.LastIndex(strings.ToLower(sql), "limit")
	orderByIdx := strings.LastIndex(strings.ToLower(sql), "order by")
	if orderByIdx != -1 {
		if limitIdx != -1 && limitIdx > orderByIdx {
			return sql[:limitIdx-1] + ", " + orderBy + " " + sql[limitIdx:]
		}
		return sql + ", " + strings.Join(orderParts, ", ")
	}

	if limitIdx != -1 {
		return sql[:limitIdx] + "ORDER BY " + orderBy + " " + sql[limitIdx:]
	}

	return sql + " ORDER BY " + orderBy
}
