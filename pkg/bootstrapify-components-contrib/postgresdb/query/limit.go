package query

import "fmt"

type Limit struct {
	Limit  int64
	Offset int64
}

func (l *Limit) AddLimit(sql string) string {
	if l.Limit > 0 {
		return sql + fmt.Sprintf(" LIMIT %d OFFSET %d", l.Limit, l.Offset)
	}
	return sql
}
