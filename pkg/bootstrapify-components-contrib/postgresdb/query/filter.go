package query

import (
	"fmt"
	"strconv"
	"strings"
)

type Filter map[string][]string

func (f *Filter) AddWhere(sql string, args ...any) (string, []any) {
	if f == nil || len(*f) == 0 {
		return sql, args
	}

	where := ""
	for k, vals := range *f {
		if where == "" {
			where = "WHERE "
		} else {
			where += "AND "
		}

		keys := strings.Split(k, ".")
		key := ""
		for i, k := range keys {
			key += fmt.Sprintf(`"%s"`, k)

			if i+1 < len(keys) {
				key += "."
			}
		}

		where += "("
		for i, v := range vals {
			where += key + " = $" + strconv.Itoa(len(args)+1) + " "
			args = append(args, v)

			if i+1 < len(vals) {
				where += " OR "
			}
		}
		where += ")"
	}

	fromIdx := strings.LastIndex(strings.ToLower(sql), "from")
	from := sql[fromIdx:]

	if li := strings.LastIndex(strings.ToLower(from), "where"); li != -1 {
		from = from[:li] + where + " AND " + from[li+5:]
	} else if li := strings.LastIndex(strings.ToLower(from), "order by"); li != -1 {
		from = from[:li] + where + " ORDER BY" + from[li+8:]
	} else if li := strings.LastIndex(strings.ToLower(from), "group by"); li != -1 {
		from = from[:li] + where + " GROUP BY" + from[li+8:]
	} else {
		from += " " + where
	}

	sql = sql[:fromIdx] + from

	return sql, args
}
