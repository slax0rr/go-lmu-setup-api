package postgresdb

import (
	"errors"
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/pgx/v5"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/jackc/pgx/v5/stdlib"
)

type MigrationsConfig struct {
	Pool     *pgxpool.Pool
	Location string
}

type Migrations struct {
	migrate *migrate.Migrate
}

func InitMigrations(cfg *MigrationsConfig) (*Migrations, error) {
	db := stdlib.OpenDBFromPool(cfg.Pool)
	driver, err := pgx.WithInstance(db, &pgx.Config{})
	if err != nil {
		return nil, fmt.Errorf("initiate pgx instance for migrations: %w", err)
	}

	m, err := migrate.NewWithDatabaseInstance(fmt.Sprintf("file://%s", cfg.Location),
		"pgx", driver)
	if err != nil {
		return nil, fmt.Errorf("initiate migrations with db instance: %w", err)
	}

	return &Migrations{migrate: m}, nil
}

func (m *Migrations) Up() error {
	err := m.migrate.Up()
	if err != nil {
		if errors.Is(err, migrate.ErrNoChange) {
			return nil
		}
		return fmt.Errorf("run up migrations: %w", err)
	}
	return nil
}

func (m *Migrations) Down() error {
	err := m.migrate.Down()
	if err != nil {
		return fmt.Errorf("run down migrations: %w", err)
	}
	return nil
}
