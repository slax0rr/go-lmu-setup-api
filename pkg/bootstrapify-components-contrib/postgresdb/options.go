package postgresdb

import (
	"fmt"
	"net/url"

	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify/components"
)

type withHost struct {
	host string
}

func (o withHost) Apply(c components.Component) error {
	postgresComponent, ok := c.(*Component)
	if !ok {
		return fmt.Errorf("component is not the %s component", ComponentName)
	}

	postgresComponent.host = o.host

	return nil
}

func WithHost(host string) components.Option {
	return withHost{
		host: host,
	}
}

type withPort struct {
	port uint
}

func (o withPort) Apply(c components.Component) error {
	postgresComponent, ok := c.(*Component)
	if !ok {
		return fmt.Errorf("component is not the %s component", ComponentName)
	}

	postgresComponent.port = o.port

	return nil
}

func WithPort(port uint) components.Option {
	return withPort{
		port: port,
	}
}

type withDatabase struct {
	database string
}

func (o withDatabase) Apply(c components.Component) error {
	postgresComponent, ok := c.(*Component)
	if !ok {
		return fmt.Errorf("component is not the %s component", ComponentName)
	}

	postgresComponent.database = o.database

	return nil
}

func WithDatabase(database string) components.Option {
	return withDatabase{
		database: database,
	}
}

type withUser struct {
	user string
}

func (o withUser) Apply(c components.Component) error {
	postgresComponent, ok := c.(*Component)
	if !ok {
		return fmt.Errorf("component is not the %s component", ComponentName)
	}

	postgresComponent.user = o.user

	return nil
}

func WithUser(user string) components.Option {
	return withUser{
		user: user,
	}
}

type withPassword struct {
	password string
}

func (o withPassword) Apply(c components.Component) error {
	postgresComponent, ok := c.(*Component)
	if !ok {
		return fmt.Errorf("component is not the %s component", ComponentName)
	}

	postgresComponent.password = o.password

	return nil
}

func WithPassword(password string) components.Option {
	return withPassword{
		password: password,
	}
}

type withDSN struct {
	dsn string
}

func (o withDSN) Apply(c components.Component) error {
	postgresComponent, ok := c.(*Component)
	if !ok {
		return fmt.Errorf("component is not the %s component", ComponentName)
	}

	var err error
	postgresComponent.dsn, err = url.Parse(o.dsn)
	if err != nil {
		return fmt.Errorf("parse database dsn url: %w", err)
	}

	return nil
}

func WithDSN(dsn string) components.Option {
	return withDSN{
		dsn: dsn,
	}
}

type withMigrations struct {
	cfg *MigrationsConfig
}

func (o withMigrations) Apply(c components.Component) error {
	postgresComponent, ok := c.(*Component)
	if !ok {
		return fmt.Errorf("component is not the %s component", ComponentName)
	}

	postgresComponent.migrationsConfig = o.cfg
	return nil
}

func WithMigrations(location string) components.Option {
	return withMigrations{
		cfg: &MigrationsConfig{
			Location: location,
		},
	}
}
