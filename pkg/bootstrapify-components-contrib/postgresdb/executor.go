package postgresdb

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"

	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/postgresdb/query"
)

var (
	ErrNotFound       error = fmt.Errorf("resource not found")
	ErrNoDBConnection error = fmt.Errorf("database connection pool not set")
	ErrNoTransaction  error = fmt.Errorf("transaction has not been started")
)

type Tx interface {
	pgx.Tx
}

type CloseTx func(error) error

type Executor struct {
	dbpool  *pgxpool.Pool
	tx      pgx.Tx
	filter  query.Filter
	orderBy query.OrderBy
	limit   query.Limit
}

type ExecOption func(*Executor)

func WithTransaction(tx Tx) ExecOption {
	return func(opts *Executor) {
		opts.tx = tx
	}
}

func WithFilter(filter query.Filter) ExecOption {
	return func(opts *Executor) {
		opts.filter = filter
	}
}

func WithOrderBy(orderBy query.OrderBy) ExecOption {
	return func(opts *Executor) {
		opts.orderBy = orderBy
	}
}

func WithLimit(limit query.Limit) ExecOption {
	return func(opts *Executor) {
		opts.limit = limit
	}
}

func NewExecutor(dbpool *pgxpool.Pool, opts ...ExecOption) *Executor {
	e := &Executor{dbpool: dbpool}
	for _, opt := range opts {
		opt(e)
	}
	return e
}

func (e *Executor) Exec(ctx context.Context, sql string, args ...any) (pgconn.CommandTag, error) {
	sql, args = e.filter.AddWhere(sql, args...)
	if e.tx != nil {
		return e.tx.Exec(ctx, sql, args...)
	}

	sql = e.orderBy.AddOrderBy(sql)

	if e.dbpool == nil {
		return pgconn.CommandTag{}, ErrNoDBConnection
	}

	return e.dbpool.Exec(ctx, sql, args...)
}

func (e *Executor) Query(ctx context.Context, sql string, args ...any) (pgx.Rows, error) {
	sql, args = e.filter.AddWhere(sql, args...)
	if e.tx != nil {
		return e.tx.Query(ctx, sql, args...)
	}

	sql = e.orderBy.AddOrderBy(sql)
	sql = e.limit.AddLimit(sql)

	if e.dbpool == nil {
		return nil, ErrNoDBConnection
	}

	return e.dbpool.Query(ctx, sql, args...)
}

func (e *Executor) QueryRow(ctx context.Context, sql string, args ...any) (pgx.Row, error) {
	sql, args = e.filter.AddWhere(sql, args...)
	if e.tx != nil {
		return e.tx.QueryRow(ctx, sql, args...), nil
	}

	sql = e.orderBy.AddOrderBy(sql)

	if e.dbpool == nil {
		return nil, ErrNoDBConnection
	}

	return e.dbpool.QueryRow(ctx, sql, args...), nil
}
