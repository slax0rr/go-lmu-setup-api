package postgresdb

import (
	"context"
	"fmt"
	"net/url"

	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/rs/zerolog"

	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify/components"
)

const (
	ComponentName = "postgresdb"

	scheme = "postgres"
)

type Component struct {
	components.Base

	dsn              *url.URL
	host             string
	port             uint
	database         string
	user             string
	password         string
	migrationsConfig *MigrationsConfig

	pool       *pgxpool.Pool
	migrations *Migrations
}

func New() components.Component {
	component := &Component{}
	component.SetName(ComponentName)
	return component
}

func (c *Component) Configure(opts ...components.Option) error {
	for _, opt := range opts {
		err := opt.Apply(c)
		if err != nil {
			return fmt.Errorf("apply %s component options: %w", ComponentName, err)
		}
	}

	c.Log(zerolog.DebugLevel, "configuring component", "name", ComponentName)

	if c.dsn == nil {
		c.dsn = BuildDSN(c.user, c.password, c.host, c.database, c.port)
	}

	var err error
	c.pool, err = pgxpool.New(context.Background(), c.dsn.String())
	if err != nil {
		return fmt.Errorf("establish db pool connection: %w", err)
	}
	if c.migrationsConfig != nil {
		c.migrationsConfig.Pool = c.pool
		c.migrations, err = InitMigrations(c.migrationsConfig)
		if err != nil {
			return fmt.Errorf("init migrations: %w", err)
		}
	}

	return nil
}

func (c *Component) Start() (err error) {
	if err := c.Health(context.Background()); err != nil {
		return err
	}

	if c.migrations != nil {
		if err := c.migrations.Up(); err != nil {
			return fmt.Errorf("start up migrations: %w", err)
		}
	}

	return nil
}

func (_ *Component) Shutdown() error {
	return nil
}

func (c *Component) Pool() *pgxpool.Pool {
	return c.pool
}

func (c *Component) Health(ctx context.Context) error {
	err := c.pool.Ping(ctx)
	if err != nil {
		return fmt.Errorf("check db pool connection: %w", err)
	}
	return nil
}

func BuildDSN(user, password, host, database string, port uint, queryOpts ...string) *url.URL {
	dsn := url.URL{
		Scheme: scheme,
		User:   url.UserPassword(user, password),
		Host:   fmt.Sprintf("%s:%d", host, port),
		Path:   database,
	}

	q := dsn.Query()
	for i := 0; i < len(queryOpts); i += 2 {
		q.Add(queryOpts[i], queryOpts[i+1])
	}
	dsn.RawQuery = q.Encode()

	return &dsn
}
