package health

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	httpComponent "gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify-components-contrib/http"
	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify/components"
)

const (
	ComponentName string = "http-health-endpoints"

	healthEndpoint string = "/health"
)

type ComponentHealthCheck interface {
	Health(context.Context) error
}

type Component struct {
	components.Base

	components      []string
	componentChecks []ComponentHealthCheck
}

var ErrUnknownComponent = fmt.Errorf("component is not the %s component", ComponentName)

func New(components ...string) *Component {
	component := &Component{}
	component.SetName(ComponentName)
	component.AddDependencies(httpComponent.ComponentName)
	component.AddDependencies(components...)

	component.components = components

	return component
}

func (c *Component) Configure(opts ...components.Option) error {
	for _, opt := range opts {
		err := opt.Apply(c)
		if err != nil {
			return fmt.Errorf("apply %s component options: %w", ComponentName, err)
		}
	}

	hc, ok := c.Dependency(httpComponent.ComponentName).(*httpComponent.Component)
	if !ok {
		return fmt.Errorf("'%s' component not loaded", httpComponent.ComponentName)
	}

	for _, comp := range c.components {
		component := c.Dependency(comp).(ComponentHealthCheck)
		c.componentChecks = append(c.componentChecks, component)
	}

	hc.RegisterRoutes(
		httpComponent.Route{
			URI:        healthEndpoint,
			HTTPMethod: http.MethodGet,
			Handler:    c.HealthHandler,
		},
	)

	return nil
}

func (c *Component) Start() error {
	return nil
}

func (c *Component) Shutdown() error {
	return nil
}

func (c *Component) HealthHandler(ctx *gin.Context) {
	for _, check := range c.componentChecks {
		if err := check.Health(ctx.Request.Context()); err != nil {
			c.Logger().Error().Err(err).Msg("health check failed")
			ctx.JSON(http.StatusInternalServerError, gin.H{"status": "unhealthy"})
			return
		}
	}
	ctx.JSON(http.StatusOK, gin.H{"status": "healthy"})
}
