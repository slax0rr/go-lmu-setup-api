package logger

import (
	"github.com/rs/zerolog"

	"gitlab.com/slax0rr/go-lmu-setup-api/pkg/bootstrapify/components"
)

const (
	ComponentName = "logger"
)

type Component struct {
	components.Base
}

func New(l zerolog.Logger) components.Component {
	component := &Component{}
	component.SetName(ComponentName)
	component.SetLogger(&l)
	return component
}

func (_ Component) Configure(_ ...components.Option) error {
	return nil
}

func (_ Component) Start() error {
	return nil
}

func (_ Component) Shutdown() error {
	return nil
}
