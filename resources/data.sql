INSERT INTO "cars" (id, class, race_class, manufacturer, model, spec, year) VALUES
    ('440f2995-866d-40ae-869a-f27f4d1f32dd', 'LMH', 'HYPERCAR', 'Toyota', 'GR010-Hybrid', '', 2023),
    ('f7de9b99-2225-49eb-9075-641b131ca03e', 'LMH', 'HYPERCAR', 'Peugeot', '9x8', '', 2023),
    ('96861210-6858-4b80-8693-28b733eadeef', 'LMH', 'HYPERCAR', 'Peugeot', '9x8', 'Evo', 2024),
    ('5b0b16d6-1155-442a-a14b-aab9bae9ea8b', 'LMH', 'HYPERCAR', 'Ferrari', '499P', '', 2023),
    ('1f2af10a-295f-461c-aec4-df34f9bb68c9', 'LMH', 'HYPERCAR', 'Glickenhaus', 'SSG 007', '', 2023),
    ('55e080c7-9375-4cdf-9f33-f7eda35367f6', 'LMH', 'HYPERCAR', 'Vanwall', 'Vandervell 680', '', 2023),
    ('bdea82af-f854-4745-8036-222308f6539d', 'LMDh', 'HYPERCAR', 'BMW', 'M Hybrid V8', '', 2024),
    ('1aad97e4-9561-400a-8e09-1b8cba2bbb14', 'LMDh', 'HYPERCAR', 'Cadillac', 'V-Series.R', '', 2023),
    ('a4ac9bfb-0af6-421b-9971-4c9ceeffc656', 'LMDh', 'HYPERCAR', 'Lamborghini', 'SC63', '', 2024),
    ('bb178f15-ed02-46da-b546-59bc363eedd6', 'LMDh', 'HYPERCAR', 'Porsche', '963', '', 2023),

    ('8bc84938-4eb3-4138-80da-8468cf4feec7', 'P2', 'LMP2', 'Oreca', '07 Gibson', '2023', 2023),

    ('54f05748-4705-4d25-8e6b-27b78de94e72', 'GTE', 'LMGTE', 'Aston Martin', 'Vantage AMR', '', 2023),
    ('105387f8-495e-4be8-b9a9-39882fcdb00e', 'GTE', 'LMGTE', 'Chevrolet', 'Corvette C8.R', '', 2023),
    ('3ae88a30-9dc3-47b6-9732-b3e46689e6b7', 'GTE', 'LMGTE', 'Ferrari', '488 GTE', 'Evo', 2023),
    ('d8827306-457a-4dad-adc5-622cd9303f35', 'GTE', 'LMGTE', 'Porsche', '911 RSR-19', '', 2023);

INSERT INTO "tracks" (id, country, track_name, year) VALUES
    ('059a488a-42c3-4629-bc78-c63f607235f4', 'be', 'Circuit de Spa-Francorchamps', 2023),
    ('6379e3a5-b774-4a67-b5b7-99aab5506980', 'fr', 'Circuit de la Sarthe (Le Mans)', 2023),
    ('886869b5-3483-4054-9c63-b61731651b14', 'us', 'Sebring International Raceway', 2023),
    ('e1716cfc-bbf9-40e9-a65c-74de3198da7c', 'it', 'Autodromo Nazionale Monza', 2023),
    ('2e9f47f9-ff0b-4a29-bf4b-b79abc3a8e0e', 'jp', 'Fuji International Speedway', 2023),
    ('82893aa7-4e23-4f3d-a35f-8ff5678ca10d', 'pt', 'Algarve International Circuit (Portimão)', 2023),
    ('12ccc0a5-257a-4de9-8f70-e70c31515ba6', 'bh', 'Bahrain International Circuit', 2023),
    ('cc1af759-8e07-48ec-8054-a655a8d344f6', 'it', 'Autodromo Internazionale Enzo e Dino Ferrari (Imola)', 2024);

INSERT INTO "cars" (id, class, race_class, manufacturer, model, spec, year) VALUES
    ('ce2636d1-292e-4f10-aaf7-f38c456829be', 'LMH', 'HYPERCAR', 'Isotta Fraschini', 'Tipo 6 LMH-C', '', 2024),
    ('ecbd2991-9771-4133-a675-58113f2ab8c1', 'LMDh', 'HYPERCAR', 'Alpine', 'A424', '', 2024);

INSERT INTO "tracks" (id, country, track_name, year) VALUES
    ('8f921a3c-37d7-4cd8-9e2b-10455c5ad90d', 'us', 'Circuit of the Americas', 2024);
