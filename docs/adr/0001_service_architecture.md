# ADR: Refactoring Service Architecture for Scalability and Flexibility

## **Status**
Accepted

## **Context**
The application was initially designed with two microservices:
- **Auth API**: Handling both authentication and user data.
- **Setup API**: Managing car setups and race strategy calculations.

Over time, we identified limitations in this architecture, particularly the tight coupling between authentication, user management, and application-specific logic. To address these concerns, we decided to **split the system into four dedicated services**:

- **Auth API** (handles authentication only)
- **User API** (stores user profiles and manages profile updates)
- **Setup API** (manages car setups, needs display names and profile images)
- **Strategy API** (stores race strategies per user but doesn’t need profile data)

This decision allows for better **separation of concerns**, **scalability**, and **reusability** of the authentication service for future applications.

### **Requirements**
1. **Decoupled Authentication from User Data**: Auth API should focus solely on authentication.
2. **Centralized User Profile Management**: Only **User API** should store and manage user profiles.
3. **Event-Driven Updates**: Changes to user profiles should be propagated asynchronously.
4. **Minimal Data Duplication**: Strategy API should only store `user_id`, not full profile data.
5. **Setup API Needs Profile Data**: Must receive updated user display names & profile pictures.

## **Decision**
1. **Auth API will not store user profiles.** Instead, it will emit a `user.authenticated` event when a user logs in.
2. **User API will consume `user.authenticated` and decide whether to create or update a user.**
3. **User API will emit events (`user.created`, `user.updated`) when user profiles are created or updated.**
4. **Setup API will listen to `user.created` and `user.updated` events** to store and update basic user data (name, profile picture).
5. **Strategy API will not store user profiles** but will store `user_id` inside the `strategies` table.

## **Diagram**

```mermaid
graph TD;
    subgraph "Event Bus"
        Kafka["Kafka"]
    end

    subgraph "Auth API"
        A1[POST /api/v1/auth] -->|Produces user.authenticated| Kafka
    end

    subgraph "User API"
        B7[GET /api/v1/user/profile]
        B1[Consumes user.authenticated]
        B2{User Exists?}
        B3[Create New User]
        B4[Update Last Login]
        B5[Produces user.created]
        
        B6[PUT /api/v1/user/profile] -->|Produces user.updated| Kafka
    end


    subgraph "Setup API"
        C1[Consumes user.created]
        C2[Store User Data]
        C3[GET /api/v1/setups] -->|Fetch Missing User Data from User API| B7
        C5[Consume user.updated]
        C6[Update User Data]
    end

    subgraph "Strategy API"
        D1[POST /api/v1/strategies] -->D2
        D2[Store user_id inside strategies table]
    end

    subgraph "User"
        WEB[UltimateSetupHub] -->|Authenticate user| A1
        WEB -->|Update profile| B6
        WEB -->|Save strategy| D1
        WEB -->|Get setup list| C3
    end

    %% Event Flow for User Creation
    Kafka --> B1
    B1 --> B2
    B2 -- No --> B3
    B2 -- Yes --> B4
    B3 --> B5
    B4 --> B5
    B5 -->|Produces user.created| Kafka
    Kafka --> C1

    %% Event Flow for User Updates
    Kafka --> C5
    C5 --> C6

    C1 --> C2
```

## **Consequences**
### **Positive**
* ✅ **Better Separation of Concerns** → Each API has a well-defined responsibility.
* ✅ **Decoupled Authentication from User Management** → Auth API remains reusable for other applications.
* ✅ **Event-Driven Updates** → Ensures Setup API always has the latest user data.
* ✅ **Minimal Data Duplication** → Strategy API avoids unnecessary profile storage.
* ✅ **Scalability & Flexibility** → Future applications can reuse Auth API without modification.

### **Negative**
* ❌ **Increased Event Complexity** → Services must correctly handle event failures.
* ❌ **Setup API Must Ensure Data Consistency** → If a user update event is missed, it could cause stale data.
* ❌ **Higher Initial Development Effort** → More services mean additional infrastructure setup.

## **Implementation Plan**
1. **Modify Auth API** to emit `user.authenticated` events.
2. **Implement User API event consumers** to process logins and updates.
3. **Implement event listeners in Setup API** for user updates.
4. **Ensure Strategy API stores only `user_id`** in strategies.
5. **Set up monitoring & retries** for event failures to avoid data inconsistencies.
6. **Deploy messaging infrastructure** (e.g., Kafka or NATS) to handle event streaming reliably.

---

### **Future Considerations**
- If needed, introduce **event replay mechanisms** (e.g., Kafka log retention) to handle missed events.
- Potentially implement a **user cache in Setup API** for performance improvements.
- Evaluate the need for a **user search API** in User API to allow Setup API to request missing data directly.

---

**Decision Date:** 2025-02-17 
**Authors:** Tomaz Lovrec  
**Reviewed By:** Mark Townsend

