# Architectural Decision Records (ADR)

This repository contains a collection of Architectural Decision Records (ADRs) that document key design decisions for our system.

## ADR List

1. [ADR-0001: Service Architecture](0001_service_architecture.md) - Decision to split from two APIs to four and implement an event-driven approach for user data management.

## What is an ADR?

An Architectural Decision Record (ADR) is a document that captures an important architectural decision, the context in which it was made, the options considered, and the final decision along with its consequences.

## How to Propose a New ADR

1. Create a new markdown file using the naming convention `XXXX_<short_title>.md` where `XXXX` is an incremental number.
2. Use the following structure:

   ```markdown
   # ADR: <Title>

   ## Status
   Proposed | Accepted | Deprecated

   ## Context
   <Explain the problem, background, or motivation for this decision.>

   ## Decision
   <Describe the final decision made and the reasoning behind it.>

   ## Consequences
   <List both positive and negative consequences of the decision.>

   ## Alternatives Considered
   <Briefly describe other options you evaluated and why they were not chosen.>
3. Add the link to the markdown file in the **ADR List** above.
