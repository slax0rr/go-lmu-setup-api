# Change Log

### Current

### 0.9.2

* fix multiple progress adjustment would sum the complete laps with previous progress

### 0.9.1

* fix current tank state being applied on all adjusted stints
* fix progress resetting stint adjustments back to their base values
* fix progress application ignoring adjusted stint laps if the progress was longer than the stint and the remaining progress bled over to the next stint

### 0.9.0

* implement pagination functionality as a middleware
* attach pagination to the setup listing endpoint
* remove all deprecated functionality to fallback to setups stored on the file system
* add endpoint to retrieve user saved strategies
* add endpoint to retrieve user created/uploaded setups
* order list of cars alphabetically by manufacturer on the cars list endpoint
* order list of tracks alphabetically by the short track name on the tracks list endpoint
* update `/strategy` routes to `/strategies` to keep consistency with other endpoints
* refactor codebase to be more in line with the layered architecture
* add track layout functionality
* add February 2025 content to migrations

### 0.8.4
* fix pit stop strategy calculations adding save laps to flat out strategies
* fix pit stop strategy calculator adding too many laps for races there full N number of stints would suffice
* fix pit stop calculator showing save strategy on a calculation where the driver would need to save over half a stint of fuel

### 0.8.3
* fix pit stop strategy not allowing longer pit stops than 99 seconds

### 0.8.2
* fix incorrect initial strategy calculations introduced in the last version

### 0.8.1
* fix validation errors for mixed conditions

### 0.8.0
* add mixed conditions for the setups
* lap time is optional for setups on the API
* save the calculated strategy
* get saved strategy
* add functionality to edit saved strategies
* add race progress, and have the calculator re-calculate your consumption and laps
* adjust each stint for a different pit length
* adjust each stint and set how many laps it will be

### 0.7.2

* fix an issue in the authentication middleware which wrongly deemed the authentication invalid

### 0.7.1
* limit strategy calculator inputs to sensible values to prevent crashes

### 0.7.0
* improved log tracing for faster debugging

### 0.6.3
* fix optional tank capacity or energy use validation errors not returning error details

### 0.6.2
* fix unhandled error if lap time or race time were equal to 0

### 0.6.1
* remove trace/debug logs from setup file retrieval

### 0.6.0
* move setups from the server file system to the database

### 0.5.0
* database update to include new content

### 0.4.2
* adjust fuel consumption calculation to not recommend too high refuel value in short splash&dash stints

### 0.4.1
* fix for potential API crash on invalid user data supplied on strategy calculator

### 0.4.0
- implement strategy calculations and endpoints to retrieve them

### 0.3.0
* add setup rating functionality
* add the information on how the user voted onto the setup listing response

### 0.2.0

* add the 2024 tracks and cars content to the database
* add a health endpoint
* add end to end tests to ensure stability of existing endpoints
* move authentication check into a middleware
* add structured error responses to all endpoints
* add update setup endpoint
* add validation to setup data on create/upload and update endpoints
* implement custom filter handling to allow caller to specify by which parameter the list should be filtered on
* add custom filtering to the setup listing endpoint
* fix setup creation to respond with 401 Unauthorized if user authentication is invalid

### 0.1.0

* bare bones service implementation and bootstrap
* database migrations on API service start up
* add database transactions
* cars listing endpoint
* tracks listing endpoint
* setups listing endpoint
* add setup file storage
* create/upload setup endpoint
    * including size limit of the setup to 256KB
* download setup file endpoint with download counter
* populate database with cars and tracks data
