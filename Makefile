BIN_NAME=go-lmu-setup-api
BIN_ARCH=amd64
BIN_PATH=$(GOPATH)/bin/$(BIN_NAME)-$(BIN_ARCH)
IMAGE_NAME=registry.lovrec.dev/$(BIN_NAME)
IMAGE_TAG=latest
DOCKERFILE=./build/go-lmu-setup-api/Dockerfile
COMPOSEFILE=./docker/docker-compose.yml

DBNAME=lmu_setups
DBUSER=lmu_setups_user
DBPASS=devpass
DATAFILE=./resources/data.sql

AUTH_DBNAME=auth
AUTH_DBUSER=auth_user
AUTH_DBPASS=devpass

DATABASE_DSN=postgres://$(DBUSER):$(DBPASS)@localhost:5433/$(DBNAME)
MIGRATIONS=internal/app/go-lmu-setup-api/infra/database/migrations/
USER_TOKEN_KEY=c3VwZXJzZWNyZXR0ZXN0c3luY2hyb25vdXNrZXkxMjMK

.PHONY: install test build clean build-image

all: install get test build build-image

install:
	go install github.com/vektra/mockery/v2@v2.50.4

get:
	go get ./...

test: mocks
	go test -v -count 1 ./...

mocks:
	mockery
	go mod tidy

build:
	GOARCH=$(BIN_ARCH) GOOS=linux go build -o $(BIN_PATH) ./cmd/$(BIN_NAME) 

build-image:
	docker build -t $(IMAGE_NAME):$(IMAGE_TAG) -f $(DOCKERFILE) .

shipit: build-image
	docker push $(IMAGE_NAME):$(IMAGE_TAG)

ship_branch:
	IMAGE_TAG=$$( \
		latest_tag=$$(git tag -l 'v*' | sort -V | tail -n1); \
		next_tag=$$(echo "$$latest_tag" | awk -F. '{print $$1"."$$2+1".0"}'); \
		branch=$$(git rev-parse --abbrev-ref HEAD); \
		commit_hash=$$(git rev-parse --short HEAD); \
		echo "$${next_tag}-$${branch}_$${commit_hash}" \
	); \
	$(MAKE) IMAGE_TAG=$$IMAGE_TAG shipit

docker_start:
	docker compose --file $(COMPOSEFILE) -p setupsapi pull
	DB_PORT=5433 docker compose --file $(COMPOSEFILE) -p setupsapi up -d

docker_down:
	docker compose --file $(COMPOSEFILE) -p setupsapi down

db_populate:
	docker compose --file $(COMPOSEFILE) -p setupsapi exec -T db psql -U postgres -d $(DBNAME) < $(DATAFILE)

run: build docker_start
	DATABASE_DSN=$(DATABASE_DSN) DB_MIGRATIONS_LOCATION=$(MIGRATIONS) USER_TOKEN_ENCODED_KEY=$(USER_TOKEN_KEY) $(BIN_PATH)

clean:
	rm -rf $(BIN_PATH)

genkeys:
	mkdir -p resources/keys/
	openssl genrsa -out resources/keys/rsa_private_key.pem 3072
	openssl rsa -pubout -in resources/keys/rsa_private_key.pem -out resources/keys/rsa_public_key.pem
