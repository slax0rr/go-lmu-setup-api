package main

import (
	"os"

	"github.com/rs/zerolog"
	"gitlab.com/slax0rr/go-lmu-setup-api/internal/app/go-lmu-setup-api/application"
)

func main() {
	log := zerolog.New(os.Stdout).
		With().Timestamp().Logger().
		Level(zerolog.DebugLevel)

	log.Debug().Msg("starting application")
	err := application.Bootstrap(&log)
	if err != nil {
		log.Fatal().Err(err).Msg("application failed")
	}

	log.Debug().Msg("application stopped cleanly")
}
