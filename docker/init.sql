CREATE DATABASE lmu_setups;
CREATE USER lmu_setups_user ENCRYPTED PASSWORD 'devpass';
GRANT ALL PRIVILEGES ON DATABASE lmu_setups TO lmu_setups_user;

CREATE DATABASE auth;
CREATE USER auth_user ENCRYPTED PASSWORD 'devpass';
GRANT ALL PRIVILEGES ON DATABASE auth TO auth_user;

\connect lmu_setups;
GRANT ALL ON SCHEMA public TO lmu_setups_user;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

\connect auth;
GRANT ALL ON SCHEMA public TO auth_user;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
